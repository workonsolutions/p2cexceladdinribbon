﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;
using P2CExcelAddIn.Business.Properties;

namespace P2CExcelAddIn.Business.ExcelControllers
{
    public static class P2CAddInMasterDataController
    {
        private static string[] Letters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CN", "CM", "CO" };

        public static void UpdateMasterData(Workbook workbookToUpdate,string dataWrite)
        {
            var serializable = new System.Web.Script.Serialization.JavaScriptSerializer();
            serializable.MaxJsonLength = 2147483644;
            var dataDes = serializable.Deserialize<MasterDataReturn>(dataWrite);


            foreach (Worksheet sheet in workbookToUpdate.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMVendedores))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[1000, 5]].Value = "";
                    int i = 1;

                    foreach (RoleP2C vend in dataDes.masterData.vendedoresList)
                    {
                        sheet.Range["A" + i].Value = vend.userId.ToString();
                        sheet.Range["B" + i].Value = vend.userName.ToString();
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMEstadosProposta))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[100, 5]].Value = "";
                    int i = 1;

                    foreach (Dictionary<string, object> stateDicionary in dataDes.masterData.estadoPropostaList)
                    {
                        foreach (var state in stateDicionary)
                        {
                            if (state.Key.Equals("StateID"))
                                sheet.Range["A" + i].Value = state.Value.ToString();
                            if (state.Key.Equals("StateQualifiedName"))
                                sheet.Range["B" + i].Value = state.Value.ToString();

                        }
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMTiposNegocio))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[100, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelAddInVariables tipoNegocio in dataDes.masterData.tiposNegocioList)
                    {
                        sheet.Range["A" + i].Value = tipoNegocio.id.ToString();
                        sheet.Range["B" + i].Value = tipoNegocio.id.ToString() + " - " + tipoNegocio.descricao;
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMTiposContrato))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[50, 60]].Value = "";
                    int i = 1;
                    int j = 0;

                    int countElems = 0;
                    foreach (ExcelAddInVariables tipoNegocio in dataDes.masterData.tiposNegocioList)
                    {
                        foreach (ExcelTipoContrato tipoContrato in dataDes.masterData.tipoContratoList)
                        {
                            if (tipoNegocio.id == tipoContrato.idTipoNegocio)
                            {
                                countElems++;
                                sheet.Range[Letters[j] + i].Value = tipoContrato.id.ToString();
                                sheet.Range[Letters[j + 1] + i].Value = tipoContrato.descricao;
                                sheet.Range[Letters[j + 2] + i].Value = tipoContrato.idTipoNegocio.ToString();

                                i++;
                            }
                        }

                        string nameToAdd = "NameP2c" + tipoNegocio.id.ToString();
                        if (countElems == 0)
                            countElems = 1;

                        Microsoft.Office.Interop.Excel.Range range = sheet.Range[sheet.Cells[1, j + 2], sheet.Cells[countElems, j + 2]];
                        workbookToUpdate.Names.Add(nameToAdd, range);

                        countElems = 0;
                        i = 1;
                        j = j + 4;

                    }

                }

                if (sheet.Name.Equals(Settings.Default.SheetDMAreas))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[100, 5]].Value = "";
                    int i = 1;

                    foreach (RoleP2C role in dataDes.masterData.areasList)
                    {
                        sheet.Range["A" + i].Value = role.userId.ToString();
                        sheet.Range["B" + i].Value = role.userName.ToString();
                        i++;
                    }

                }

                if (sheet.Name.Equals(Settings.Default.SheetDMParceiros))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[100, 5]].Value = "";
                    int i = 1;

                    foreach (RoleP2C parceiros in dataDes.masterData.parceirosList)
                    {
                        sheet.Range["A" + i].Value = parceiros.userId.ToString();
                        sheet.Range["B" + i].Value = parceiros.userName.ToString();
                        i++;
                    }
                }

                //if (sheet.Name.Equals(Settings.Default.SheetDMPeriocidade))
                //{
                //    sheet.Range[sheet.Cells[1, 1], sheet.Cells[50, 5]].Value = "";
                //    int i = 1;

                //    foreach (ExcelAddInVariables periocidade in dataDes.masterData.periocidadeList)
                //    {
                //        sheet.Range["A" + i].Value = periocidade.id.ToString();
                //        sheet.Range["B" + i].Value = periocidade.descricao;
                //        i++;
                //    }
                //}

                if (sheet.Name.Equals(Settings.Default.SheetDMCopias))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[50, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelAddInVariables copias in dataDes.masterData.copiasList)
                    {
                        sheet.Range["A" + i].Value = copias.id.ToString();
                        sheet.Range["B" + i].Value = copias.descricao;
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMCarencia))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[50, 5]].Value = "";
                    int i = 1;
                    //sheet.Range["A" + i].Value = "0";
                    //sheet.Range["B" + i].Value = string.Empty;
                    //i = 2;
                    foreach (ExcelAddInVariables carencia in dataDes.masterData.carenciaList)
                    {
                        sheet.Range["A" + i].Value = carencia.id.ToString();
                        sheet.Range["B" + i].Value = carencia.descricao;
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMTabela))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[100, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelAddInVariables tabela in dataDes.masterData.tabelaList)
                    {
                        sheet.Range["A" + i].Value = tabela.id.ToString();
                        sheet.Range["B" + i].Value = tabela.descricao;
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMEquipamentos))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[2000, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelEquipamento equipamento in dataDes.masterData.equipamentosList)
                    {
                        sheet.Range["A" + i].Value = equipamento.idPart.ToString();
                        sheet.Range["B" + i].Value = equipamento.modelCode;
                        sheet.Range["C" + i].Value = equipamento.product;
                        sheet.Range["D" + i].Value = equipamento.listPrice.ToString();
                        sheet.Range["E" + i].Value = equipamento.description;
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMServicos))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[2000, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelServico servico in dataDes.masterData.servicosList)
                    {
                        sheet.Range["A" + i].Value = servico.IdServico.ToString();
                        sheet.Range["B" + i].Value = servico.PartId.ToString();
                        sheet.Range["C" + i].Value = servico.product;
                        sheet.Range["D" + i].Value = servico.Plano;
                        sheet.Range["E" + i].Value = servico.Tfm.ToString();
                        sheet.Range["F" + i].Value = servico.VolIncCor.ToString();
                        sheet.Range["G" + i].Value = servico.PrecoPrintCor.ToString();
                        sheet.Range["H" + i].Value = servico.VolIncPB.ToString();
                        sheet.Range["I" + i].Value = servico.PrecoPrintPB.ToString();
                        sheet.Range["J" + i].Value = servico.VolIncCor.ToString();
                        sheet.Range["K" + i].Value = servico.VolInc3.ToString();
                        sheet.Range["L" + i].Value = servico.PrecoPrint3.ToString();
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDMNiveis))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[100, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelNivelOperador nivel in dataDes.masterData.niveisOperadorList)
                    {
                        sheet.Range["A" + i].Value = nivel.IdNivel.ToString();
                        sheet.Range["B" + i].Value = nivel.NivelRecurso;
                        sheet.Range["C" + i].Value = nivel.CustoMensal;
                        i++;
                    }
                }

                if (sheet.Name.Equals(Settings.Default.SheetDmFactores))
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[200, 5]].Value = "";
                    int i = 1;

                    foreach (ExcelTabelaFactores factor in dataDes.masterData.tabelaFactoresList)
                    {
                        sheet.Range["A" + i].Value = factor.factor.ToString();
                        sheet.Range["B" + i].Value = factor.idTabela.ToString();
                        sheet.Range["C" + i].Value = factor.tabela;
                        i++;
                    }
                }

            }
        }

        
        
    }
}
