﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;

namespace P2CExcelAddIn.Business.ExcelControllers
{
    public static class P2CAddinUtilsController
    {
        public static string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (adapter.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = sMacAddress + adapter.GetPhysicalAddress().ToString() + "#";
                }
            }

            return sMacAddress;
        }
    }
}
