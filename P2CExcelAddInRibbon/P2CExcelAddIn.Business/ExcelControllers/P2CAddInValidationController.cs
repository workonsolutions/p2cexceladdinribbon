﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using P2CExcelAddIn.Business.Properties;
using SpiritucSI.Xerox.Workflow.Business;
using System.Text.RegularExpressions;

namespace P2CExcelAddIn.Business.ExcelControllers
{
    public static class P2CAddInValidationController
    {
        public static bool ValidateProposal(out List<string> msgErrors, Workbook workbookValidate, Sheets workSheets, ProposalExcel proposal)
        {
            bool retVal = true;
            List<string> arrMsgs = new List<string>();
            foreach (Worksheet sheet in workbookValidate.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                {
                    string nomeCliente = Convert.ToString(sheet.Range["C9"].Value);
                    string numContribuinte = Convert.ToString(sheet.Range["C10"].Value);
                    string codigoPostalCliente = Convert.ToString(sheet.Range["C15"].Value);
                    string codigoPostalCliente3 = Convert.ToString(sheet.Range["E15"].Value);
                    string localidadeCliente = Convert.ToString(sheet.Range["C16"].Value);
                    string moradaCliente = Convert.ToString(sheet.Range["C14"].Value);

                    string tipoNegocio = Convert.ToString(sheet.Range["J10"].Value);
                    string tipoContrato = Convert.ToString(sheet.Range["P10"].Value);
                    string duracaoContrato = Convert.ToString(sheet.Range["J17"].Value);
                    string fatorCreditex = Convert.ToString(sheet.Range["P18"].Value);
                    string tabela = Convert.ToString(sheet.Range["P17"].Value);
                    string emailCli = Convert.ToString(sheet.Range["C11"].Value);

                    string[] tipoNegocioCod = tipoNegocio.Split('-');
                    int codigoTipoNegocio = Convert.ToInt32(tipoNegocioCod[0].Trim());

                    if (nomeCliente == null || nomeCliente == string.Empty)
                    {
                        arrMsgs.Add("Nome de Cliente é Obrigatório");
                        retVal = false;
                    }
                    else
                    {
                        if (nomeCliente.Trim().Length > 30)
                        {
                            arrMsgs.Add("Comprimento de Nome de Cliente não pode ser superior a 30 caracteres");
                            retVal = false;
                        }

                    }


                    if (numContribuinte == null || numContribuinte == string.Empty)
                    {
                        arrMsgs.Add("Número de contribuinte Inválido");
                        retVal = false;
                    }
                    else
                    {
                        if (!ValidateNif(numContribuinte.Trim()))
                        {
                            arrMsgs.Add("Número de Contribuinte Inválido");
                            retVal = false;
                        }
                    }
                    if (tipoNegocio == null || tipoNegocio == string.Empty)
                    {
                        arrMsgs.Add("Tipo de Negócio é Obrigatório");
                        retVal = false;
                    }
                    else
                    {
                        if (!ValidateTipoNegocioEquipRemoverServico(workSheets,proposal))
                        {
                            arrMsgs.Add("Tipo de Negócio não pode ter Serviços e Equipamentos a remover");
                            retVal = false;
                        }
                    }


                    if ((codigoTipoNegocio!=77 && codigoTipoNegocio!=94) && (tipoContrato == null || tipoContrato == string.Empty))
                    {
                        arrMsgs.Add("Tipo de Contrato é Obrigatório");
                        retVal = false;
                    }

                    if (codigoPostalCliente != null && codigoPostalCliente != string.Empty)
                    {

                        int isNumericCod4 = 0;
                        bool isNumeric = int.TryParse(codigoPostalCliente, out isNumericCod4);

                        if (codigoPostalCliente.Trim().Length != 4 || !isNumeric)
                        {
                            arrMsgs.Add("Código de Postal 4 inválido");
                            retVal = false;
                        }
                    }

                    if (codigoPostalCliente3 != null && codigoPostalCliente3 != string.Empty)
                    {

                        int isNumericCod3 = 0;
                        bool isNumeric = int.TryParse(codigoPostalCliente3, out isNumericCod3);

                        if (codigoPostalCliente3.Trim().Length != 3 || !isNumeric)
                        {
                            arrMsgs.Add("Código de Postal 3 inválido");
                            retVal = false;
                        }
                    }

                    if (localidadeCliente != null && localidadeCliente != string.Empty)
                    {
                        if (localidadeCliente.Length > 30)
                        {
                            arrMsgs.Add("Comprimento de Localidade não pode ser superior a 30 caracteres");
                            retVal = false;
                        }
                    }

                    if (moradaCliente != null && moradaCliente != string.Empty)
                    {
                        if (moradaCliente.Length > 30)
                        {
                            arrMsgs.Add("Comprimento de Morada não pode ser superior a 30 caracteres");
                            retVal = false;
                        }
                    }

                    if (duracaoContrato != null && duracaoContrato != string.Empty)
                    {
                        int duracaoContratoVal = 0;
                        bool isValidDuracao = int.TryParse(duracaoContrato, out duracaoContratoVal);
                        if (!isValidDuracao)
                        {
                            arrMsgs.Add("Duração de Contrato inválida");
                            retVal = false;
                        }
                        else
                        {
                            if (duracaoContratoVal > 999)
                            {
                                arrMsgs.Add("Duração de Contrato não pode ser maior que 999");
                                retVal = false;
                            }
                        }
                    }

                    if (fatorCreditex != null && fatorCreditex != string.Empty)
                    {
                        double fatorCreditexVal = 0;
                        bool isfatorCreditex = double.TryParse(fatorCreditex.Replace('.', ','), out fatorCreditexVal);
                        if (!isfatorCreditex)
                        {
                            arrMsgs.Add("Factor de Creditex inválido");
                            retVal = false;
                        }
                        else
                        {
                            if (tabela!=null && tabela!=string.Empty && isCreditexBussiness(codigoTipoNegocio))
                            {
                                List<ExcelTabelaFactores> listFactores = new List<ExcelTabelaFactores>();
                                listFactores = P2CAddInGenericListsController.GetListaFactores(workSheets);
                                double minFact = 0.0D;
                                double maxFact = 0.0D;
                                var listTabelaFact = listFactores.Where(x => x.tabela.Equals(tabela.Trim()));
                                listTabelaFact = listTabelaFact.Where(x => x.factor > 0);
                                if (listTabelaFact != null && listTabelaFact.Count() > 0)
                                {
                                    minFact = listTabelaFact.Min(x => x.factor);
                                    maxFact = listTabelaFact.Max(x => x.factor);
                                    if (fatorCreditexVal >= minFact && fatorCreditexVal <= maxFact)
                                    {
                                        
                                    }
                                    else
                                    {
                                        arrMsgs.Add("Factor Creditex não está no intervalo da tabela seleccionada: " + minFact.ToString() + "..." + maxFact.ToString());
                                        retVal = false;
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        if (isCreditexBussiness(codigoTipoNegocio))
                        {
                            arrMsgs.Add("Factor de Creditex não pode ser vazio para tipos de Negócios Creditex");
                            retVal = false;
                        }
                    }

                    if (emailCli != null && emailCli != string.Empty)
                    {
                        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                        Match match = regex.Match(emailCli);
                        if (!match.Success)
                        {
                             arrMsgs.Add("Email de Cliente inválido");
                             retVal = false;
                        }
                           
                    }
                   //  92; 93; 95 ou 1000.
                    if (isCreditexBussiness(codigoTipoNegocio))
                    {

                        if (tabela == null || tabela == string.Empty)
                        {
                            arrMsgs.Add("Para tipos de Negócios Creditex é obrigatório especificar a tabela");
                            retVal = false;
                        }
                    }
                   




                }
            }

            msgErrors = arrMsgs;
            return retVal;
        }

        public static bool isCreditexBussiness(int codigoTipoNegocio)
        {
            bool retval = false;
            if (codigoTipoNegocio == 1 || codigoTipoNegocio == 72 || codigoTipoNegocio == 75 ||
                codigoTipoNegocio == 76 || codigoTipoNegocio == 83 || codigoTipoNegocio == 90 ||
                codigoTipoNegocio == 91 || codigoTipoNegocio == 92 || codigoTipoNegocio == 93 ||
                codigoTipoNegocio == 95 || codigoTipoNegocio == 1000)
            {
                retval = true;
            }
            return retval;
        }

        public static bool ValidateTipoNegocioEquipRemoverServico(Sheets workSheets, ProposalExcel proposal)
        {

            bool retval = true;
            ProposalExcel propData = proposal;

            //string[] tipoNegocioCod = propData.ti;
            int codigoTipoNegocio = propData.tipoNegocioCod ?? 0;
            List<ExcelTipoNegocioMenu> listMenu = new List<ExcelTipoNegocioMenu>();
            List<ExcelTipoNegocioMenu> listNegocios = new List<ExcelTipoNegocioMenu>();

            listMenu = P2CAddInGenericListsController.GetListaTiposNegocio(workSheets);
            listNegocios = listMenu.FindAll(x => x.idNegocio == codigoTipoNegocio);

            bool haveSheetEquipRemover = false;
            bool haveSheetServico = false;
            foreach (ExcelTipoNegocioMenu menuIt in listNegocios)
            {
                if (menuIt.descSheet.Equals(Settings.Default.SheetEquipRemover))
                    haveSheetEquipRemover = true;
                if (menuIt.descSheet.Equals(Settings.Default.SheetServicos))
                    haveSheetServico = true;
            }

            if (!haveSheetEquipRemover)
            {
                if (propData.equipamentosRemoverList != null && propData.equipamentosRemoverList.Count > 0)
                {
                    return false;
                }
            }

            if (!haveSheetServico)
            {
                if (propData.equipamentosServicosList != null && propData.equipamentosServicosList.Count > 0)
                {
                    foreach (EquipamentosInstalarServicoExcel it in propData.equipamentosServicosList)
                    {
                        if (it.GuidPagePack != null || it.GuidPagePack != string.Empty)
                            return false;
                    }
                }
            }


            return retval;
        }

        public static bool ValidateNif(string Nif)
        {
            bool retVal = false;

            int len = Nif.Length;
            Nif = Nif.Trim();

            if (len == 9 || (len == 11 && Nif.Substring(0, 2).Equals("PT")))
            {
                retVal = true;
                var iDiv = 0;
                Nif = (len == 11 && Nif.Substring(0, 2) == "PT") ? Nif.Replace("PT", "") : Nif;

                int pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, iCheckDigNif = 0;
                int.TryParse(Nif.Substring(0, 1), out pos1);
                int.TryParse(Nif.Substring(1, 1), out pos2);
                int.TryParse(Nif.Substring(2, 1), out pos3);
                int.TryParse(Nif.Substring(3, 1), out pos4);
                int.TryParse(Nif.Substring(4, 1), out pos5);
                int.TryParse(Nif.Substring(5, 1), out pos6);
                int.TryParse(Nif.Substring(6, 1), out pos7);
                int.TryParse(Nif.Substring(7, 1), out pos8);

                iDiv = pos1 * 9 + pos2 * 8 + pos3 * 7;
                iDiv = iDiv + pos4 * 6 + pos5 * 5 + pos6 * 4;
                iDiv = iDiv + pos7 * 3 + pos8 * 2;
                var iResto = iDiv % 11;
                var iCheckDig = 11 - iResto;
                if (iCheckDig > 9) iCheckDig = 0;
                int.TryParse(Nif.Substring(8, 1), out iCheckDigNif);
                if (iCheckDig != iCheckDigNif)
                {
                    return false;
                }
            }
            else if (len < 11)
            {
                return false;
            }

            return retVal;
        }
    }

     
}
