﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpiritucSI.Xerox.Workflow.Business;
using Microsoft.Office.Interop.Excel;
using P2CExcelAddIn.Business.Properties;
using P2CExcelAddIn.Business.Entities;

namespace P2CExcelAddIn.Business.ExcelControllers
{
    public static class P2CAddInGenericListsController
    {

        public static List<ExcelServico> GetListaServicos(Sheets workSheets)
        {
            List<ExcelServico> retList = new List<ExcelServico>();

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMServicos))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        var tfmCell = sheet.Range["E" + i].Value;
                        double tfm;
                        bool isDouble = Double.TryParse(tfmCell, out tfm);
                        if (!isDouble)
                        {
                            tfm = Convert.ToDouble(tfmCell.Replace(',', '.'));
                        }

                        var precoPrintCorCell = sheet.Range["G" + i].Value;
                        double precoPrintCor;
                        isDouble = Double.TryParse(precoPrintCorCell, out precoPrintCor);
                        if (!isDouble)
                        {
                            precoPrintCor = Convert.ToDouble(precoPrintCorCell.Replace(',', '.'));
                        }

                        var precoPrintPBCell = sheet.Range["I" + i].Value;
                        double precoPrintPB;
                        isDouble = Double.TryParse(precoPrintPBCell, out precoPrintPB);
                        if (!isDouble)
                        {
                            precoPrintPB = Convert.ToDouble(precoPrintPBCell.Replace(',', '.'));
                        }

                        var precoPrint3Cell = sheet.Range["L" + i].Value;
                        double precoPrint3;
                        isDouble = Double.TryParse(precoPrint3Cell, out precoPrint3);
                        if (!isDouble)
                        {
                            precoPrint3 = Convert.ToDouble(precoPrint3Cell.Replace(',', '.'));
                        }

                        ExcelServico servicoAdd = new ExcelServico
                        {
                            IdServico = new Guid(sheet.Range["A" + i].Value),
                            PartId = Convert.ToInt32(sheet.Range["B" + i].Value),
                            product = Convert.ToString(sheet.Range["C" + i].Value),
                            Plano = sheet.Range["D" + i].Value,
                            Tfm = tfm,
                            VolIncCor = Convert.ToInt32(sheet.Range["F" + i].Value),
                            PrecoPrintCor = precoPrintCor,
                            VolIncPB = Convert.ToInt32(sheet.Range["H" + i].Value),
                            PrecoPrintPB = precoPrintPB,
                            VolInc3 = Convert.ToInt32(sheet.Range["K" + i].Value),
                            PrecoPrint3 = precoPrint3

                        };
                        retList.Add(servicoAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<RoleExcelP2C> GetListaParceiros(Sheets workSheets)
        {
            List<RoleExcelP2C> retList = new List<RoleExcelP2C>();
            retList.Add(new RoleExcelP2C { Id = string.Empty, Desc = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMParceiros))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        RoleExcelP2C parceiroToAdd = new RoleExcelP2C
                        {
                            Id = sheet.Range["A" + i].Value,
                            Desc = sheet.Range["B" + i].Value
                        };
                        retList.Add(parceiroToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<RoleExcelP2C> GetListaVendedores(Sheets workSheets)
        {
            List<RoleExcelP2C> retList = new List<RoleExcelP2C>();
            retList.Add(new RoleExcelP2C { Id = string.Empty, Desc = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMVendedores))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        RoleExcelP2C vendToAdd = new RoleExcelP2C
                        {
                            Id = sheet.Range["A" + i].Value,
                            Desc = sheet.Range["B" + i].Value
                        };
                        retList.Add(vendToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<RoleExcelP2C> GetPerfilAreas(Sheets workSheets)
        {
            List<RoleExcelP2C> retList = new List<RoleExcelP2C>();
            retList.Add(new RoleExcelP2C { Id = string.Empty, Desc = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMAreas))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        RoleExcelP2C perfilToAdd = new RoleExcelP2C
                        {
                            Id = sheet.Range["A" + i].Value,
                            Desc = sheet.Range["B" + i].Value
                        };
                        retList.Add(perfilToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelEquipamento> GetListaEquipamentos(Sheets workSheets)
        {

            List<ExcelEquipamento> retList = new List<ExcelEquipamento>();

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMEquipamentos))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        var listPrice = sheet.Range["D" + i].Value;
                        double price;
                        bool isDouble = Double.TryParse(listPrice, out price);
                        if (!isDouble)
                        {
                            price = Convert.ToDouble(listPrice.Replace(',', '.'));
                        }
                        ExcelEquipamento equipAdd = new ExcelEquipamento
                        {
                            idPart = Convert.ToInt32(sheet.Range["A" + i].Value),
                            modelCode = Convert.ToString(sheet.Range["C" + i].Value),
                            product = Convert.ToString(sheet.Range["B" + i].Value),
                            listPrice = price,
                            description = sheet.Range["E" + i].Value
                        };
                        retList.Add(equipAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelAddInVariables> GetPeriocidades(Sheets workSheets)
        {
            List<ExcelAddInVariables> retList = new List<ExcelAddInVariables>();
            retList.Add(new ExcelAddInVariables { id = 0, descricao = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMPeriocidade))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelAddInVariables varToAdd = new ExcelAddInVariables
                        {
                            id = Convert.ToInt32(sheet.Range["A" + i].Value),
                            descricao = sheet.Range["B" + i].Value
                        };
                        retList.Add(varToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelAddInVariables> GetCopias(Sheets workSheets)
        {
            List<ExcelAddInVariables> retList = new List<ExcelAddInVariables>();
            retList.Add(new ExcelAddInVariables { id = 0, descricao = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMCopias))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelAddInVariables varToAdd = new ExcelAddInVariables
                        {
                            id = Convert.ToInt32(sheet.Range["A" + i].Value),
                            descricao = sheet.Range["B" + i].Value
                        };
                        retList.Add(varToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelAddInVariables> GetCarencias(Sheets workSheets)
        {
            List<ExcelAddInVariables> retList = new List<ExcelAddInVariables>();
            retList.Add(new ExcelAddInVariables { id = 0, descricao = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMCarencia))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelAddInVariables varToAdd = new ExcelAddInVariables
                        {
                            id = Convert.ToInt32(sheet.Range["A" + i].Value),
                            descricao = sheet.Range["B" + i].Value
                        };
                        retList.Add(varToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelAddInVariables> GetEstadosProposta(Sheets workSheets)
        {
            List<ExcelAddInVariables> retList = new List<ExcelAddInVariables>();
            retList.Add(new ExcelAddInVariables { id = 0, descricao = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMEstadosProposta))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelAddInVariables varToAdd = new ExcelAddInVariables
                        {
                            id = Convert.ToInt32(sheet.Range["A" + i].Value),
                            descricao = sheet.Range["B" + i].Value
                        };
                        retList.Add(varToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelAddInVariables> GetTiposNegocio(Sheets workSheets)
        {
            List<ExcelAddInVariables> retList = new List<ExcelAddInVariables>();
            retList.Add(new ExcelAddInVariables { id = 0, descricao = string.Empty });

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMTiposNegocio))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelAddInVariables varToAdd = new ExcelAddInVariables
                        {
                            id = Convert.ToInt32(sheet.Range["A" + i].Value),
                            descricao = sheet.Range["B" + i].Value
                        };
                        retList.Add(varToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelNivelOperador> GetNiveisOperador(Sheets workSheets)
        {
            List<ExcelNivelOperador> retList = new List<ExcelNivelOperador>();

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDMNiveis))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelNivelOperador niveltoAdd = new ExcelNivelOperador
                        {
                            IdNivel = Convert.ToInt32(sheet.Range["A" + i].Value),
                            NivelRecurso = sheet.Range["B" + i].Value,
                            CustoMensal = sheet.Range["C" + i].Value

                        };
                        retList.Add(niveltoAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelTabelaFactores> GetListaFactores(Sheets workSheets)
        {
            List<ExcelTabelaFactores> retList = new List<ExcelTabelaFactores>();

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDmFactores))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelTabelaFactores factorToAdd = new ExcelTabelaFactores
                        {
                            factor = Convert.ToDouble(sheet.Range["A" + i].Value),
                            idTabela = Convert.ToInt32(sheet.Range["B" + i].Value),
                            tabela = sheet.Range["C" + i].Value

                        };
                        retList.Add(factorToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }

        public static List<ExcelTipoNegocioMenu> GetListaTiposNegocio(Sheets workSheets)
        {
            List<ExcelTipoNegocioMenu> retList = new List<ExcelTipoNegocioMenu>();

            foreach (Worksheet sheet in workSheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDmTipoNegocioMenu))
                {
                    int i = 1;

                    while (sheet.Range["A" + i].Value != null)
                    {
                        ExcelTipoNegocioMenu factorToAdd = new ExcelTipoNegocioMenu
                        {
                            idNegocio = Convert.ToInt32(sheet.Range["A" + i].Value),
                            descNegocio = sheet.Range["B" + i].Value,
                            idMenu = Convert.ToInt32(sheet.Range["C" + i].Value),
                            descSheet = sheet.Range["D" + i].Value
                        };
                        retList.Add(factorToAdd);
                        i++;
                    }

                }
            }

            return retList;
        }


    }
}
