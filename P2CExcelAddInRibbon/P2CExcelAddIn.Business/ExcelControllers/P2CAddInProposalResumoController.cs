﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpiritucSI.Xerox.Workflow.Business;
using P2CExcelAddIn.Business.Entities;

namespace P2CExcelAddIn.Business.ExcelControllers
{
    public static class P2CAddInProposalResumoController
    {

        public static PropostaResumo SetTotais(ProposalExcel proposta)
        {
            PropostaResumo retVal = new PropostaResumo();

            retVal.TotalEquipInstalar = GetTotalContratoInProposal(proposta);
            retVal.TotalServico = GetTotalServico(proposta);
            retVal.TotalRH = GetTotalRH(proposta);
            retVal.TotalSoftware = GetTotalSoftware(proposta);
            retVal.TotalOutrosServ = GetTotalOutrosServicos(proposta);
            retVal.TotalORS = retVal.TotalEquipInstalar + retVal.TotalSoftware;
            int duracao = proposta.duracaoContrato ?? 0;
            int period =proposta.periocidadeCod ?? 0;
            double divDuracao = 1;
            if (period != 0)
            {
                divDuracao = duracao / period;
            }
            retVal.TotalRenda = GetRenda(proposta, retVal.TotalServico, retVal.TotalRH, retVal.TotalOutrosServ);
            retVal.TotalContrato = retVal.TotalRenda * (divDuracao);

            return retVal;
        }


        private static double GetTotalContratoInProposal(ProposalExcel proposta)
        {
            var objs = proposta.equipamentosServicosList;

            double total = 0;
            if (objs != null)
            {
                if (objs.Count > 0)
                    total = (from obj in objs
                             select obj.precoLista * (1 - obj.desconto / 100) * obj.quantidade + obj.topUp).Sum();

                proposta.equipamentosRemoverList.ForEach(x => total += x.tipoRetiradaTopUp ? x.valor ?? 0 : 0);
            }

            return total ;
        }

        private static double GetTotalServico(ProposalExcel proposta)
        {
            var objs = proposta.equipamentosServicosList;

            double total = 0;
            if (objs != null)
            {
                if (objs.Count > 0)
                    total = (from obj in objs
                             select obj.total).Sum();
            }
            return total * proposta.duracaoContrato ?? 0;

        }

        private static double GetTotalRH(ProposalExcel proposta)
        {
            double TotalContrato = 0;

            var objs = proposta.recursosHumanosList;

            if (objs != null)
            {
                foreach (RecursosHumanosExcel it in objs)
                {
                    double upLifp = it.upLift??0;
                    double auxUpLift = 1 + (upLifp / 100);
                    TotalContrato += it.custoMensal * auxUpLift * it.quantidade * proposta.duracaoContrato ?? 0;
                }
            }
            return TotalContrato;
        }

        private static double GetTotalSoftware(ProposalExcel proposta)
        {
            double TotalContrato = 0;
            double anosContrato = 0;

            var objs = proposta.softwareList;


            if (objs != null)
            {
                foreach (SoftwareExcel it in objs)
                {
                    if ((proposta.duracaoContrato ?? 0) > 12)
                        anosContrato = (proposta.duracaoContrato ?? 0) / 12;
                    else
                        anosContrato = 1;
                    TotalContrato += it.preco * (1 - it.desconto / 100) * it.quantidade;
                }
            }
            return TotalContrato;
        }

        private static double GetTotalSoftwareSemManutencao(ProposalExcel proposta)
        {
            double TotalContrato = 0;

            var objs = proposta.softwareList;


            if (objs != null)
            {
                foreach (SoftwareExcel it in objs)
                {
                    TotalContrato += it.preco * (1 - it.desconto / 100) * it.quantidade;
                }
            }
            return TotalContrato;
        }

        private static double GetTotalSoftwareManutencao(ProposalExcel proposta)
        {
            double manutencao = 0;


            if (proposta.softwareList != null && proposta.softwareList.Count > 0)
            {

                int periodoContrato = (proposta.duracaoContrato.Value < 12) ? 12 : proposta.duracaoContrato ?? 0;
                manutencao = (from obj in proposta.softwareList
                              select (obj.manutencao * (1 - obj.manutencaoDesconto / 100)) * ((periodoContrato - (obj.primeiroAnoIncuido.GetValueOrDefault(false) ? 12 : 0)) / 12) * obj.quantidade).Sum();//12 * (periodoContrato - (obj.ManutPrimAnoIncluido.GetValueOrDefault(false) ? 12 : 0)) * obj.Quantidade * (1 - obj.DescontoManutencao.GetValueOrDefault(0) / 100)).Sum();

            }
            return manutencao;
        }


        private static double GetTotalOutrosServicos(ProposalExcel proposta)
        {
            double TotalContrato = 0;

            var objs = proposta.outrosServicosList;


            if (objs != null)
            {
                foreach (OutrosServicosExcel it in objs)
                {
                    TotalContrato += it.precoLista * (1 - it.desconto / 100) * it.quantidade;
                }
            }
            return TotalContrato;
        }

        private static double GetRenda(ProposalExcel proposta, double totServico, double totRh, double totOtherServ)
        {
            double totalRenda = 0;

            totalRenda += GetRendaEquipInstal(proposta);
            totalRenda += GetRendaServico(proposta, totServico);
            totalRenda += GetRendaSoftware(proposta);
            totalRenda += GetRendaRH(proposta, totRh);
            totalRenda += GetRendaOtherServ(proposta, totOtherServ);


            return totalRenda;
        }

        private static double GetRendaEquipInstal(ProposalExcel proposta)
        {
            double totalRenda = 0;
            double creditex = proposta.factorCreditex ?? 0;
            int period = proposta.periocidadeCod ?? 1;
            totalRenda = GetTotalAFinanciar(proposta, true) * creditex * period;

            return totalRenda;
        }

        private static double GetRendaServico(ProposalExcel proposta, double totalServico)
        {
            double renda = 0;


            double total = totalServico;

            renda = (total / ((proposta.duracaoContrato.HasValue && proposta.duracaoContrato.GetValueOrDefault() > 0) ? proposta.duracaoContrato.GetValueOrDefault() : 1));

            return renda * proposta.periocidadeCod.GetValueOrDefault(1);
        }

        private static double GetRendaSoftware(ProposalExcel proposta)
        {
            double renda = 0;



            double total = GetTotalSoftwareSemManutencao(proposta);
            double creditex = proposta.factorCreditex ?? 0;
            int period = proposta.periocidadeCod ?? 1;
            renda = total * creditex * period;
            renda += GetTotalSoftwareManutencao(proposta) / ((proposta.duracaoContrato.HasValue && proposta.duracaoContrato.GetValueOrDefault() > 0) ? proposta.duracaoContrato.GetValueOrDefault() : 1);

            return renda;
        }

        private static double GetRendaRH(ProposalExcel proposta, double totalRh)
        {
            double renda = 0;

            renda = (totalRh / ((proposta.duracaoContrato.HasValue && proposta.duracaoContrato.GetValueOrDefault() > 0) ? proposta.duracaoContrato.GetValueOrDefault() : 1));

            return renda * proposta.periocidadeCod.GetValueOrDefault(1);
        }

        private static double GetRendaOtherServ(ProposalExcel proposta, double totalOtherServ)
        {
            double renda = 0;


            renda = (totalOtherServ / ((proposta.duracaoContrato.HasValue && proposta.duracaoContrato.GetValueOrDefault() > 0) ? proposta.duracaoContrato.GetValueOrDefault() : 1));

            return renda * proposta.periocidadeCod.GetValueOrDefault(1);
        }



        private static double GetTotalAFinanciar(ProposalExcel proposta, bool includeRetoma)
        {
            double totalAFinanciar = 0;
            if (proposta != null)
            {


                if (proposta.equipamentosServicosList != null && proposta.equipamentosServicosList.Count > 0)
                    totalAFinanciar = (from obj in proposta.equipamentosServicosList
                                       select obj.precoLista * (1 - obj.desconto / 100) * obj.quantidade + obj.topUp).Sum();

                if (includeRetoma)
                {
                    proposta.equipamentosRemoverList.ForEach(x => totalAFinanciar += x.tipoRetiradaTopUp ? x.valor ?? 0 : 0);
                }
            }
            return totalAFinanciar;
        }
    }
}
