﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using P2CExcelAddIn.Business.ProposalP2CtoExcelAddIn;
using System.ServiceModel;
using System.ServiceModel.Channels;
using P2CExcelAddIn.Business.Properties;
using SpiritucSI.Xerox.Workflow.Business;

namespace P2CExcelAddIn.Business.ServicesController
{
    public class ProposalP2CtoExcelAddInController
    {
        public ProposalP2CtoExcelAddInClient service; 

        public ProposalP2CtoExcelAddInController()
        {
           
            Binding binding = BindingFactory.CreateInstance();
            EndpointAddress epa = new EndpointAddress(Settings.Default.ProposalServiceEndPoint);
            
            service = new ProposalP2CtoExcelAddInClient(binding, epa);
            //service.ClientCredentials.Windows.ClientCredential.UserName = Settings.Default.ServerUsername;
            //service.ClientCredentials.Windows.ClientCredential.Password = Settings.Default.ServerPass;
            //service.ClientCredentials.Windows.ClientCredential.Domain = Settings.Default.ServerDomain;
            
        }

        internal static class BindingFactory
        {
            internal static Binding CreateInstance()
            {
                WSHttpBinding binding = new WSHttpBinding();
                binding.MaxBufferPoolSize = 5242880;
                binding.MaxReceivedMessageSize = 655553600;
                binding.ReaderQuotas.MaxArrayLength = 1638400;
                binding.ReaderQuotas.MaxStringContentLength = 888192000;
                binding.ReaderQuotas.MaxBytesPerRead = 40960;
                binding.ReaderQuotas.MaxNameTableCharCount = 16384;
                binding.ReceiveTimeout = new TimeSpan(0,5000,0);
                binding.SendTimeout = new TimeSpan(0, 5000, 0);
                //Descomentar a seguinte linha para ambiente de QUA
                //binding.Security.Mode = SecurityMode.None;
                //3 Linhas seguintes para HTTPS
                binding.Security.Mode = SecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
               
                return binding;
            }

        }

        public string GetMasterData(string sessionKey, string macAddress)
        {
            
            string masterData = string.Empty;
            //ProposalP2CtoExcelAddInClient service = new ProposalP2CtoExcelAddInClient();
            
            masterData = service.GetMasterData(sessionKey, macAddress);

            return masterData;

        }

        public Status Login(string sessionKey, string macAddress)
        {
            Status retStatus = new  Status();
           // service = new ProposalP2CtoExcelAddIn.ProposalP2CtoExcelAddInClient();

            retStatus = service.Login(sessionKey, macAddress);

            return retStatus;
        }

        public ProposalReturnStatus CreateProposal(ProposalExcel proposalData, string sessionKey, bool wkDraft, string macAddress)
        {
            ProposalReturnStatus retStatus = new ProposalReturnStatus();
            //ProposalP2CtoExcelAddInClient service = new ProposalP2CtoExcelAddInClient();
            
            retStatus=service.CreateProposal(proposalData, sessionKey, wkDraft, macAddress);

            return retStatus;
        }

        public string GetProposal(string estadoProposta, string nomeCliente, int tipoNegocio, int idProposta, string numContribuinte, System.Nullable<bool> isCreditex, System.Nullable<System.DateTime> dataCriacaoIni, System.Nullable<System.DateTime> dataCriacaoFim, string UserVendedor, string sessionKey, string macAddress)
        {

            string retString = string.Empty;
               
            retString = service.GetProposal(estadoProposta, nomeCliente, tipoNegocio, idProposta, numContribuinte, isCreditex, dataCriacaoIni, dataCriacaoFim, UserVendedor, sessionKey, macAddress);

            return retString;
        
        }

        public string GetProposalState( string idProposta, string sessionKey, string macAddress)
        {
            string retString = string.Empty;
            //ProposalP2CtoExcelAddInClient service = new ProposalP2CtoExcelAddInClient();
 
            retString = service.GetProposalState( idProposta, sessionKey, macAddress);

            return retString;

        }

    }
}
