﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2CExcelAddIn.Business.Entities
{
    public class TipoNegocio
    {
        public int IdTipoNegocio { get; set; }
        public string DescTipoNegocio { get; set; }
    }
}
