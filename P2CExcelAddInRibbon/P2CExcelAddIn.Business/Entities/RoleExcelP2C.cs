﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2CExcelAddIn.Business.Entities
{
    public class RoleExcelP2C
    {
        public string Id { get; set; }
        public string Desc { get; set; }
    }
}
