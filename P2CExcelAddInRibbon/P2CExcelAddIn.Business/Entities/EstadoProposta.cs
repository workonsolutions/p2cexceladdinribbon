﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2CExcelAddIn.Business.Entities
{
    public class EstadoProposta
    {
        public int IdEstado { get; set; }
        public string DescEstado { get; set; }
    }
}
