﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2CExcelAddIn.Business.Entities
{
    public class PropostaGrid
    {
        public string Urgente { get; set; }
        public string NumProposta { get; set; }
        public string DataCriacao { get; set; }
        public string Cliente { get; set; }
        public string Vendedor { get; set; }
        public string TipoNegocio { get; set; }
        public string Estado { get; set; }
    }
}
