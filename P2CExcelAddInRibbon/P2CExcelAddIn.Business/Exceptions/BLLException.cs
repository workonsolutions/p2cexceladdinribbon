﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P2CExcelAddIn.Business
{
    /// <summary>
    /// TODO: 999 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
    /// </summary>
    public class BLLException : System.Exception
    {
        /// <summary>
        /// TODO: 999 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
        /// </summary>
        public BLLException() : base() { }
        /// <summary>
        ///  TODO: 999 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
        /// </summary>
        /// <param name="_message"></param>
        public BLLException(string _message) : base(_message) { }
        /// <summary>
        ///  TODO: 999 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
        /// </summary>
        /// <param name="_message"></param>
        /// <param name="_ex"></param>
        public BLLException(string _message, Exception _ex) : base(_message == "{message}" ? _ex.Message : _message, _ex) { }
    }
}