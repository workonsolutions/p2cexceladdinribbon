﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using P2CExcelAddInRibbon;
using System.Windows.Forms;
using P2CExcelAddInRibbon.Properties;
using System.Security.Principal;
using System.Reflection;
using System.Collections;
using System.Data;
using System.Collections.Specialized;
using Microsoft.Vbe.Interop.Forms; 
using System.Drawing;
using System.Data.SqlClient;
using P2CExcelAddIn.Business;
using SpiritucSI.Xerox.Workflow.Business;
using System.Net.NetworkInformation;
using P2CExcelAddIn.Business.ServicesController;
using P2CExcelAddIn.Business.ExcelControllers;
using P2CExcelAddIn.Business.Security;
using P2CExcelAddIn.Business.Entities;
using System.IO;


namespace ExcelAddIn
{
    public partial class P2CTool
    {
       
        private System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(P2CTool));
        private ListDictionary SheetChanged = new ListDictionary();
        private string SheetProtectionPassword = "!xerox2013";

        public string AppRoot { get 
        {
            SessionKeyRegistry registry = new SessionKeyRegistry();
            var fileLocation = registry.Read("P2CInstallLocation");
            return fileLocation;
        
        } }
        private string CurrentUserName { get { return WindowsIdentity.GetCurrent().Name; } }
        private WindowsIdentity CurrentUser { get { return WindowsIdentity.GetCurrent(); } }
        private Microsoft.Office.Interop.Excel.Application Excel_App { get { return Globals.ThisAddIn.Application; } }
        public Workbook Excel_Activebook { get; set; }
        private Worksheet Excel_ActiveSheet { get { return Excel_Activebook.ActiveSheet; } }
        private Microsoft.Office.Tools.Excel.Workbook Excel_ActivebookExt { get { return Globals.Factory.GetVstoObject(Excel_Activebook); } }
        private Microsoft.Office.Tools.Excel.Worksheet Excel_ActiveSheetExt { get { return Globals.Factory.GetVstoObject(Excel_Activebook.ActiveSheet); } }
        private int FreazeCount = 0;
  
        private string ChaveSessao = string.Empty;
        public bool FactorCreditexInError = true;


        #region Button Group Events

        private void buttonAdicionarEquipRem_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                //EquipRemoveForm formEquip = new EquipRemoveForm();

                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                long insertLine = sheet.Cells.Find("Totais Remover").Row;
                var fristLine = sheet.Range["AO3"].Value;
                long row = 0;


                Range Line = sheet.Range["AN3:AX3"];
                Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                //row = insertLine;
                //formEquip.ShowDialog();

                //var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                //long insertLine = sheet.Cells.Find("Totais Remover").Row;
                //var fristLine = sheet.Range["AN3"].Value;
                //long row = 0;

                //if (fristLine != null && fristLine != string.Empty)
                //{
                    //Range Line = sheet.Range["AN" + insertLine + ":AX" + insertLine + ""];
                    //Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    //row = insertLine;
                //}

                var rangeSel = sheet.get_Range("AN3", "AN3");
                rangeSel.Select();

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }

        private void buttonEliminarEquip_Click(object sender, RibbonControlEventArgs e)
        {
            ConfirmacaoForm confForm = new ConfirmacaoForm();

            confForm.msgToShow = "Tem a certeza que deseja remover o equipamento/Serviço seleccionado?";
            confForm.ShowDialog();

            if (confForm.confirmation)
                EliminarEquipamento();

        }

        private void buttonEliminarEquipRemover_Click(object sender, RibbonControlEventArgs e)
        {
            ConfirmacaoForm confForm = new ConfirmacaoForm();

            confForm.msgToShow = "Tem a certeza que deseja remover o equipamento a remover seleccionado?";
            confForm.ShowDialog();

            if (confForm.confirmation)
                EliminarEquipRemover();
        }

        private void buttonEliminarRH_Click(object sender, RibbonControlEventArgs e)
        {
            ConfirmacaoForm confForm = new ConfirmacaoForm();

            confForm.msgToShow = "Tem a certeza que deseja remover o recurso humano seleccionado?";
            confForm.ShowDialog();

            if (confForm.confirmation)
                EliminarRH();
        }

        private void buttonDelSoft_Click(object sender, RibbonControlEventArgs e)
        {
            ConfirmacaoForm confForm = new ConfirmacaoForm();

            confForm.msgToShow = "Tem a certeza que deseja remover o software seleccionado?";
            confForm.ShowDialog();

            if (confForm.confirmation)
                EliminarSoft();
        }

        private void buttonDelOutroServ_Click(object sender, RibbonControlEventArgs e)
        {
            ConfirmacaoForm confForm = new ConfirmacaoForm();

            confForm.msgToShow = "Tem a certeza que deseja remover o outro serviço seleccionado?";
            confForm.ShowDialog();

            if (confForm.confirmation)
                EliminarOutrosServ();
        }


        private void EliminarEquipamento()
        {
            try
            {
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                var selection = (Range)Globals.ThisAddIn.Application.Selection;
                long totaisLine = sheet.Cells.Find("Totais Instalar").Row;


                if (selection.Row > 2 && selection.Row < totaisLine)
                {
                    if (totaisLine != 4)
                    {
                        Range Line = sheet.Range["A" + selection.Row + ":AL" + selection.Row + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        
                        sheet.Range["A" + selection.Row].Value = "";
                        sheet.Range["B" + selection.Row].Value = "";
                        sheet.Range["C" + selection.Row].Value = "";
                        sheet.Range["D" + selection.Row].Value = "";
                        sheet.Range["E" + selection.Row].Value = "";
                        sheet.Range["G" + selection.Row].Value = "";
                        sheet.Range["H" + selection.Row].Value = "";
                        sheet.Range["I" + selection.Row].Value = "";
                        sheet.Range["K" + selection.Row].Value = "";
                        sheet.Range["L" + selection.Row].Value = "";
                        sheet.Range["M" + selection.Row].Value = "";
                        sheet.Range["N" + selection.Row].Value = "";
                        sheet.Range["O" + selection.Row].Value = "";
                        sheet.Range["P" + selection.Row].Value = "";
                        sheet.Range["Q" + selection.Row].Value = "";

                        sheet.Range["S" + selection.Row].Value = "";
                        sheet.Range["T" + selection.Row].Value = "";
                        sheet.Range["U" + selection.Row].Value = "";
                        sheet.Range["W" + selection.Row].Value = "";

                        sheet.Range["Y" + selection.Row].Value = "";
                        sheet.Range["Z" + selection.Row].Value = "";
                        sheet.Range["AA" + selection.Row].Value = "";
                        sheet.Range["AC" + selection.Row].Value = "";

                        sheet.Range["AE" + selection.Row].Value = "";
                        sheet.Range["AF" + selection.Row].Value = "";
                        sheet.Range["AG" + selection.Row].Value = "";
                        sheet.Range["AJ" + selection.Row].Value = "";
                        sheet.Range["AI" + selection.Row].Value = "";

                    }
                }

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }

        }

        private void EliminarEquipRemover()
        {
            try
            {
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);

                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                var selection = (Range)Globals.ThisAddIn.Application.Selection;
                long totaisLine = sheet.Cells.Find("Totais Remover").Row;


                if (selection.Row > 2 && selection.Row < totaisLine)
                {
                    if (totaisLine != 4)
                    {
                        Range Line = sheet.Range["AN" + selection.Row + ":AX" + selection.Row + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                        //Line.Delete();
                    }
                    else
                    {
                        //Range Line = (Range)sheet.Rows[selection.Row];
                        Range Line = sheet.Range["AN" + selection.Row + ":AW" + selection.Row + ""];
                        Line.Value = "";
                    }
                }

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }


        private void EliminarRH()
        {
            try
            {
                this.UnProtectSheet(Settings.Default.SheetRecursosHumanos);
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                var selection = (Range)Globals.ThisAddIn.Application.Selection;
                long totaisLine = sheet.Cells.Find("Totais").Row;


                if (selection.Row > 2 && selection.Row < totaisLine)
                {
                    if (totaisLine != 4)
                    {
                        Range Line = sheet.Range["A" + selection.Row + ":H" + selection.Row + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {

                        //Range Line = sheet.Range["A" + selection.Row + ":E" + selection.Row + ""];
                        //Line.Value = "";
                        sheet.Range["A" + selection.Row].Value = "";
                        sheet.Range["B" + selection.Row].Value = "";
                        sheet.Range["D" + selection.Row].Value = "";
                        sheet.Range["E" + selection.Row].Value = "";
                        sheet.Range["F" + selection.Row].Value = "";
                    }
                }
                this.ProtectSheet(Settings.Default.SheetRecursosHumanos);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }

        private void EliminarSoft()
        {
            try
            {
                this.UnProtectSheet(Settings.Default.SheetSoftware);
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                var selection = (Range)Globals.ThisAddIn.Application.Selection;
                long totaisLine = sheet.Cells.Find("Totais").Row;


                if (selection.Row > 2 && selection.Row < totaisLine)
                {
                    if (totaisLine != 4)
                    {
                        Range Line = sheet.Range["A" + selection.Row + ":M" + selection.Row + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A" + selection.Row].Value = "";
                        sheet.Range["B" + selection.Row].Value = "";
                        sheet.Range["D" + selection.Row].Value = "";
                        sheet.Range["E" + selection.Row].Value = "";
                        sheet.Range["G" + selection.Row].Value = "";
                        sheet.Range["I" + selection.Row].Value = "";
                        sheet.Range["J" + selection.Row].Value = "";
                        sheet.Range["K" + selection.Row].Value = "";
                    }
                }
                this.ProtectSheet(Settings.Default.SheetSoftware);
            }
            catch (Exception ex)
            { 
                HandleException(ex.Message);
            }
        }

        private void EliminarOutrosServ()
        {
            try
            {
                this.UnProtectSheet(Settings.Default.SheetOutrosServicos);
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                var selection = (Range)Globals.ThisAddIn.Application.Selection;
                long totaisLine = sheet.Cells.Find("Totais").Row;


                if (selection.Row > 2 && selection.Row < totaisLine)
                {
                    if (totaisLine != 4)
                    {
                        Range Line = sheet.Range["A" + selection.Row + ":G" + selection.Row + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A" + selection.Row].Value = "";
                        sheet.Range["B" + selection.Row].Value = "";
                        sheet.Range["D" + selection.Row].Value = "";
                        sheet.Range["E" + selection.Row].Value = "";

                    }
                }
                this.ProtectSheet(Settings.Default.SheetOutrosServicos);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }


        private void buttonAdicionarEquip_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                EquipInstalForm formPopup = new EquipInstalForm();

                List<ExcelEquipamento> listEquip = P2CAddInGenericListsController.GetListaEquipamentos(Excel_App.Worksheets);
                //List<ExcelServico> listserv = GetListaServicos();

                formPopup.DataSource_Equipamentos = listEquip;
                //formPopup.listServicos = listserv;
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                formPopup.ShowDialog();
                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }

        private void buttonAdicionarServico_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                ServicoForm formServico = new ServicoForm();
                List<ExcelServico> listserv = P2CAddInGenericListsController.GetListaServicos(Excel_App.Worksheets);
                List<ExcelEquipamento> listEquip = P2CAddInGenericListsController.GetListaEquipamentos(Excel_App.Worksheets);

                formServico.listServicos = listserv;
                formServico.listEquipamentos = listEquip;
                formServico.SetLists();

                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                formServico.ShowDialog();
                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }

        }

        private void buttonAdicionarRH_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                this.UnProtectSheet(Settings.Default.SheetRecursosHumanos);
                RHForm formRh = new RHForm();

                formRh.DataSource_Niveis = P2CAddInGenericListsController.GetNiveisOperador(Excel_App.Worksheets);

                formRh.ShowDialog();
                this.ProtectSheet(Settings.Default.SheetRecursosHumanos);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }

        }

        private void buttonAddSoft_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                SoftwareForm formSoft = new SoftwareForm();

                this.UnProtectSheet(Settings.Default.SheetSoftware);
                formSoft.ShowDialog();
                this.ProtectSheet(Settings.Default.SheetSoftware);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }

        private void buttonAddOutrosServ_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                OutrosServicosForm formOutrosServ = new OutrosServicosForm();

                this.UnProtectSheet(Settings.Default.SheetOutrosServicos);
                formOutrosServ.ShowDialog();
                this.ProtectSheet(Settings.Default.SheetOutrosServicos);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }



        #endregion

        #region Button Generic Events

        private void Btn_Login_Click(object sender, RibbonControlEventArgs e)
        {
            this.Connect();
        }

        private void SearchPropost_Click(object sender, RibbonControlEventArgs e)
        {
            this.Pesquisar();
        }

        private void buttonCriarProposta_Click(object sender, RibbonControlEventArgs e)
        {
            FreazeLayout();
            try
            {
                string File = AppRoot + Settings.Default.Template_ApplicationMenu;
                Excel_Activebook = Excel_App.Workbooks.Add(File);

                Excel_Activebook.SheetActivate += new WorkbookEvents_SheetActivateEventHandler(Excel_Activebook_SheetActivate);

                if (checkAddEvents())
                {
                    Excel_App.SheetChange += new AppEvents_SheetChangeEventHandler(Excel_Activebook_SheetChange);
                    Excel_App.SheetSelectionChange += new AppEvents_SheetSelectionChangeEventHandler(Excel_Activebook_SheetSelectionChange);
                }
                ActualizarResumoData();
            }
            catch (Exception ex)
            {
                //throw;
                HandleException(ex.Message);
            }
            finally 
            {
                UnFreazeLayout();
            }
        }

        private void SaveProposta_Click(object sender, RibbonControlEventArgs e)
        {

            FreazeLayout();
            try
            {
                UnProtectSheet(Settings.Default.SheetDadosGerais);

                ProposalExcel propValidate = GetProposalData();
                List<string> listErrors = new List<string>();
                bool isValid = P2CAddInValidationController.ValidateProposal(out listErrors, ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook, Excel_App.Worksheets, propValidate);

                MensagensForm formMsg = new MensagensForm();

                if (isValid)
                {
                    ProposalExcel proposalTosave = GetProposalData();

                    SavePropostaForm formSave = new SavePropostaForm();
                    formSave.macAddress = P2CAddinUtilsController.GetMACAddress();
                    formSave.encryptKey = SheetProtectionPassword;

                    if (proposalTosave.GuidProposta == string.Empty || proposalTosave.GuidProposta == null || proposalTosave.EstadoProposta == "Draft")
                    {

                        formSave.propostaToSend = proposalTosave;
                        formSave.ShowDialog();
                    }
                    else
                    {
                        formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                        formMsg.msgToShow = "O estado da proposta não permite o seu envio para o P2C";
                        formMsg.ShowDialog();
                    }
                }
                else
                {
                    string textToDisplay = "Dados Incorrectos:";

                    foreach (string errorMsg in listErrors)
                    {
                        textToDisplay += "\n" + " -" + errorMsg;
                    }
                    formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                    formMsg.msgToShow = textToDisplay;
                    formMsg.ShowDialog();
                }




                ProtectSheet(Settings.Default.SheetDadosGerais);
            }
            catch (Exception ex)
            {
                //throw;
                HandleException(ex.Message);
            }
            finally
            {
                UnFreazeLayout();
            }


        }

        private void buttonActualizar_Click(object sender, RibbonControlEventArgs e)
        {
            SessionKeyRegistry registry = new SessionKeyRegistry();
            ActualizacaoDados("Os dados mestre do template base vão ser actualizados, pretende também actualizar os dados mestre deste documento?(esta operação demorará alguns minutos)");
            
            registry.Write("DadosMestreData", DateTime.Now.ToString());
            labelDMData.Label = DateTime.Now.ToShortDateString();
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.ok_48;
            formMsg.msgToShow = "Os dados Mestre foram actualizados";
            formMsg.ShowDialog();
        }


        private void ActualizacaoDados(string message)
        {
            ConfirmacaoForm confForm = new ConfirmacaoForm();
            MensagensActForm messWaitForm = new MensagensActForm();

            confForm.msgToShow = message;
            confForm.ShowDialog();
            Excel_App.DisplayAlerts = false;

            if (confForm.confirmation)
            {
                messWaitForm.Show();
                ActualizarDadosMestre(Excel_App.ActiveWorkbook);

                Excel_App.ActiveWorkbook.Save();
                
            }

            if (!confForm.confirmation)
                messWaitForm.Show();

            string File = AppRoot + Settings.Default.Template_ApplicationMenu;
            Excel_Activebook = Excel_App.Workbooks.Add(File);

            ActualizarDadosMestre(Excel_Activebook);
            Excel_Activebook.SaveAs(File, XlFileFormat.xlOpenXMLTemplate);
            Excel_Activebook.Close();

            messWaitForm.Close();
            Excel_App.DisplayAlerts = true;

            
        }

        private void buttonValidarDadosProposta_Click(object sender, RibbonControlEventArgs e)
        {
             FreazeLayout();
             try
             {
                 ProposalExcel propValidate = GetProposalData();
                 List<string> listErrors = new List<string>();
                 bool isValid = P2CAddInValidationController.ValidateProposal(out listErrors, ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook, Excel_App.Worksheets, propValidate);

                 MensagensForm formMsg = new MensagensForm();

                 if (isValid)
                 {
                     formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.ok_48;
                     formMsg.msgToShow = "Dados Correctos, pode enviar a proposta";
                     formMsg.ShowDialog();
                 }
                 else
                 {
                     string textToDisplay = "Dados Incorrectos:";

                     foreach (string errorMsg in listErrors)
                     {
                         textToDisplay += "\n" + " -" + errorMsg;
                     }
                     formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                     formMsg.msgToShow = textToDisplay;
                     formMsg.ShowDialog();
                 }
             }
             catch (Exception ex)
             {
                 //throw;
                 HandleException(ex.Message);
             }
            finally
            {
                UnFreazeLayout();
            }

        }


        #endregion

        #region Excel Generic Events

        private void timer1_Tick(object sender, EventArgs e)
        {

            SessionKeyRegistry register = new SessionKeyRegistry();
            string sesskey = register.Read("ChaveSessao");
            string userNameToShow = register.Read("UserNameSessao");


            if (sesskey != null && register.ValidateKey(sesskey))
            {
                Btn_Login.Visible = false;
                buttonLoggedIn.Visible = true;
                buttonLoggedIn.Label = "Log-Off "+userNameToShow;
            }
            else
            {
                Btn_Login.Visible = true;
                buttonLoggedIn.Visible = false;
            }


            bool isTemplateLoaded = false;
            foreach (Worksheet sheet in Excel_App.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                    isTemplateLoaded = true;

            }

            if (isTemplateLoaded && (sesskey != null && register.ValidateKey(sesskey)))
            {
                this.pnlActualizacao.Visible = true;
                this.pnlProposta.Visible = true;
                
            }
            else
            {
                this.pnlActualizacao.Visible = false;
                this.pnlProposta.Visible = false;
                
            }

            if (isTemplateLoaded)
            {
                this.groupDadosResumo.Visible = true;
                setResumoData();

            }
            else
            {
                this.groupDadosResumo.Visible = false;
            }

            SessionKeyRegistry registry = new SessionKeyRegistry();
            var dataUltimaActualizacao = registry.Read("DadosMestreData");

            if (dataUltimaActualizacao != null)
            {
                DateTime datalastUpdate = Convert.ToDateTime(dataUltimaActualizacao);
                labelDMData.Label = datalastUpdate.ToShortDateString();
            }

            


        }

        private void P2CTool_Load(object sender, RibbonUIEventArgs e)
        {

            if (Excel_App.Workbooks.Count == 0)
            {

                Excel_Activebook = Excel_App.ActiveWorkbook;
                Excel_App.SheetActivate += new AppEvents_SheetActivateEventHandler(Excel_Activebook_SheetActivate);
                if (checkAddEvents())
                {
                    Excel_App.SheetChange += new AppEvents_SheetChangeEventHandler(Excel_Activebook_SheetChange);
                    Excel_App.SheetSelectionChange += new AppEvents_SheetSelectionChangeEventHandler(Excel_Activebook_SheetSelectionChange);
                }
                
            }

            SessionKeyRegistry registry = new SessionKeyRegistry();

            var dataUltimaActualizacao=registry.Read("DadosMestreData");

            if (dataUltimaActualizacao != null)
            {
                DateTime datalastUpdate = Convert.ToDateTime(dataUltimaActualizacao);
                labelDMData.Label = datalastUpdate.ToShortDateString();
            }

        }

        private void Excel_Activebook_SheetActivate(object Sh)
        {

            try
            {
                groupEquipServInstalar.Visible = false;
                groupRecursosHumanos.Visible = false;
                groupSoftware.Visible = false;
                groupOutrosServ.Visible = false;
                Worksheet s = (Worksheet)Sh;

                //editBoxTipoContrato.Text = "";

                switch (s.Name)
                {
                    case "EquipInstalarRem&Servicos":
                        groupEquipServInstalar.Visible = true;
                        break;
                    case "RecursosHumanos":
                        groupRecursosHumanos.Visible = true;
                        break;
                    case "Software":
                        groupSoftware.Visible = true;
                        break;
                    case "OutrosServicos":
                        groupOutrosServ.Visible = true;
                        break;
                }

            }
            catch (BLLException ex) { throw (ex); }
            catch (Exception ex) { throw (ex); }
        }

        void Excel_Activebook_SheetSelectionChange(object Sh, Range Target)
        {


            Worksheet s = (Worksheet)Sh;

            if (s.Name.Equals(Settings.Default.SheetDadosGerais))
            {
                Range tipoNegocio = s.Range["J10"];
                var tipoContrato = s.Range["P10"].Value;
                if (RangeIsEqual(tipoNegocio, Target))
                {
                    SessionKeyRegistry register = new SessionKeyRegistry();
                    register.Write("CheckIsSetProposal", "0");
                    this.UnProtectSheet(Settings.Default.SheetDadosGerais);
                    s.Range["B28"].Value = tipoNegocio.Value;
                    this.ProtectSheet(Settings.Default.SheetDadosGerais);
                }
            }

        }

        void Excel_Activebook_SheetChange(object Sh, Range Target)
        {
            Worksheet s = (Worksheet)Sh;

            Range tipoNegocio = s.Range["J10"];
            Range tabela = s.Range["P17"];
            Range fatorCreditex = s.Range["P18"];

            Range tipoContrato = s.Range["P10"];
            Range periocidade = s.Range["J15"];
            Range prazo = s.Range["J17"];
            string flagCanContinue = Convert.ToString(s.Range["B27"].Value);
            string tipoNegocioAnt = Convert.ToString(s.Range["B28"].Value);

            if (s.Name.Equals(Settings.Default.SheetDadosGerais))
            {
                if (RangeIsEqual(tipoNegocio, Target))
                {
                    tipoContrato.Value = string.Empty;
                    if (flagCanContinue == "Nao")
                    {
                        s.Range["B27"].Value = string.Empty;
                    }
                    else
                    {
                        SessionKeyRegistry register = new SessionKeyRegistry();
                        string isSetProposal= register.Read("CheckIsSetProposal");
                        var showPopup = true;

                        if (isSetProposal == "0") showPopup = false;

                        if (!showPopup)
                        {
                            //showPopup = ShowConfirmPopup(tipoNegocio.Value, tipoNegocioAnt);
                            showPopup = ShowConfirmPopupData(tipoNegocio.Value, tipoNegocioAnt);// && ShowConfirmPopup(tipoNegocio.Value, tipoNegocioAnt);
                        }


                        ConfirmacaoForm confForm = new ConfirmacaoForm();

                        if (showPopup)
                        {
                            if (isSetProposal == "1")
                            {
                                confForm.isPesquisa = true;
                            }
                            confForm.msgToShow = "Atenção, perderá dados da actual proposta, deseja continuar?";
                            confForm.ShowDialog();
                            register.Write("CheckIsProposalDuplicated", "0");
                        }
                        else
                        {
                            confForm.confirmation = true;
                        }

                        if (confForm.confirmation)
                        {
                            this.UnProtectSheet(Settings.Default.SheetDadosGerais);
                            s.Range["B27"].Value = "Sim";
                            s.Range["B29"].Value = "Sim";
                            s.Range["B28"].Value = tipoNegocio.Value;
                            if (confForm.isDuplicated)
                            {
                                register.Write("CheckIsProposalDuplicated", "1");
                            }
                            this.ProtectSheet(Settings.Default.SheetDadosGerais);
                            SetVisibilityTabs();
                        }
                        else
                        {
                            this.UnProtectSheet(Settings.Default.SheetDadosGerais);
                            s.Range["B27"].Value = "Nao";
                            s.Range["B29"].Value = "Nao";
                            //string tipoNegocioAnt = Convert.ToString(s.Range["B28"].Value);
                            tipoNegocio.Value = tipoNegocioAnt == null ? string.Empty : tipoNegocioAnt.Trim();
                            s.Range["B28"].Value = tipoNegocio.Value;
                            this.ProtectSheet(Settings.Default.SheetDadosGerais);
                        }
                    }

                    

                }

                if (RangeIsEqual(tabela, Target))
                {
                    CheckTabelaFactores();
                }

                if (RangeIsEqual(fatorCreditex, Target))
                {
                    CheckTabelaFactores();
                    setResumoData();
                    ActualizarResumoData();

                }

                if (RangeIsEqual(tipoContrato, Target))
                {
                    setResumoData();
                }

                if (RangeIsEqual(periocidade, Target) || RangeIsEqual(prazo, Target))
                {
                    setResumoData();
                    ActualizarResumoData();
                }
            }

            if (s.Name.Equals(Settings.Default.SheetEquipInstalar) || s.Name.Equals(Settings.Default.SheetRecursosHumanos)
                 || s.Name.Equals(Settings.Default.SheetServicos) || s.Name.Equals(Settings.Default.SheetSoftware)
                 || s.Name.Equals(Settings.Default.SheetOutrosServicos))
            {
                ActualizarResumoData();
            }


        }


        public bool ShowConfirmPopup(string tipoNegocio, string tipoNegocioAnt)
        {
            string[] tipoNegocioCod = tipoNegocio.Split('-');
            int codigoTipoNegocio = Convert.ToInt32(tipoNegocioCod[0].Trim());
            string[] tipoNegocioAntCod = tipoNegocioAnt.Split('-');
            int codigoTipoNegocioAnt = Convert.ToInt32(tipoNegocioAntCod[0].Trim());
            List<ExcelTipoNegocioMenu> listMenu = new List<ExcelTipoNegocioMenu>();
            List<ExcelTipoNegocioMenu> listNegocios = new List<ExcelTipoNegocioMenu>();
            List<ExcelTipoNegocioMenu> listNegociosAnt = new List<ExcelTipoNegocioMenu>();
            List<int> listNegociosCmp = new List<int>();
            List<int> listNegociosAntCmp = new List<int>();

            listMenu = P2CAddInGenericListsController.GetListaTiposNegocio(Excel_App.Worksheets);
            listNegocios = listMenu.FindAll(x => x.idNegocio == codigoTipoNegocio);
            listNegociosAnt = listMenu.FindAll(x => x.idNegocio == codigoTipoNegocioAnt);
            listNegocios.ForEach(a => listNegociosCmp.Add(a.idMenu));
            listNegociosAnt.ForEach(a => listNegociosAntCmp.Add(a.idMenu));

            var areEquivalent = !listNegociosCmp.Except(listNegociosAntCmp).Union(listNegociosAntCmp.Except(listNegociosCmp)).Any();

            return !areEquivalent;
        }

        public bool ShowConfirmPopupData(string tipoNegocio, string tipoNegocioAnt)
        {
            bool workBookEmpty = true;

            if (ShowConfirmPopup(tipoNegocio, tipoNegocioAnt))
            {
                string[] tipoNegocioCod = tipoNegocio.Split('-');
                int codigoTipoNegocio = Convert.ToInt32(tipoNegocioCod[0].Trim());
                List<ExcelTipoNegocioMenu> listMenu = new List<ExcelTipoNegocioMenu>();
                List<ExcelTipoNegocioMenu> listNegocios = new List<ExcelTipoNegocioMenu>();
                listMenu = P2CAddInGenericListsController.GetListaTiposNegocio(Excel_App.Worksheets);
                listNegocios = listMenu.FindAll(x => x.idNegocio == codigoTipoNegocio);

                foreach (Worksheet sheet in Excel_App.Worksheets)
                {
                    List<ExcelTipoNegocioMenu> listCheck = new List<ExcelTipoNegocioMenu>();

                    if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                    {
                        //long insertLine = sheet.Cells.Find("Totais Instalar").Row;
                        //var totais = sheet.Range["J" + insertLine].Value;
                        //if (totais > 0)
                        //    workBookEmpty = false;

                    }

                    if (sheet.Name.Equals(Settings.Default.SheetRecursosHumanos))
                    {
                        listCheck = listNegocios.FindAll(x => x.descSheet.Equals(Settings.Default.SheetRecursosHumanos));
                        if (listCheck.Count == 0)
                        {
                            long insertLine = sheet.Cells.Find("Totais").Row;
                            var totais = sheet.Range["E" + insertLine].Value;
                            if (totais > 0)
                                workBookEmpty = false;
                        }
                    }

                    if (sheet.Name.Equals(Settings.Default.SheetSoftware))
                    {
                         listCheck = listNegocios.FindAll(x => x.descSheet.Equals(Settings.Default.SheetSoftware));
                         if (listCheck.Count == 0)
                         {
                             long insertLine = sheet.Cells.Find("Totais").Row;
                             var totais = sheet.Range["B" + insertLine].Value;
                             if (totais > 0)
                                 workBookEmpty = false;
                         }
                    }

                    if (sheet.Name.Equals(Settings.Default.SheetOutrosServicos))
                    {
                        listCheck = listNegocios.FindAll(x => x.descSheet.Equals(Settings.Default.SheetOutrosServicos));
                        if (listCheck.Count == 0)
                        {
                            long insertLine = sheet.Cells.Find("Totais").Row;
                            var totais = sheet.Range["B" + insertLine].Value;
                            if (totais > 0)
                                workBookEmpty = false;
                        }
                    }

                }
            }


            return !workBookEmpty;
        }

        


        private void P2CTool_LoadImage(object sender, RibbonLoadImageEventArgs e)
        {
            string a = "fef";
        }

        public void FreazeLayout()
        {
            Excel_App.ScreenUpdating = false;
            ++FreazeCount;
        }

        public void UnFreazeLayout()
        {
            --FreazeCount;
            Excel_App.ScreenUpdating = FreazeCount == 0;
        }



        #endregion

        #region METHODS

        public bool checkAddEvents()
        {
            bool retVal = true;
            int count = 0;
            foreach (Workbook wk in Excel_App.Workbooks)
            {
                foreach (Worksheet sheet in wk.Worksheets)
                {
                    if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                        count++;

                }
            }

            if (count > 1)
                retVal = false;
            
            return retVal;
        }

        public bool CheckTabelaFactores()
        {
            bool retVal = false;
            List<ExcelTabelaFactores> listFactores = new List<ExcelTabelaFactores>();
            listFactores =P2CAddInGenericListsController.GetListaFactores(Excel_App.Worksheets);
            MensagensForm formMsg = new MensagensForm();

            foreach (Worksheet sheet in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                {


                    if (sheet.Range["P18"].Value != null && sheet.Range["P17"].Value != null)
                    {
                        string tabela = Convert.ToString(sheet.Range["P17"].Value);
                        string fatorCreditex = Convert.ToString(sheet.Range["P18"].Value);
                        double fatorCreditexVal = 0.0D;
                        bool isfatorCreditex = double.TryParse(fatorCreditex.Replace('.', ','), out fatorCreditexVal);

                        if (isfatorCreditex)
                        {
                            double minFact = 0.0D;
                            double maxFact = 0.0D;
                            var listTabelaFact = listFactores.Where(x => x.tabela.Equals(tabela.Trim()));
                            listTabelaFact = listTabelaFact.Where(x => x.factor > 0);
                            if (listTabelaFact != null && listTabelaFact.Count() > 0)
                            {
                                minFact = listTabelaFact.Min(x => x.factor);
                                maxFact = listTabelaFact.Max(x => x.factor);
                                if (fatorCreditexVal >= minFact && fatorCreditexVal <= maxFact)
                                {
                                    FactorCreditexInError = false;
                                }
                                else
                                {
                                    FactorCreditexInError = true;
                                    formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                                    formMsg.msgToShow = "Factor Creditex não está no intervalo da tabela seleccionada: " + minFact.ToString() + "..." + maxFact.ToString();
                                    formMsg.ShowDialog();
                                }
                            }
                        }
                        else
                        {
                            FactorCreditexInError = true;
                            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                            formMsg.msgToShow = "Factor Creditex Inválido";
                            formMsg.ShowDialog();

                        }
                    }





                }
            }


            return retVal;
        }

        private ProposalExcel GetProposalData()
        {
            ProposalExcel proposal = new ProposalExcel();



            foreach (Worksheet sheet in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
            {


                if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                {

                    List<ExcelAddInVariables> valueNegociosList = new List<ExcelAddInVariables>();
                    valueNegociosList =P2CAddInGenericListsController.GetTiposNegocio(Excel_App.Worksheets);

                    List<ExcelAddInVariables> valueTipoContratoList = new List<ExcelAddInVariables>();


                    var nomeCliente = sheet.Range["C9"].Value;
                    var numContribuinte = sheet.Range["C10"].Value;
                    var emailCliente = sheet.Range["C11"].Value;
                    var webSiteCliente = sheet.Range["C12"].Value;
                    var moradaCliente = sheet.Range["C14"].Value;
                    var codigoPostalCliente = sheet.Range["C15"].Value;
                    var codigoPostalCliente3 = sheet.Range["E15"].Value;
                    var localidadeCliente = sheet.Range["C16"].Value;
                    var telefoneCliente = sheet.Range["C17"].Value;

                    string perfilArea = sheet.Range["J9"].Value;
                    string tipoNegocio = sheet.Range["J10"].Value;
                    string parceiro = sheet.Range["P9"].Value;
                    string tipoContrato = sheet.Range["P10"].Value;


                    bool haveClausuladoStandard = false;
                    if (sheet.Range["P11"].Value == "Sim")
                    {
                        haveClausuladoStandard = true;
                    }

                    var periocidade = sheet.Range["J15"].Value;
                    var copia = sheet.Range["P15"].Value;
                    var carencia = sheet.Range["J16"].Value;
                    var nbid = sheet.Range["P16"].Value;
                    var duracaoContrato = sheet.Range["J17"].Value;

                    bool haveAdc = false;
                    if (sheet.Range["J18"].Value == "Sim")
                    {
                        haveAdc = true;
                    }

                    var tabelaCred = sheet.Range["P17"].Value;
                    
                    string fatorCreditex = Convert.ToString(sheet.Range["P18"].Value);
                    double fatorCreditexVal = 0.0D;
                    bool isfatorCreditex = false;
                    if (fatorCreditex != null)
                    {
                        isfatorCreditex = double.TryParse(fatorCreditex.Replace('.', ','), out fatorCreditexVal);
                    }
                    

                    bool temCreditex = false;
                    if (sheet.Range["J19"].Value == "Sim")
                    {
                        temCreditex = true;
                    }

                    bool urgente = false;
                    if (sheet.Range["J20"].Value == "Sim")
                    {
                        urgente = true;
                    }

                    proposal.NumProposta = Convert.ToInt32(sheet.Range["C4"].Value);
                    proposal.EstadoProposta = sheet.Range["C5"].Value;
                    proposal.GuidProposta = sheet.Range["C21"].Value;
                    proposal.nomeCliente = nomeCliente;
                    proposal.numContribuinteCliente = numContribuinte;
                    proposal.emailCliente = emailCliente;
                    proposal.websiteCliente = webSiteCliente;
                    proposal.moradaCliente = moradaCliente;
                    proposal.codigoPostalCliente = codigoPostalCliente;
                    proposal.codigoPostalCliente3 = codigoPostalCliente3;
                    proposal.localidadeCliente = localidadeCliente;
                    proposal.telefoneCliente = telefoneCliente;
                    proposal.clausuradoStandard = haveClausuladoStandard;
                    proposal.comAdc = haveAdc;
                    proposal.temCreditex = temCreditex;
                    proposal.nBid = nbid;
                    proposal.duracaoContrato = Convert.ToInt32(duracaoContrato);
                    proposal.tipoContratoDesc = tipoContrato;
                    proposal.Urgente = urgente;
                    proposal.factorCreditex = (isfatorCreditex)?fatorCreditexVal:0.0D;
                    proposal.tabelaFactoresCod = tabelaCred;

                    string[] tipoNegocioCod = tipoNegocio.Split('-');
                    proposal.tipoNegocioCod = Convert.ToInt32(tipoNegocioCod[0].Trim());

                    List<RoleExcelP2C> listaPerfilAreas = P2CAddInGenericListsController.GetPerfilAreas(Excel_App.Worksheets);

                    if (perfilArea != null && perfilArea != string.Empty)
                    {
                        var perfilAreaEle = listaPerfilAreas.Find(s => s.Desc == perfilArea);
                        if (perfilAreaEle != null) perfilArea = proposal.perfilAreaCod = perfilAreaEle.Id;
                    }

                    List<ExcelAddInVariables> listaPeriocidades = P2CAddInGenericListsController.GetPeriocidades(Excel_App.Worksheets);

                    if (periocidade != null && periocidade != string.Empty)
                    {
                        var periocidadeEle = listaPeriocidades.Find(s => s.descricao == periocidade);
                        if (periocidadeEle != null) proposal.periocidadeCod = periocidadeEle.id;
                    }

                    List<ExcelAddInVariables> listaCopias =P2CAddInGenericListsController.GetCopias(Excel_App.Worksheets);

                    if (copia != null && copia != string.Empty)
                    {
                        var copiaEle = listaCopias.Find(s => s.descricao == copia);
                        if (copiaEle != null) proposal.periodoLeituraCopiasCod = copiaEle.id;
                    }

                    List<ExcelAddInVariables> listaCarencias = P2CAddInGenericListsController.GetCarencias(Excel_App.Worksheets);

                    if (carencia != null && carencia != string.Empty)
                    {
                        var carenciaEle = listaCarencias.Find(s => s.descricao == carencia);
                        if (carenciaEle != null) proposal.carenciaCod = carenciaEle.id;
                    }

                    List<RoleExcelP2C> listaParceiros = P2CAddInGenericListsController.GetListaParceiros(Excel_App.Worksheets);

                    if (parceiro != null && parceiro != string.Empty)
                    {
                        var parceiroEle = listaParceiros.Find(s => s.Desc == parceiro);
                        if (parceiroEle != null) proposal.parceiroCod = parceiroEle.Id;
                    }


                }

                if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                {
                    string[] columnMapping = new string[] { "tipoEquipamentoDesc", "codigo", "modelo", "quantidade", "precoLista", "desconto", "margem", "", "", "idPart", "topUp", "descricao", "GuidEquipInst", "", "TFMreferenciaServico", "TFMdescontoServico", "", "VolumeRefCorServico", "VolumeCliCorServico", "PrecoExtCorServico", "DescontoCorServico", "", "VolumeRefPBServico", "VolumeCliPBServico", "PrecoExtPBServico", "DescontoPBServico", "", "VolumeRef3CServico", "VolumeCli3CServico", "PrecoExt3CServico", "Desconto3CServico", "", "" };
                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);
                    List<EquipamentosInstalarServicoExcel> listEquipInstalar = new List<EquipamentosInstalarServicoExcel>();
                    //System.Data.DataTable dt = ((System.Data.DataView)SheetObject.DataSource).Table;

                    foreach (ListRow row in SheetObject.ListRows)
                    {
                        int index = row.Index + 2;

                        if (sheet.Range["A" + index].Value != null && sheet.Range["A" + index].Value.ToString() != string.Empty)
                        {
                            var tipoEquipamentoId = 0;
                            if (sheet.Range["A" + index].Value.ToString().Trim().Equals("Equip. Cliente"))
                                tipoEquipamentoId = 1;
                            if (sheet.Range["A" + index].Value.ToString().Trim().Equals("3rd Party"))
                                tipoEquipamentoId = 2;

                            int? idPart = null;
                            if (sheet.Range["K" + index].Value != null && sheet.Range["K" + index].Value.ToString() != string.Empty)
                                idPart = Convert.ToInt32(sheet.Range["K" + index].Value);
                            EquipamentosInstalarServicoExcel rowToAdd = new EquipamentosInstalarServicoExcel()
                            {
                                tipoEquipamentoDesc = (sheet.Range["A" + index].Value != null) ? sheet.Range["A" + index].Value.ToString() : string.Empty,
                                tipoEquipamento = tipoEquipamentoId,
                                codigo = (sheet.Range["B" + index].Value != null) ? sheet.Range["B" + index].Value.ToString() : string.Empty,
                                modelo = (sheet.Range["C" + index].Value != null) ? sheet.Range["C" + index].Value.ToString() : string.Empty,
                                quantidade = Convert.ToInt32((sheet.Range["D" + index].Value != null && sheet.Range["D" + index].Value.ToString() != string.Empty) ? sheet.Range["D" + index].Value : "0"),
                                precoLista = Convert.ToDouble((sheet.Range["E" + index].Value != null && sheet.Range["E" + index].Value.ToString() != string.Empty) ? sheet.Range["E" + index].Value : "0"),
                                desconto = Convert.ToDouble((sheet.Range["F" + index].Value != null && sheet.Range["F" + index].Value.ToString() != string.Empty) ? sheet.Range["F" + index].Value : "0"),
                                margem = Convert.ToDouble((sheet.Range["G" + index].Value != null && sheet.Range["G" + index].Value.ToString() != string.Empty) ? sheet.Range["G" + index].Value : "0"),
                               //H is3party
                                idPart = idPart,
                                topUp = Convert.ToDouble((sheet.Range["L" + index].Value != null && sheet.Range["L" + index].Value.ToString() != string.Empty) ? sheet.Range["L" + index].Value : "0"),
                                descricao = (sheet.Range["M" + index].Value != null) ? sheet.Range["M" + index].Value.ToString() : string.Empty,
                                GuidEquipInst = (sheet.Range["N" + index].Value != null) ? sheet.Range["N" + index].Value.ToString() : string.Empty,
                                TFMreferenciaServico = Convert.ToDouble((sheet.Range["P" + index].Value != null && sheet.Range["P" + index].Value.ToString() != string.Empty) ? sheet.Range["P" + index].Value : "0"),
                                TFMdescontoServico = Convert.ToDouble((sheet.Range["Q" + index].Value != null && sheet.Range["Q" + index].Value.ToString() != string.Empty) ? sheet.Range["Q" + index].Value : "0"),
                                VolumeRefCorServico = Convert.ToInt32((sheet.Range["S" + index].Value != null && sheet.Range["S" + index].Value.ToString() != string.Empty) ? sheet.Range["S" + index].Value : "0"),
                                VolumeCliCorServico = Convert.ToInt32((sheet.Range["T" + index].Value != null && sheet.Range["T" + index].Value.ToString() != string.Empty) ? sheet.Range["T" + index].Value : "0"),
                                PrecoExtCorServico = Convert.ToDouble((sheet.Range["U" + index].Value != null && sheet.Range["U" + index].Value.ToString() != string.Empty) ? sheet.Range["U" + index].Value : "0"),
                                DescontoCorServico = Convert.ToDouble((sheet.Range["V" + index].Value != null && sheet.Range["V" + index].Value.ToString() != string.Empty) ? sheet.Range["V" + index].Value : "0"),
                                VolumeRefPBServico = Convert.ToInt32((sheet.Range["Y" + index].Value != null && sheet.Range["Y" + index].Value.ToString() != string.Empty) ? sheet.Range["Y" + index].Value : "0"),
                                VolumeCliPBServico = Convert.ToInt32((sheet.Range["Z" + index].Value != null && sheet.Range["Z" + index].Value.ToString() != string.Empty) ? sheet.Range["Z" + index].Value : "0"),
                                PrecoExtPBServico = Convert.ToDouble((sheet.Range["AA" + index].Value != null && sheet.Range["AA" + index].Value.ToString() != string.Empty) ? sheet.Range["AA" + index].Value : "0"),
                                DescontoPBServico = Convert.ToDouble((sheet.Range["AB" + index].Value != null && sheet.Range["AB" + index].Value.ToString() != string.Empty) ? sheet.Range["AB" + index].Value : "0"),
                                VolumeRef3CServico = Convert.ToInt32((sheet.Range["AE" + index].Value != null && sheet.Range["AE" + index].Value.ToString() != string.Empty) ? sheet.Range["AE" + index].Value : "0"),
                                VolumeCli3CServico = Convert.ToInt32((sheet.Range["AF" + index].Value != null && sheet.Range["AF" + index].Value.ToString() != string.Empty) ? sheet.Range["AF" + index].Value : "0"),
                                PrecoExt3CServico = Convert.ToDouble((sheet.Range["AG" + index].Value != null && sheet.Range["AG" + index].Value.ToString() != string.Empty) ? sheet.Range["AG" + index].Value : "0"),
                                Desconto3CServico = Convert.ToDouble((sheet.Range["AH" + index].Value != null && sheet.Range["AH" + index].Value.ToString() != string.Empty) ? sheet.Range["AH" + index].Value : "0"),
                                GuidPagePack = (sheet.Range["AI" + index].Value != null) ? sheet.Range["AI" + index].Value.ToString() : string.Empty,
                                total = Convert.ToDouble((sheet.Range["AL" + index].Value != null && sheet.Range["AL" + index].Value.ToString() != string.Empty) ? sheet.Range["AL" + index].Value : "0")
                            };

                            if (rowToAdd.PrecoExtCorServico == 0)
                            {
                                rowToAdd.VolumeCliCorServico = 0;
                                rowToAdd.DescontoCorServico = 0;
                            }
                            if (rowToAdd.PrecoExtPBServico == 0)
                            {
                                rowToAdd.VolumeCliPBServico = 0;
                                rowToAdd.DescontoPBServico = 0;
                            }
                            if (rowToAdd.PrecoExt3CServico == 0)
                            {
                                rowToAdd.VolumeCli3CServico = 0;
                                rowToAdd.Desconto3CServico = 0;
                            }

                            listEquipInstalar.Add(rowToAdd);
                        }

                    }

                    Microsoft.Office.Tools.Excel.ListObject SheetObjectRem = Globals.Factory.GetVstoObject(sheet.ListObjects[2]);
                    List<EquipamentosRemoverExcel> equipRemover = new List<EquipamentosRemoverExcel>();

                    long lastLine = sheet.Cells.Find("Totais Remover").Row;

                    for (int index=3;index<lastLine;index++)
                    {
                        //int index = row.Index + 2;
                        if (sheet.Range["AR" + index].Value != null && sheet.Range["AR" + index].Value.ToString() != string.Empty)
                        {
                            EquipamentosRemoverExcel rowToAdd = new EquipamentosRemoverExcel()
                            {
                                NumSerie = (sheet.Range["AO" + index].Value != null) ? sheet.Range["AO" + index].Value.ToString() : string.Empty,
                                Modelo = (sheet.Range["AP" + index].Value != null) ? sheet.Range["AP" + index].Value.ToString() : string.Empty,
                                tipoRetirada = (sheet.Range["AQ" + index].Value != null) ? sheet.Range["AQ" + index].Value.ToString() : string.Empty,
                                valor = Convert.ToDouble((sheet.Range["AR" + index].Value != null && sheet.Range["AR" + index].Value.ToString() != string.Empty) ? sheet.Range["AR" + index].Value : "0"),
                                is3Party = (sheet.Range["AS" + index].Value != null && sheet.Range["AS" + index].Value.ToString() != string.Empty) ? (sheet.Range["AS" + index].Value.ToString() == "Não") ? false : true : false,
                                dataInicioContrato = (sheet.Range["AT" + index].Value != null && sheet.Range["AT" + index].Value.ToString() != string.Empty) ? Convert.ToDateTime(sheet.Range["AT" + index].Value) : null,
                                dataFimContrato = (sheet.Range["AU" + index].Value != null && sheet.Range["AU" + index].Value.ToString() != string.Empty) ? Convert.ToDateTime(sheet.Range["AU" + index].Value) : null,
                                dataTermoContrato = (sheet.Range["AV" + index].Value != null && sheet.Range["AV" + index].Value.ToString() != string.Empty) ? Convert.ToDateTime(sheet.Range["AV" + index].Value) : null,
                                observacoes = (sheet.Range["AW" + index].Value != null) ? sheet.Range["AW" + index].Value.ToString() : string.Empty,
                            };

                            rowToAdd.tipoRetiradaRetoma=false;
                            rowToAdd.tipoRetiradaTopUp = false;

                            if (rowToAdd.tipoRetirada.Equals("TopUp"))
                                rowToAdd.tipoRetiradaTopUp = true;
                            else
                                rowToAdd.tipoRetiradaRetoma = true;

                            if (rowToAdd.valor!=null && rowToAdd.valor>0)
                                equipRemover.Add(rowToAdd);
                        }
                    }

                    proposal.equipamentosServicosList = listEquipInstalar;
                    proposal.equipamentosRemoverList = equipRemover;
                }

                if (sheet.Name.Equals(Settings.Default.SheetRecursosHumanos))
                {
                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);
                    List<RecursosHumanosExcel> recurosList = new List<RecursosHumanosExcel>();
                    var niveis = P2CAddInGenericListsController.GetNiveisOperador(Excel_App.Worksheets);

                    foreach (ListRow row in SheetObject.ListRows)
                    {
                        int index = row.Index + 2;
                        if (sheet.Range["A" + index].Value != null && sheet.Range["A" + index].Value.ToString() != string.Empty)
                        {
                            var nivelEle = niveis.Find(s => s.NivelRecurso == sheet.Range["A" + index].Value.ToString());
                            RecursosHumanosExcel rowToAdd = new RecursosHumanosExcel()
                            {
                                nivelOperador = (nivelEle != null) ? nivelEle.IdNivel : 0,
                                nivelOperadorDesc = (nivelEle != null) ? nivelEle.NivelRecurso : string.Empty,
                                descricao = (sheet.Range["B" + index].Value != null) ? sheet.Range["B" + index].Value.ToString() : string.Empty,
                                upLift = Convert.ToDouble((sheet.Range["C" + index].Value != null && sheet.Range["C" + index].Value.ToString() != string.Empty) ? sheet.Range["C" + index].Value : "0"),
                                quantidade = Convert.ToInt32((sheet.Range["D" + index].Value != null && sheet.Range["D" + index].Value.ToString() != string.Empty) ? sheet.Range["D" + index].Value : "0"),
                                custoMensal = Convert.ToDouble((sheet.Range["E" + index].Value != null && sheet.Range["E" + index].Value.ToString() != string.Empty) ? sheet.Range["E" + index].Value : "0"),
                            };

                            recurosList.Add(rowToAdd);
                        }

                    }
                    proposal.recursosHumanosList = recurosList;
                }

                if (sheet.Name.Equals(Settings.Default.SheetSoftware))
                {
                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);
                    List<SoftwareExcel> listSofware = new List<SoftwareExcel>();

                    foreach (ListRow row in SheetObject.ListRows)
                    {
                        int index = row.Index + 2;
                        if (sheet.Range["A" + index].Value != null && sheet.Range["A" + index].Value.ToString() != string.Empty)
                        {
                            SoftwareExcel rowToAdd = new SoftwareExcel()
                            {
                                designacao = (sheet.Range["A" + index].Value != null) ? sheet.Range["A" + index].Value.ToString() : string.Empty,
                                preco = Convert.ToDouble((sheet.Range["B" + index].Value != null && sheet.Range["B" + index].Value.ToString() != string.Empty) ? sheet.Range["B" + index].Value : "0"),
                                desconto = Convert.ToDouble((sheet.Range["C" + index].Value != null && sheet.Range["C" + index].Value.ToString() != string.Empty) ? sheet.Range["C" + index].Value : "0"),
                                quantidade = Convert.ToInt32((sheet.Range["E" + index].Value != null && sheet.Range["E" + index].Value.ToString() != string.Empty) ? sheet.Range["E" + index].Value : "0"),
                                manutencao = Convert.ToDouble((sheet.Range["G" + index].Value != null && sheet.Range["G" + index].Value.ToString() != string.Empty) ? sheet.Range["G" + index].Value : "0"),
                                manutencaoDesconto = Convert.ToDouble((sheet.Range["H" + index].Value != null && sheet.Range["H" + index].Value.ToString() != string.Empty) ? sheet.Range["H" + index].Value : "0")
                            };

                            string is1year = (sheet.Range["J" + index].Value != null) ? sheet.Range["J" + index].Value.ToString() : string.Empty;
                            if (is1year.Equals("Sim"))
                            {
                                rowToAdd.primeiroAnoIncuido = true;
                            }
                            else
                            {
                                rowToAdd.primeiroAnoIncuido = false;
                            }

                            string is1party = (sheet.Range["K" + index].Value != null) ? sheet.Range["K" + index].Value.ToString() : string.Empty;
                            if (is1party.Equals("Sim"))
                            {
                                rowToAdd.is3Party = true;
                            }
                            else
                            {
                                rowToAdd.is3Party = false;
                            }
                            //    rowToAdd.tipoRetiradaTopUp = true;
                            //else
                            //    rowToAdd.tipoRetiradaRetoma = true;

                            listSofware.Add(rowToAdd);
                        }

                    }

                    proposal.softwareList = listSofware;

                }

                if (sheet.Name.Equals(Settings.Default.SheetOutrosServicos))
                {
                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);
                    List<OutrosServicosExcel> listOutrosServicos = new List<OutrosServicosExcel>();
                    foreach (ListRow row in SheetObject.ListRows)
                    {
                        int index = row.Index + 2;
                        if (sheet.Range["A" + index].Value != null && sheet.Range["A" + index].Value.ToString() != string.Empty)
                        {
                            OutrosServicosExcel rowToAdd = new OutrosServicosExcel()
                            {
                                descricao = (sheet.Range["A" + index].Value != null) ? sheet.Range["A" + index].Value.ToString() : string.Empty,
                                precoLista = Convert.ToDouble((sheet.Range["B" + index].Value != null && sheet.Range["B" + index].Value.ToString() != string.Empty) ? sheet.Range["B" + index].Value : "0"),
                                desconto = Convert.ToDouble((sheet.Range["C" + index].Value != null && sheet.Range["C" + index].Value.ToString() != string.Empty) ? sheet.Range["C" + index].Value : "0"),
                                quantidade = Convert.ToInt32((sheet.Range["E" + index].Value != null && sheet.Range["E" + index].Value.ToString() != string.Empty) ? sheet.Range["E" + index].Value : "0")
                            };
                            listOutrosServicos.Add(rowToAdd);
                        }
                    }
                    proposal.outrosServicosList = listOutrosServicos;

                }

            }


            return proposal;
        }

        private void Pesquisar()
        {
            FreazeLayout();
            try
            {
                PesquisaForm pesquisarForm = new PesquisaForm();
                pesquisarForm.macAddress = P2CAddinUtilsController.GetMACAddress();
                pesquisarForm.encryptKey = SheetProtectionPassword;
                //Proteger Todas as Sheets
                this.UnProtectSheet(Settings.Default.SheetDadosGerais);
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                this.UnProtectSheet(Settings.Default.SheetRecursosHumanos);
                this.UnProtectSheet(Settings.Default.SheetSoftware);
                this.UnProtectSheet(Settings.Default.SheetOutrosServicos);

                List<RoleExcelP2C> valueVendList = new List<RoleExcelP2C>();
                valueVendList = P2CAddInGenericListsController.GetListaVendedores(Excel_App.Worksheets);
                pesquisarForm.DataSource_Vendedor = valueVendList;

                List<ExcelAddInVariables> valueEstadosList = new List<ExcelAddInVariables>();
                valueEstadosList = P2CAddInGenericListsController.GetEstadosProposta(Excel_App.Worksheets);
                pesquisarForm.DataSource_EstadosProposta = valueEstadosList;

                List<ExcelAddInVariables> valueNegociosList = new List<ExcelAddInVariables>();
                valueNegociosList = P2CAddInGenericListsController.GetTiposNegocio(Excel_App.Worksheets);
                pesquisarForm.DataSource_TipoNegocio = valueNegociosList;

                List<RoleExcelP2C> valuePerfilAreaList = new List<RoleExcelP2C>();
                valuePerfilAreaList =P2CAddInGenericListsController.GetPerfilAreas(Excel_App.Worksheets);
                pesquisarForm.listaPerfilAreas = valuePerfilAreaList;

                List<ExcelAddInVariables> valuePeriocidadesList = new List<ExcelAddInVariables>();
                valuePeriocidadesList = P2CAddInGenericListsController.GetPeriocidades(Excel_App.Worksheets);
                pesquisarForm.listaPeriocidades = valuePeriocidadesList;

                List<ExcelAddInVariables> valueCopiasList = new List<ExcelAddInVariables>();
                valueCopiasList = P2CAddInGenericListsController.GetCopias(Excel_App.Worksheets);
                pesquisarForm.listaCopias = valueCopiasList;

                List<ExcelAddInVariables> valueCarenciasList = new List<ExcelAddInVariables>();
                valueCarenciasList = P2CAddInGenericListsController.GetCarencias(Excel_App.Worksheets);
                pesquisarForm.listaCarencias = valueCarenciasList;

                List<RoleExcelP2C> valuePareceirosList = new List<RoleExcelP2C>();
                valuePareceirosList =P2CAddInGenericListsController.GetListaParceiros(Excel_App.Worksheets);
                pesquisarForm.listaParceiros = valuePareceirosList;

                pesquisarForm.ShowDialog();
                setResumoData();


                this.ProtectSheet(Settings.Default.SheetDadosGerais);
                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
                this.ProtectSheet(Settings.Default.SheetRecursosHumanos);
                this.ProtectSheet(Settings.Default.SheetSoftware);
                this.ProtectSheet(Settings.Default.SheetOutrosServicos);
            }
            catch (Exception ex)
            {
                //throw;
                HandleException(ex.Message);
            }
            finally
            {
                UnFreazeLayout();
            }

            //SetVisibilityTabs();
        }

        private void Connect()
        {
            FreazeLayout();
            try
            {

                LoginForm loginForm = new LoginForm();
                loginForm.macAddress = P2CAddinUtilsController.GetMACAddress();
                loginForm.encriptKey = SheetProtectionPassword;
                loginForm.ShowDialog();


                if (loginForm.IsLoggedIn)
                {
                    ApplyTemplate();

                }

            }
            catch (Exception e)
            {
                //throw;
                HandleException(e.Message);
            }
            finally 
            {
                UnFreazeLayout();
            }
            
        }

        public bool CheckIfDadosMestreDesactualizados()
        {
            bool retVal = true;
            DateTime dataActual = DateTime.Now;
            SessionKeyRegistry registry = new SessionKeyRegistry();

            var dataUltimaActualizacao=registry.Read("DadosMestreData");

            if (dataUltimaActualizacao != null)
            {
                DateTime datalastUpdate = Convert.ToDateTime(dataUltimaActualizacao);
                

                var hours = (dataActual - datalastUpdate).TotalHours;
                var timeUpdate = Settings.Default.UpdateMasterTime;

                if (hours > timeUpdate)
                {
                    return true;
                }
                retVal = false;
            }

            return retVal;
        }

        public void ApplyTemplate()
        {
            SessionKeyRegistry registry = new SessionKeyRegistry();
            bool updateMasterData= CheckIfDadosMestreDesactualizados();
            bool isTemplateLoaded = false;
            bool isUpdated = false;

            foreach (Worksheet sheet in Excel_App.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                    isTemplateLoaded = true;

            }

            if (!isTemplateLoaded)
            {
                string File = AppRoot + Settings.Default.Template_ApplicationMenu;
                Excel_Activebook = Excel_App.Workbooks.Add(File);

                if (updateMasterData)
                {
                    Excel_App.DisplayAlerts = false;
                    ActualizarDadosMestre(Excel_Activebook);
                    Excel_Activebook.SaveAs(File, XlFileFormat.xlOpenXMLTemplate);
                    Excel_Activebook.Close();
                    Excel_Activebook = Excel_App.Workbooks.Add(File);
                    Excel_App.DisplayAlerts = true;
                    isUpdated = true;
                }

                Excel_Activebook.SheetActivate += new WorkbookEvents_SheetActivateEventHandler(Excel_Activebook_SheetActivate);

                if (checkAddEvents())
                {
                    Excel_App.SheetChange += new AppEvents_SheetChangeEventHandler(Excel_Activebook_SheetChange);
                    Excel_App.SheetSelectionChange += new AppEvents_SheetSelectionChangeEventHandler(Excel_Activebook_SheetSelectionChange);
                }
            
            }
            else
            {
                if (updateMasterData)
                {
                    ActualizacaoDados("É necessário actualizar os dados mestre, pretende também actualizar os dados mestre deste documento?(esta operação demorará alguns minutos)");
                    
                    isUpdated = true;
                    
                }
            }

            if (isUpdated)
            {
                registry.Write("DadosMestreData", DateTime.Now.ToString());
                MensagensForm formMsg = new MensagensForm();
                formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.ok_48;
                formMsg.msgToShow = "Os dados Mestre foram actualizados";
                formMsg.ShowDialog();
            }
        }

        private bool validateSessionKey(string sessionKey)
        {
            bool isSessionKeyValid = true;

            return isSessionKeyValid;

        }

        private void ActualizarDadosMestre(Workbook workbookToUpdate)
        {
            FreazeLayout();
            try
            {

                string macAddress = P2CAddinUtilsController.GetMACAddress();

                SessionKeyRegistry register = new SessionKeyRegistry();
                string dtSessao = register.Read("ChaveSessao");
                string userSessao = register.Read("UserNameSessao");
                string sessKey = userSessao + "<------&#Xerox2013#&----->pass<------&#Xerox2013#&----->" + dtSessao;

                
                sessKey = register.Encrypt(sessKey, true, SheetProtectionPassword);
                ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();
                var masterData = service.GetMasterData(sessKey, macAddress);

                P2CAddInMasterDataController.UpdateMasterData(workbookToUpdate, masterData);


            }
            catch (Exception ex)
            {
                //throw;
                HandleException(ex.Message);
            }
            finally
            {
                UnFreazeLayout();
            }
        }

        private void ActualizarResumoData()
        {
            ProposalExcel proposta = this.GetProposalData();
            PropostaResumo resumoData = P2CAddInProposalResumoController.SetTotais(proposta);


            editBoxTotalEquipInst.Text = string.Format("{0:#,##0.00}", resumoData.TotalEquipInstalar); //resumoData.TotalEquipInstalar.ToString();
            editBoxTotServico.Text = string.Format("{0:#,##0.00}", resumoData.TotalServico); //resumoData.TotalServico.ToString();
            editBoxTotalRH.Text = string.Format("{0:#,##0.00}", resumoData.TotalRH);  //resumoData.TotalRH.ToString();
            editBoxTotalSoft.Text = string.Format("{0:#,##0.00}", resumoData.TotalSoftware);  //resumoData.TotalSoftware.ToString();
            editBoxOutrosServ.Text = string.Format("{0:#,##0.00}", resumoData.TotalOutrosServ);  //resumoData.TotalOutrosServ.ToString();
            editBoxTotalOrs.Text = string.Format("{0:#,##0.00}", resumoData.TotalORS); // resumoData.TotalORS.ToString();
            editBoxTotalCont.Text = string.Format("{0:#,##0.00}", resumoData.TotalContrato); //resumoData.TotalContrato.ToString();
            editBoxTotalRenda.Text = string.Format("{0:#,##0.00}", resumoData.TotalRenda);// resumoData.TotalRenda.ToString();
        }

        private bool RangeIsEqual(Range a, Range b)
        {
            bool retVal = false;

            if (a.Row == b.Row && a.Column == b.Column)
                return true;

            return retVal;
        }

        public void SetVisibilityTabs()
        {
            foreach (Worksheet sheet in Excel_App.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                {
                    string tipoNegocio = sheet.Range["J10"].Value;


                    if (tipoNegocio == null || tipoNegocio == string.Empty)
                    {

                    }
                    else
                    {
                        string[] tipoNegocioCod = tipoNegocio.Split('-');
                        int codigoTipoNegocio = Convert.ToInt32(tipoNegocioCod[0].Trim());
                        List<ExcelTipoNegocioMenu> listMenu = new List<ExcelTipoNegocioMenu>();
                        List<ExcelTipoNegocioMenu> listNegocios = new List<ExcelTipoNegocioMenu>();

                        listMenu =  P2CAddInGenericListsController.GetListaTiposNegocio(Excel_App.Worksheets);
                        listNegocios = listMenu.FindAll(x => x.idNegocio == codigoTipoNegocio);

                        bool haveRecursosHumanos = false;
                        bool haveSoftware = false;
                        bool haveOutrosServicos = false;

                        foreach (ExcelTipoNegocioMenu menuIt in listNegocios)
                        {
                            if (menuIt.descSheet.Equals(Settings.Default.SheetRecursosHumanos))
                                haveRecursosHumanos = true;

                            if (menuIt.descSheet.Equals(Settings.Default.SheetSoftware))
                                haveSoftware = true;

                            if (menuIt.descSheet.Equals(Settings.Default.SheetOutrosServicos))
                                haveOutrosServicos = true;

                        }

                        if (haveRecursosHumanos)
                            ShowSheet(Settings.Default.SheetRecursosHumanos);
                        else
                            HideSheet(Settings.Default.SheetRecursosHumanos);

                        if (haveSoftware)
                            ShowSheet(Settings.Default.SheetSoftware);
                        else
                            HideSheet(Settings.Default.SheetSoftware);

                        if (haveOutrosServicos)
                            ShowSheet(Settings.Default.SheetOutrosServicos);
                        else
                            HideSheet(Settings.Default.SheetOutrosServicos);
                    }
                }
            }

            //currentSheet.Activate();


        }

        private void setResumoData()
        {

            foreach (Worksheet sheet in Excel_App.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                {
                    var tipoContrato = sheet.Range["P10"].Value;
                    var periocidade = sheet.Range["J15"].Value;
                    var factor = sheet.Range["P18"].Value;
                    var prazo = sheet.Range["J17"].Value;

                    editBoxTipoContrato.Text = Convert.ToString(tipoContrato);
                    editBoxPeriocidade.Text = Convert.ToString(periocidade);
                    editBoxFactor.Text = Convert.ToString(factor);
                    editBoxPrazo.Text = Convert.ToString(prazo);
                }
            }
        }


        #endregion

        #region EXCEPTION HANDLE

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }
        

        #endregion


        #region LAYOUT VISIBILITY


        private void EliminarAllOutrosServ(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetOutrosServicos);
                long totaisLine = sheet.Cells.Find("Totais").Row;
                totaisDy = totaisLine;

                for (int i = 3; i <totaisLine; i++)
                {
                    
                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":G" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["E3"].Value = "";

                    }

                    totaisDy = sheet.Cells.Find("Totais").Row;
                }
                this.ProtectSheet(Settings.Default.SheetOutrosServicos);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }

        private void EliminarAllSofware(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetSoftware);
                long totaisLine = sheet.Cells.Find("Totais").Row;
                totaisDy = totaisLine;

                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":M" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                       
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["E3"].Value = "";
                        sheet.Range["G3"].Value = "";
                        sheet.Range["I3"].Value = "";
                        sheet.Range["J3"].Value = "";
                        sheet.Range["K3"].Value = "";

                    }

                    totaisDy = sheet.Cells.Find("Totais").Row;
                }
                this.ProtectSheet(Settings.Default.SheetSoftware);
            }
            catch (Exception ex) 
            {
                HandleException(ex.Message);
            }
        }

        public void EliminarAllRecursosHumanos(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetRecursosHumanos);
                long totaisLine = sheet.Cells.Find("Totais").Row;
                totaisDy = totaisLine;

                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":H" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {

                        //Range Line = sheet.Range["A3:E3"];
                        //Line.Value = "";
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["F3"].Value = "";
                    }

                    totaisDy = sheet.Cells.Find("Totais").Row;
                }
                this.ProtectSheet(Settings.Default.SheetRecursosHumanos);
            }
            catch (Exception ex) 
            {

                HandleException(ex.Message);
            
            }
        }

        private void EliminarAllEquipInstalarServico(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                long totaisLine = sheet.Cells.Find("Totais Instalar").Row;
                totaisDy = totaisLine;


                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":AL" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["C3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["E3"].Value = "";
                        sheet.Range["G3"].Value = "";
                        sheet.Range["H3"].Value = "";
                        sheet.Range["I3"].Value = "";
                        sheet.Range["K3"].Value = "";
                        sheet.Range["L3"].Value = "";
                        sheet.Range["M3"].Value = "";
                        sheet.Range["N3"].Value = "";
                        sheet.Range["O3"].Value = "";
                        sheet.Range["P3"].Value = "";
                        sheet.Range["Q3"].Value = "";

                        sheet.Range["S3"].Value = "";
                        sheet.Range["T3"].Value = "";
                        sheet.Range["U3"].Value = "";
                        sheet.Range["W3"].Value = "";


                        sheet.Range["Y3"].Value = "";
                        sheet.Range["Z3"].Value = "";
                        sheet.Range["AA3"].Value = "";
                        sheet.Range["AC3"].Value = "";

                        sheet.Range["AE3"].Value = "";
                        sheet.Range["AF3"].Value = "";
                        sheet.Range["AG3"].Value = "";
                        sheet.Range["AJ3"].Value = "";
                        sheet.Range["AI3"].Value = "";

                    }

                    totaisDy = sheet.Cells.Find("Totais Instalar").Row;

                }

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
            }

        }

        private void EliminarAllEquipRemover(Worksheet sheet)
        {
            try
            {

                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                long totaisLine = sheet.Cells.Find("Totais Remover").Row;
                totaisDy = totaisLine;


                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["AN" + index + ":AX" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                        //Line.Delete();
                    }
                    else
                    {
                        //Range Line = (Range)sheet.Rows[selection.Row];
                        Range Line = sheet.Range["AN3:AX3"];
                        Line.Value = "";
                    }

                    totaisDy = sheet.Cells.Find("Totais Remover").Row;
                }

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SheetName"></param>
        private void ShowSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in Excel_App.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();


            this.UnProtectWorkBook();
            if (Sheet != null)
            {
                ShowSheet(Sheet);
            }
            this.ProtectWorkBook();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SheetName"></param>
        private void HideSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in Excel_App.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();
            if (Sheet != null)
            {

                this.UnProtectWorkBook();

                switch (SheetName)
                {
                    case "EquipInstalarRem&Servicos":
                        EliminarAllEquipInstalarServico(Sheet);
                        EliminarAllEquipRemover(Sheet);
                        break;
                    case "RecursosHumanos":
                        EliminarAllRecursosHumanos(Sheet);
                        HideSheet(Sheet);
                        break;
                    case "Software":
                        EliminarAllSofware(Sheet);
                        HideSheet(Sheet);
                        break;
                    case "OutrosServicos":
                        EliminarAllOutrosServ(Sheet);
                        HideSheet(Sheet);
                        break;
                }

                this.ProtectWorkBook();

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sheet"></param>
        private void ShowSheet(Worksheet Sheet)
        {
            Sheet.Visible= XlSheetVisibility.xlSheetVisible;
            //Sheet.Activate();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sheet"></param>
        private void HideSheet(Worksheet Sheet)
        {
            Sheet.Visible = XlSheetVisibility.xlSheetVeryHidden;
        }
        #endregion


        #region LAYOUT PROTECTION
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SheetName"></param>
        private void ProtectSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in Excel_App.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();
            if (Sheet != null)
            {
                this.ProtectSheet(Sheet);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SheetName"></param>
        private void UnProtectSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in Excel_App.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();
            if (Sheet != null)
            {
                this.UnProtectSheet(Sheet);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sheet"></param>
        /// <param name="AllowNewLines"></param>
        /// <param name="AllowPivot"></param>
        private void ProtectSheet(Worksheet Sheet, bool AllowNewLines = false, bool AllowPivot = false)
        {
            if (!Sheet.ProtectContents ||
               !Sheet.ProtectDrawingObjects ||
               !Sheet.ProtectScenarios)
                Sheet.Protect(SheetProtectionPassword, true, true,true, false, false, false, false, false, AllowNewLines, false, false, AllowNewLines, true, true, AllowPivot);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sheet"></param>
        private void UnProtectSheet(Worksheet Sheet)
        {
            if (Sheet.ProtectContents ||
                Sheet.ProtectDrawingObjects ||
                Sheet.ProtectScenarios)
                Sheet.Unprotect(SheetProtectionPassword);
        }

        private void ProtectWorkBook()
        {
            ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Protect(SheetProtectionPassword);
        }

        private void UnProtectWorkBook()
        {


            ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Unprotect(SheetProtectionPassword);
        }

        #endregion

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            SessionKeyRegistry register = new SessionKeyRegistry();
            register.Write("ChaveSessao", DateTime.MinValue.ToString());
            register.Write("UserNameSessao", string.Empty);

        }


       

        

        
       

        
        

       
 
        

        

        

       
       




       
    }
}