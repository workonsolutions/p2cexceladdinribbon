﻿namespace ExcelAddIn
{
    partial class MensagensForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxMsg = new System.Windows.Forms.PictureBox();
            this.labelMsg = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMsg)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxMsg
            // 
            this.pictureBoxMsg.Image = global::P2CExcelAddInRibbon.Properties.Resources.ok_48;
            this.pictureBoxMsg.Location = new System.Drawing.Point(7, 5);
            this.pictureBoxMsg.Name = "pictureBoxMsg";
            this.pictureBoxMsg.Size = new System.Drawing.Size(59, 57);
            this.pictureBoxMsg.TabIndex = 1;
            this.pictureBoxMsg.TabStop = false;
            // 
            // labelMsg
            // 
            this.labelMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMsg.Location = new System.Drawing.Point(77, 18);
            this.labelMsg.Name = "labelMsg";
            this.labelMsg.Size = new System.Drawing.Size(361, 177);
            this.labelMsg.TabIndex = 2;
            this.labelMsg.Text = "Proposta criada com sucesso";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(373, 198);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(65, 23);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // MensagensForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 233);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelMsg);
            this.Controls.Add(this.pictureBoxMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MensagensForm";
            this.Load += new System.EventHandler(this.MensagensForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMsg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxMsg;
        private System.Windows.Forms.Label labelMsg;
        private System.Windows.Forms.Button buttonOk;
    }
}