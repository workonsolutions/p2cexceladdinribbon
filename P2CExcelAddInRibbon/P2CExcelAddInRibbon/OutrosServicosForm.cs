﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;

namespace ExcelAddIn
{
    public partial class OutrosServicosForm : Form
    {

        public OutrosServicosForm()
        {
            InitializeComponent();
        }

        private void buttonInserir_Click(object sender, EventArgs e)
        {

            if (ValidateControls())
                InsertDataInTable();

        }

        private bool ValidateControls()
        {
            bool isValidated = true;

            int quantidade = 0;
            bool validQtd = int.TryParse(textBoxQuantidade.Text, out quantidade);

            if (!validQtd || quantidade<0)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxQuantidade, "Quantidade Inválida");
            }

            double preco = 0.0D;
            bool validPreco = double.TryParse(textBoxPrecoLista.Text, out preco);

            if ((!validPreco || preco<0) && textBoxPrecoLista.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxPrecoLista, "Preço Lista Inválido");
            }

            double desconto = 0.0D;
            bool validDesc = double.TryParse(textBoxDesconto.Text, out desconto);

            if ((!validDesc || desconto<0) && textBoxDesconto.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDesconto, "Desconto Inválido");
            }

            return isValidated;
        }

        private void InsertDataInTable()
        {
            try
            {
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                long insertLine = sheet.Cells.Find("Totais").Row;
                string fristLine = sheet.Range["A3"].Value;
                long row = 0;

                if (fristLine != null && fristLine != string.Empty)
                {
                    Range Line = sheet.Range["A" + insertLine + ":G" + insertLine + ""];
                    Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    row = insertLine;
                }
                else
                    row = 3;

                double precoUnit = Convert.ToDouble((textBoxPrecoLista.Text != string.Empty) ? textBoxPrecoLista.Text : "0");
                if (textBoxDesconto.Text != string.Empty && Convert.ToDouble(textBoxDesconto.Text) > 0)
                {
                    precoUnit = precoUnit - (precoUnit * (0.01 * Convert.ToDouble(textBoxDesconto.Text)));
                }


                sheet.Range["J" + row].Value = "=DadosClienteGerais!J17";
                sheet.Range["A" + row].Value = textBoxDesignacao.Text;
                sheet.Range["B" + row].Value = Convert.ToDouble((textBoxPrecoLista.Text == string.Empty) ? "0" : textBoxPrecoLista.Text);
                //sheet.Range["C" + row].Value = Convert.ToDouble((textBoxDesconto.Text == string.Empty) ? "0" : textBoxDesconto.Text);
                sheet.Range["D" + row].Value = precoUnit;
                sheet.Range["E" + row].Value = Convert.ToDouble((textBoxQuantidade.Text == string.Empty) ? "0" : textBoxQuantidade.Text); 

                //s.Manutencao / 12 * (periodoContrato - (s.ManutPrimAnoIncluido.GetValueOrDefault(false) ? 12 : 0)) * s.Quantidade * (1 - s.DescontoManutencao.GetValueOrDefault(0) / 100);
                long insertLineFix = sheet.Cells.Find("Totais").Row;
                Range LineFix = sheet.Range["A" + insertLineFix + ":Z" + insertLineFix + ""];
                LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            this.Close();
        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OutrosServicosForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonInserir_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                button1_Click(sender, e);
            }
        }

        
    }
}
