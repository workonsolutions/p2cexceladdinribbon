﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExcelAddIn
{
    public partial class MensagensForm : Form
    {

        public Bitmap imageDisplay { get; set; }
        public string msgToShow { get; set; }

        public MensagensForm()
        {
            InitializeComponent();
        }

        private void MensagensForm_Load(object sender, EventArgs e)
        {
            pictureBoxMsg.Image = imageDisplay;
            labelMsg.Text = msgToShow; 
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
