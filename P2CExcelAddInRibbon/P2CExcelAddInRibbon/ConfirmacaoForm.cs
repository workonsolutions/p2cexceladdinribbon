﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExcelAddIn
{
    public partial class ConfirmacaoForm : Form
    {

        public Bitmap imageDisplay { get; set; }
        public string msgToShow { get; set; }
        public bool confirmation { get; set; }
        public bool isPesquisa { get; set; }
        public bool isDuplicated { get; set; }

        public ConfirmacaoForm()
        {
            InitializeComponent();
        }

        private void MensagensForm_Load(object sender, EventArgs e)
        {
            isDuplicated = false;
            labelMsg.Text = msgToShow;
            if (isPesquisa)
                buttonDuplicar.Visible = true;
            else
                buttonDuplicar.Visible = false;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            confirmation = false;
            this.Close();
        }

        private void buttonSim_Click(object sender, EventArgs e)
        {
            confirmation = true;
            this.Close();
        }

        private void buttonDuplicar_Click(object sender, EventArgs e)
        {
            isDuplicated = true;
            confirmation = true;
            this.Close();
        }

        

    }
}
