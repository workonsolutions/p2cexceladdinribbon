﻿namespace ExcelAddIn
{
    partial class ServicoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bindingSourceEquipamentos = new System.Windows.Forms.BindingSource(this.components);
            this.buttonOk = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxTfmRef = new System.Windows.Forms.TextBox();
            this.labelPreco = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxDescontoCor = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxDesconto3C = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxDescontoPB = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxPrecoExdCor = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPrecoExd3C = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxPrecoExdPB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxVolCliCor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxVolCli3C = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxVolCliPB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxVolRefCor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxVolRef3C = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxVolRefPB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxTfmDesconto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxServicos = new System.Windows.Forms.ComboBox();
            this.comboBoxEquipamentos = new System.Windows.Forms.ComboBox();
            this.bindingSourceServicos = new System.Windows.Forms.BindingSource(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquipamentos)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceServicos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(498, 306);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(167, 23);
            this.buttonOk.TabIndex = 17;
            this.buttonOk.Text = "Inserir Equipamento a Instalar";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(402, 306);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxTfmRef
            // 
            this.textBoxTfmRef.Enabled = false;
            this.textBoxTfmRef.Location = new System.Drawing.Point(106, 106);
            this.textBoxTfmRef.Name = "textBoxTfmRef";
            this.textBoxTfmRef.Size = new System.Drawing.Size(71, 20);
            this.textBoxTfmRef.TabIndex = 3;
            // 
            // labelPreco
            // 
            this.labelPreco.AutoSize = true;
            this.labelPreco.Location = new System.Drawing.Point(13, 108);
            this.labelPreco.Name = "labelPreco";
            this.labelPreco.Size = new System.Drawing.Size(87, 13);
            this.labelPreco.TabIndex = 7;
            this.labelPreco.Text = "TFM Referência:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBoxDescontoCor);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBoxDesconto3C);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.textBoxDescontoPB);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBoxPrecoExdCor);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBoxPrecoExd3C);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBoxPrecoExdPB);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxVolCliCor);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxVolCli3C);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBoxVolCliPB);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxVolRefCor);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxVolRef3C);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxVolRefPB);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxTfmDesconto);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxServicos);
            this.groupBox1.Controls.Add(this.comboBoxEquipamentos);
            this.groupBox1.Controls.Add(this.labelPreco);
            this.groupBox1.Controls.Add(this.textBoxTfmRef);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(654, 288);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serviço";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(387, 110);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 13);
            this.label19.TabIndex = 49;
            this.label19.Text = "%";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(205, 252);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 13);
            this.label18.TabIndex = 48;
            this.label18.Text = "%";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(619, 253);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(415, 252);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 13);
            this.label16.TabIndex = 46;
            this.label16.Text = "%";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(442, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Desconto Cor:";
            // 
            // textBoxDescontoCor
            // 
            this.textBoxDescontoCor.Enabled = false;
            this.textBoxDescontoCor.Location = new System.Drawing.Point(535, 250);
            this.textBoxDescontoCor.Name = "textBoxDescontoCor";
            this.textBoxDescontoCor.Size = new System.Drawing.Size(71, 20);
            this.textBoxDescontoCor.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(236, 251);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "Desconto 3ºC:";
            // 
            // textBoxDesconto3C
            // 
            this.textBoxDesconto3C.Enabled = false;
            this.textBoxDesconto3C.Location = new System.Drawing.Point(329, 249);
            this.textBoxDesconto3C.Name = "textBoxDesconto3C";
            this.textBoxDesconto3C.Size = new System.Drawing.Size(71, 20);
            this.textBoxDesconto3C.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 251);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Desconto P/B:";
            // 
            // textBoxDescontoPB
            // 
            this.textBoxDescontoPB.Enabled = false;
            this.textBoxDescontoPB.Location = new System.Drawing.Point(115, 249);
            this.textBoxDescontoPB.Name = "textBoxDescontoPB";
            this.textBoxDescontoPB.Size = new System.Drawing.Size(71, 20);
            this.textBoxDescontoPB.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(436, 222);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "Preço Exd. Cor:";
            // 
            // textBoxPrecoExdCor
            // 
            this.textBoxPrecoExdCor.Enabled = false;
            this.textBoxPrecoExdCor.Location = new System.Drawing.Point(535, 220);
            this.textBoxPrecoExdCor.Name = "textBoxPrecoExdCor";
            this.textBoxPrecoExdCor.Size = new System.Drawing.Size(71, 20);
            this.textBoxPrecoExdCor.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(230, 221);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Preço Exd. 3ºC:";
            // 
            // textBoxPrecoExd3C
            // 
            this.textBoxPrecoExd3C.Enabled = false;
            this.textBoxPrecoExd3C.Location = new System.Drawing.Point(329, 219);
            this.textBoxPrecoExd3C.Name = "textBoxPrecoExd3C";
            this.textBoxPrecoExd3C.Size = new System.Drawing.Size(71, 20);
            this.textBoxPrecoExd3C.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 221);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Preço Exd. P/B:";
            // 
            // textBoxPrecoExdPB
            // 
            this.textBoxPrecoExdPB.Enabled = false;
            this.textBoxPrecoExdPB.Location = new System.Drawing.Point(115, 219);
            this.textBoxPrecoExdPB.Name = "textBoxPrecoExdPB";
            this.textBoxPrecoExdPB.Size = new System.Drawing.Size(71, 20);
            this.textBoxPrecoExdPB.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(442, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Volume Cli. Cor:";
            // 
            // textBoxVolCliCor
            // 
            this.textBoxVolCliCor.Enabled = false;
            this.textBoxVolCliCor.Location = new System.Drawing.Point(535, 180);
            this.textBoxVolCliCor.Name = "textBoxVolCliCor";
            this.textBoxVolCliCor.Size = new System.Drawing.Size(71, 20);
            this.textBoxVolCliCor.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(236, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Volume Cli. 3ºC:";
            // 
            // textBoxVolCli3C
            // 
            this.textBoxVolCli3C.Enabled = false;
            this.textBoxVolCli3C.Location = new System.Drawing.Point(329, 179);
            this.textBoxVolCli3C.Name = "textBoxVolCli3C";
            this.textBoxVolCli3C.Size = new System.Drawing.Size(71, 20);
            this.textBoxVolCli3C.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 181);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Volume Cli. P/B:";
            // 
            // textBoxVolCliPB
            // 
            this.textBoxVolCliPB.Enabled = false;
            this.textBoxVolCliPB.Location = new System.Drawing.Point(115, 179);
            this.textBoxVolCliPB.Name = "textBoxVolCliPB";
            this.textBoxVolCliPB.Size = new System.Drawing.Size(71, 20);
            this.textBoxVolCliPB.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(436, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Volume Ref. Cor:";
            // 
            // textBoxVolRefCor
            // 
            this.textBoxVolRefCor.Enabled = false;
            this.textBoxVolRefCor.Location = new System.Drawing.Point(535, 150);
            this.textBoxVolRefCor.Name = "textBoxVolRefCor";
            this.textBoxVolRefCor.Size = new System.Drawing.Size(71, 20);
            this.textBoxVolRefCor.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Volume Ref. 3ºC:";
            // 
            // textBoxVolRef3C
            // 
            this.textBoxVolRef3C.Enabled = false;
            this.textBoxVolRef3C.Location = new System.Drawing.Point(329, 149);
            this.textBoxVolRef3C.Name = "textBoxVolRef3C";
            this.textBoxVolRef3C.Size = new System.Drawing.Size(71, 20);
            this.textBoxVolRef3C.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Volume Ref. P/B:";
            // 
            // textBoxVolRefPB
            // 
            this.textBoxVolRefPB.Enabled = false;
            this.textBoxVolRefPB.Location = new System.Drawing.Point(115, 149);
            this.textBoxVolRefPB.Name = "textBoxVolRefPB";
            this.textBoxVolRefPB.Size = new System.Drawing.Size(71, 20);
            this.textBoxVolRefPB.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Desconto TFM";
            // 
            // textBoxTfmDesconto
            // 
            this.textBoxTfmDesconto.Enabled = false;
            this.textBoxTfmDesconto.Location = new System.Drawing.Point(295, 107);
            this.textBoxTfmDesconto.Name = "textBoxTfmDesconto";
            this.textBoxTfmDesconto.Size = new System.Drawing.Size(71, 20);
            this.textBoxTfmDesconto.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Serviço:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Equipamento:";
            // 
            // comboBoxServicos
            // 
            this.comboBoxServicos.FormattingEnabled = true;
            this.comboBoxServicos.Location = new System.Drawing.Point(82, 65);
            this.comboBoxServicos.Name = "comboBoxServicos";
            this.comboBoxServicos.Size = new System.Drawing.Size(258, 21);
            this.comboBoxServicos.TabIndex = 2;
            this.comboBoxServicos.SelectedIndexChanged += new System.EventHandler(this.comboBoxServicos_SelectedIndexChanged);
            // 
            // comboBoxEquipamentos
            // 
            this.comboBoxEquipamentos.FormattingEnabled = true;
            this.comboBoxEquipamentos.Location = new System.Drawing.Point(82, 28);
            this.comboBoxEquipamentos.Name = "comboBoxEquipamentos";
            this.comboBoxEquipamentos.Size = new System.Drawing.Size(258, 21);
            this.comboBoxEquipamentos.TabIndex = 1;
            this.comboBoxEquipamentos.SelectedIndexChanged += new System.EventHandler(this.comboBoxEquipamentos_SelectedIndexChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ServicoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 341);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServicoForm";
            this.Text = "Adicionar Serviço";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ServicoForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquipamentos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceServicos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingSourceEquipamentos;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxTfmRef;
        private System.Windows.Forms.Label labelPreco;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxServicos;
        private System.Windows.Forms.ComboBox comboBoxEquipamentos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTfmDesconto;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxDescontoCor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxDesconto3C;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxDescontoPB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxPrecoExdCor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxPrecoExd3C;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxPrecoExdPB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxVolCliCor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxVolCli3C;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxVolCliPB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxVolRefCor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxVolRef3C;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxVolRefPB;
        private System.Windows.Forms.BindingSource bindingSourceServicos;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label19;
    }
}