﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;
using P2CExcelAddInRibbon.Properties;
using System.Globalization;
using System.Runtime.Serialization;

namespace ExcelAddIn
{
    public partial class ServicoForm : Form
    {

        public IEnumerable<object> DataSource_Equipamentos
        {
            get
            {
                return (IEnumerable<object>)bindingSourceEquipamentos.DataSource;
            }
            set
            {
                bindingSourceEquipamentos.DataSource = value;
                comboBoxEquipamentos.DataSource = bindingSourceEquipamentos;
                comboBoxEquipamentos.DisplayMember = "product";
            }
        }

        public IEnumerable<object> DataSource_Servicos
        {
            get
            {
                return (IEnumerable<object>)bindingSourceServicos.DataSource;
            }
            set
            {
                bindingSourceServicos.DataSource = value;
                comboBoxServicos.DataSource = bindingSourceServicos;
                comboBoxServicos.DisplayMember = "Plano";
            }
        }

        public List<ExcelEquipamento> listEquipamentos { get; set; }

        public List<ExcelServico> listServicos { get; set; }

        public ServicoForm()
        {
            InitializeComponent();
        }


        public void SetLists()
        {
            //DataSource_Equipamentos = listEquipamentos;
            try
            {
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;

                Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);

                List<ExcelEquipamento> equipInSheet = new List<ExcelEquipamento>();
                foreach (ListRow row in SheetObject.ListRows)
                {

                    int index = row.Index + 2;
                    string codigo = Convert.ToString(sheet.Range["K" + index].Value);
                    string codigoDesc = Convert.ToString(row.Range[2, Type.Missing].Value);

                    int idEquip = 0;
                    bool isCodigo = int.TryParse(codigo,out idEquip);

                    if (isCodigo)
                        equipInSheet.Add(new ExcelEquipamento { idPart = idEquip, product = codigoDesc , description = index.ToString() });
                }

                var sublistEquip = (from a in equipInSheet
                                    from b in listEquipamentos
                                    where a.idPart == b.idPart
                                    select b);

                int idFrist = (sublistEquip!=null && sublistEquip.Count()>0)?sublistEquip.FirstOrDefault().idPart:0;

                var sublistServ = (from b in listServicos
                                   where b.PartId == idFrist
                                   select b);


                if (sublistEquip.Count() > 0)
                    DataSource_Equipamentos = equipInSheet; //sublistEquip;
                if (sublistServ.Count() > 0)
                {
                    DataSource_Servicos = sublistServ;
                    loadEnableTextBoxs(sublistServ.FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
        }

        private void comboBoxEquipamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExcelEquipamento equipSelec = (ExcelEquipamento)comboBoxEquipamentos.SelectedItem;

            if (equipSelec != null)
            {
                var sublistServ = (from b in listServicos
                                   where b.PartId == equipSelec.idPart
                                   select b).Distinct();

                if (sublistServ.Count() > 0)
                    DataSource_Servicos = sublistServ;
                else
                {
                    sublistServ = new List<ExcelServico>();
                    DataSource_Servicos = sublistServ;
                }

                
            
            }

        }

        private void loadEnableTextBoxs(ExcelServico item)
        {
            textBoxPrecoExdCor.Text = item.PrecoPrintCor.ToString();
            textBoxPrecoExdPB.Text = item.PrecoPrintPB.ToString();
            textBoxPrecoExd3C.Text = item.PrecoPrint3.ToString();
            textBoxTfmRef.Text = item.Tfm.ToString();
            textBoxVolRefCor.Text = item.VolIncCor.ToString();
            textBoxVolRefPB.Text = item.VolIncPB.ToString();
            textBoxVolRef3C.Text = item.VolInc3.ToString();
            
            textBoxTfmDesconto.Enabled = true;

            if (item.PrecoPrintCor > 0)
            {
                textBoxVolCliCor.Enabled = true;
                textBoxDescontoCor.Enabled = true;
            }

            if (item.PrecoPrintPB > 0)
            {
                textBoxVolCliPB.Enabled = true;
                textBoxDescontoPB.Enabled = true;
            }

            if (item.PrecoPrint3 > 0)
            {
                textBoxVolCli3C.Enabled = true;
                textBoxDesconto3C.Enabled = true;
            }
        }

        private void comboBoxServicos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExcelServico selectServ = (ExcelServico)comboBoxServicos.SelectedItem;

            if (selectServ!=null)
                loadEnableTextBoxs(selectServ);

        }


        private void InsertDataInTable()
        {
            SetServicoByEquipamentoCodigo();

            this.Close();
        }

        private bool ValidateControls()
        {
            bool isValidated = true;

            long volumePb = 0;
            bool validVolPb = long.TryParse(textBoxVolCliPB.Text, out volumePb);

            if ((!validVolPb || volumePb<0) && textBoxVolCliPB.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxVolCliPB, "Volume Inválido");
            }

            long volumeCor = 0;
            bool validVolCor = long.TryParse(textBoxVolCliCor.Text, out volumeCor);

            if ((!validVolCor || volumeCor<0) && textBoxVolCliCor.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxVolCliCor, "Volume Inválido");
            }

            long volume3cont = 0;
            bool validVol3c = long.TryParse(textBoxVolCli3C.Text, out volume3cont);

            if ((!validVol3c || volume3cont<0) && textBoxVolCli3C.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxVolCli3C, "Volume Inválido");
            }

            double descPb = 0.0D;
            bool validDescPb = double.TryParse(textBoxDescontoPB.Text, out descPb);

            if ((!validDescPb || descPb<0) && textBoxDescontoPB.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDescontoPB, "Desconto Inválido");
            }

            double descCor = 0.0D;
            bool validDescCor = double.TryParse(textBoxDescontoCor.Text, out descCor);

            if ((!validDescCor || descCor<0) && textBoxDescontoCor.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDescontoCor, "Desconto Inválido");
            }

            double desc3c = 0.0D;
            bool validDesc3C = double.TryParse(textBoxDesconto3C.Text, out desc3c);

            if ((!validDesc3C || desc3c<0) && textBoxDesconto3C.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDesconto3C, "Desconto Inválido");
            }

            double tfmDesc = 0.0D;
            bool validtfmDesc = double.TryParse(textBoxTfmDesconto.Text, out tfmDesc);

            if ((!validtfmDesc || tfmDesc<0) && textBoxTfmDesconto.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxTfmDesconto, "Desconto Inválido");
            }


            if (comboBoxEquipamentos.Text.Equals(string.Empty))
            {
                isValidated = false;
                errorProvider1.SetError(comboBoxEquipamentos, "Equipamento Inválido");
            }

            if (comboBoxServicos.Text.Equals(string.Empty))
            {
                isValidated = false;
                errorProvider1.SetError(comboBoxServicos, "Serviço Inválido");
            }



            return isValidated;

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {

            if (ValidateControls())
                InsertDataInTable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }


        public void SetServicoByEquipamentoCodigo()
        {
            try
            {
                ExcelEquipamento equipSelec = (ExcelEquipamento)comboBoxEquipamentos.SelectedItem;

                foreach (Worksheet sheet in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
                {
                    if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                    {

                        Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);

                        double precoUnitCor = Convert.ToDouble((textBoxPrecoExdCor.Text!=string.Empty)?textBoxPrecoExdCor.Text:"0");
                        if (textBoxDescontoCor.Text != string.Empty && Convert.ToDouble(textBoxDescontoCor.Text) > 0)
                        {
                            precoUnitCor = precoUnitCor - (precoUnitCor * (0.01 * Convert.ToDouble(textBoxDescontoCor.Text)));
                        }

                        double precoUnitPB = Convert.ToDouble((textBoxPrecoExdPB.Text != string.Empty) ? textBoxPrecoExdPB.Text : "0");
                        if (textBoxDescontoPB.Text != string.Empty && Convert.ToDouble(textBoxDescontoPB.Text) > 0)
                        {
                            precoUnitPB = precoUnitPB - (precoUnitPB * (0.01 * Convert.ToDouble(textBoxDescontoPB.Text)));
                        }

                        double precoUnit3C = Convert.ToDouble((textBoxPrecoExd3C.Text != string.Empty) ? textBoxPrecoExd3C.Text : "0");
                        if (textBoxDesconto3C.Text != string.Empty && Convert.ToDouble(textBoxDesconto3C.Text) > 0)
                        {
                            precoUnit3C = precoUnit3C - (precoUnit3C * (0.01 * Convert.ToDouble(textBoxDesconto3C.Text)));
                        }


                        foreach (ListRow row in SheetObject.ListRows)
                        {
                            int index = row.Index + 2;

                            if (sheet.Range["A" + index].Value != null && sheet.Range["A" + index].Value.ToString() != string.Empty)
                            {

                                string codigo = (sheet.Range["B" + index].Value != null) ? sheet.Range["B" + index].Value.ToString() : string.Empty;
                                string modelo = (sheet.Range["C" + index].Value != null) ? sheet.Range["C" + index].Value.ToString() : string.Empty;

                                string codigoRowAux = equipSelec.description;

                                int codigoRow = 0;
                                int.TryParse(codigoRowAux,out codigoRow);

                                //if (equipSelec.product == codigo || equipSelec.product == modelo)
                                if (codigoRow == index)
                                {
                                    ExcelServico servicoSelected = (ExcelServico)comboBoxServicos.SelectedItem;
                                    string codigoServ = comboBoxServicos.SelectedText;

                                    sheet.Range["O" + index].Value = codigoServ;
                                    sheet.Range["P" + index].Value = Convert.ToDouble((textBoxTfmRef.Text == string.Empty) ? "0" : textBoxTfmRef.Text);
                                    sheet.Range["Q" + index].Value = Convert.ToDouble((textBoxTfmDesconto.Text == string.Empty) ? "0" : textBoxTfmDesconto.Text);

                                    sheet.Range["S" + index].Value = Convert.ToDouble((textBoxVolRefCor.Text == string.Empty) ? "0" : textBoxVolRefCor.Text);
                                    sheet.Range["T" + index].Value = Convert.ToDouble((textBoxVolCliCor.Text == string.Empty) ? "0" : textBoxVolCliCor.Text);
                                    sheet.Range["U" + index].Value = Convert.ToDouble((textBoxPrecoExdCor.Text == string.Empty) ? "0" : textBoxPrecoExdCor.Text);
                                    //sheet.Range["V" + index].Value = Convert.ToDouble((textBoxDescontoCor.Text == string.Empty) ? "0" : textBoxDescontoCor.Text);
                                    sheet.Range["W" + index].Value = precoUnitCor;

                                    sheet.Range["Y" + index].Value = Convert.ToDouble((textBoxVolRefPB.Text == string.Empty) ? "0" : textBoxVolRefPB.Text);
                                    sheet.Range["Z" + index].Value = Convert.ToDouble((textBoxVolCliPB.Text == string.Empty) ? "0" : textBoxVolCliPB.Text);
                                    sheet.Range["AA" + index].Value = Convert.ToDouble((textBoxPrecoExdPB.Text == string.Empty) ? "0" : textBoxPrecoExdPB.Text);
                                    //sheet.Range["AB" + index].Value = Convert.ToDouble((textBoxDescontoPB.Text == string.Empty) ? "0" : textBoxDescontoPB.Text);
                                    sheet.Range["AC" + index].Value = precoUnitPB;

                                    sheet.Range["AE" + index].Value = Convert.ToDouble((textBoxVolRef3C.Text == string.Empty) ? "0" : textBoxVolRef3C.Text);
                                    sheet.Range["AF" + index].Value = Convert.ToDouble((textBoxVolCli3C.Text == string.Empty) ? "0" : textBoxVolCli3C.Text);
                                    sheet.Range["AG" + index].Value = Convert.ToDouble((textBoxPrecoExd3C.Text == string.Empty) ? "0" : textBoxPrecoExd3C.Text);
                                    //sheet.Range["AH" + index].Value = Convert.ToDouble((textBoxDesconto3C.Text == string.Empty) ? "0" : textBoxDesconto3C.Text); 
                                    sheet.Range["AJ" + index].Value = precoUnit3C; 
                                    sheet.Range["AI" + index].Value = servicoSelected.IdServico.ToString();
                                }

                            }

                        }

                        var rangeSel = sheet.get_Range("O3", "O3");
                        rangeSel.Select();
                    }


                }

               


            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }

        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void ServicoForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonOk_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                button1_Click(sender, e);
            }
        }
   
 
    }
}
