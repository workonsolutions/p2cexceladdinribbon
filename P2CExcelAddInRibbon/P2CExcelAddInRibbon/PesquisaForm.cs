﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SpiritucSI.Xerox.Workflow.Business;
using Microsoft.Office.Interop.Excel;
using P2CExcelAddIn.Business;
using P2CExcelAddInRibbon.Properties;
using P2CExcelAddIn.Business.ServicesController;
using P2CExcelAddIn.Business.Security;
using P2CExcelAddIn.Business.Entities;

namespace ExcelAddIn
{
    public partial class PesquisaForm : Form
    {
        public PesquisaForm()
        {
            InitializeComponent();
        }

        public string macAddress {get;set;}
        public string encryptKey { get; set; }
        private string SheetProtectionPassword = "!xerox2013";

        public IEnumerable<object> DataSource_EstadosProposta
        {
            get
            {
                return (IEnumerable<object>)bindingSourceEstadoProp.DataSource;
            }
            set
            {
                bindingSourceEstadoProp.DataSource = value;
                comboBoxEstadoProposta.DataSource = bindingSourceEstadoProp;
                comboBoxEstadoProposta.DisplayMember = "descricao";
            }
        }

        public IEnumerable<object> DataSource_TipoNegocio
        {
            get
            {
                return (IEnumerable<object>)bindingSourceTipoNegocio.DataSource;
            }
            set
            {
                bindingSourceTipoNegocio.DataSource = value;
                comboBoxTipoNegocio.DataSource = bindingSourceTipoNegocio;
                comboBoxTipoNegocio.DisplayMember = "descricao";
            }
        }

        public IEnumerable<object> DataSource_Vendedor
        {
            get
            {
                return (IEnumerable<object>)bindingSourceVendedor.DataSource;
            }
            set
            {
                bindingSourceVendedor.DataSource = value;
                comboBoxVendedor.DataSource = bindingSourceVendedor;
                comboBoxVendedor.DisplayMember = "Desc";
            }
        }

        public List<RoleExcelP2C> listaPerfilAreas { get; set; }
        public List<RoleExcelP2C> listaParceiros { get; set; } 
        public List<ExcelAddInVariables> listaPeriocidades { get; set; }
        public List<ExcelAddInVariables> listaCopias { get; set; }
        public List<ExcelAddInVariables> listaCarencias { get; set; }


        public List<ProposalExcel> listaPropostas { get; set; }

        private void bindingSourceEstadoProp_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxEstadoProposta_SelectedIndexChanged(object sender, EventArgs e)
        {
            var test = comboBoxEstadoProposta.SelectedValue;
        }

        private void buttonPesquisar_Click(object sender, EventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            try
            {
              
                SessionKeyRegistry register = new SessionKeyRegistry();
                string dtSessao = register.Read("ChaveSessao");
                string userSessao = register.Read("UserNameSessao");
                string sessKey = userSessao + "<------&#Xerox2013#&----->pass<------&#Xerox2013#&----->" + dtSessao;

                sessKey = register.Encrypt(sessKey, true, encryptKey);

                bool? isCreditex = null;
                DateTime? dataInicio = null;
                DateTime? dataFim = null;


                var estadoProposta = comboBoxEstadoProposta.Text;
                var nomeCliente = textBoxNomeCliente.Text;
                var tipoNegocio = (ExcelAddInVariables)comboBoxTipoNegocio.SelectedItem;
                var idProposta = 0;//(textBoxIdProposta.Text != null && textBoxIdProposta.Text != string.Empty) ? Convert.ToInt32(textBoxIdProposta.Text) : 0;
                var nifCliente = textBoxNifCliente.Text;
                var vendedorUser = comboBoxVendedor.Text;

                if (radioButtonCreditexNao.Checked)
                    isCreditex = false;

                if (radioButtonCreditexSim.Checked)
                    isCreditex = true;

                if (dateTimePickerDtInicio.Text != string.Empty)
                {
                    dataInicio = Convert.ToDateTime(dateTimePickerDtInicio.Text);
                }

                if (dateTimePickerDtFim.Text != string.Empty)
                {
                    dataFim = Convert.ToDateTime(dateTimePickerDtFim.Text);
                    DateTime dtFim = dataFim ?? DateTime.Now;
                    dtFim.AddDays(1);
                    dataFim = dtFim;
                }

                int idNegocio = tipoNegocio != null ? tipoNegocio.id : 0;
                ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();
                
                
                var masterData = string.Empty;
                if (textBoxIdProposta.Text != null && textBoxIdProposta.Text != string.Empty)
                {
                    int idPropostaT;
                    bool isIdValid = int.TryParse(textBoxIdProposta.Text, out idPropostaT);
                    if (!isIdValid)
                    {
                        HandleException("Num proposta inválida");
                    }
                    else
                    {
                        var idPropostaS = (textBoxIdProposta.Text != null && textBoxIdProposta.Text != string.Empty) ? Convert.ToInt32(textBoxIdProposta.Text) : 0;
                        masterData = service.GetProposal("", "", 0, idPropostaS, "", null, null, null, "", sessKey, macAddress);
                    }

                }
                else
                {
                    masterData = service.GetProposal(estadoProposta, nomeCliente, idNegocio, idProposta, nifCliente, isCreditex, dataInicio, dataFim, vendedorUser, sessKey, macAddress);
                }
                
                
                

                var serializable = new System.Web.Script.Serialization.JavaScriptSerializer();
                serializable.MaxJsonLength = 214783644;
                var dataDes = serializable.Deserialize<ProposalDataReturn>(masterData);

                var orderResults = from p in dataDes.propostasList
                                   orderby p.Urgente descending, p.NumProposta ascending
                                    select p;

                dataDes.propostasList = orderResults.ToList();

                List<ExcelAddInVariables> valueNegociosList = new List<ExcelAddInVariables>();
                valueNegociosList = (List<ExcelAddInVariables>)bindingSourceTipoNegocio.DataSource;



                List<PropostaGrid> propostaListSearched = new List<PropostaGrid>();

                foreach (ProposalExcel prop in dataDes.propostasList)
                {
                    string negocio = string.Empty;
                    if (prop.tipoNegocioCod != null)
                        negocio = valueNegociosList.First(s => s.id == prop.tipoNegocioCod).descricao;
                    PropostaGrid propToAdd = new PropostaGrid
                    {
                        Urgente = (prop.Urgente==true)?"Sim":"Não",
                        NumProposta = prop.NumProposta.ToString(),
                        Cliente = prop.nomeCliente,
                        DataCriacao = prop.dataCriacao,
                        TipoNegocio = negocio,
                        Vendedor = prop.vendedorNome,
                        Estado = prop.EstadoProposta
                    };
                    propostaListSearched.Add(propToAdd);

                }


                dataGridView1.DataSource = propostaListSearched;

                this.listaPropostas = dataDes.propostasList;

            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                bool canContinue = true;
                DataGridView dgv = sender as DataGridView;
                if (dgv == null)
                    return;
                if (dgv.CurrentRow.Selected)
                {
                    ProposalExcel selected = listaPropostas.First(o => o.NumProposta.ToString() == dgv.CurrentRow.Cells[1].Value.ToString());

                    SessionKeyRegistry register = new SessionKeyRegistry();
                    register.Write("CheckIsSetProposal","1");

                    foreach (Worksheet sheet in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
                    {



                        if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                        {

                            List<ExcelAddInVariables> valueNegociosList = new List<ExcelAddInVariables>();
                            valueNegociosList = (List<ExcelAddInVariables>)bindingSourceTipoNegocio.DataSource;

                            string tipoNegocio = string.Empty;
                            if (selected.tipoNegocioCod != null)
                            {
                                var tipoNegocioIt = valueNegociosList.Find(s => s.id == selected.tipoNegocioCod);
                                if (tipoNegocioIt != null) tipoNegocio = tipoNegocioIt.descricao;
                                sheet.Range["B28"].Value = sheet.Range["J10"].Value;
                                sheet.Range["J10"].Value = tipoNegocio;
                                this.UnProtectSheet(Settings.Default.SheetDadosGerais);
                                this.UnProtectSheet(Settings.Default.SheetRecursosHumanos);
                                this.UnProtectSheet(Settings.Default.SheetSoftware);
                                this.UnProtectSheet(Settings.Default.SheetOutrosServicos);
                                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                                string canContinueStr = Convert.ToString(sheet.Range["B29"].Value);
                                if (canContinueStr.Trim().Equals("Nao"))
                                    canContinue = false;

                            }

                            if (canContinue)
                            {
                                string perfilArea = string.Empty;
                                if (selected.perfilAreaCod != null)
                                {
                                    var perfilAreaIt = listaPerfilAreas.Find(s => s.Id == selected.perfilAreaCod);
                                    if (perfilAreaIt != null) perfilArea = perfilAreaIt.Desc;
                                }

                                string periocidade = string.Empty;
                                if (selected.periocidadeCod != null)
                                {
                                    var periocidadeIt = listaPeriocidades.Find(s => s.id == selected.periocidadeCod);
                                    if (periocidadeIt != null) periocidade = periocidadeIt.descricao;
                                }

                                string copia = string.Empty;
                                if (selected.periodoLeituraCopiasCod != null)
                                {
                                    var copiaIt = listaCopias.Find(s => s.id == selected.periodoLeituraCopiasCod);
                                    if (copiaIt != null) copia = copiaIt.descricao;
                                }

                                string carencia = string.Empty;
                                if (selected.carenciaCod != null)
                                {
                                    var carenciaIt = listaCarencias.Find(s => s.id == selected.carenciaCod);
                                    if (carenciaIt != null) carencia = carenciaIt.descricao;
                                }

                                string parceiro = string.Empty;
                                if (selected.parceiroCod != null)
                                {
                                    var parceiroIt = listaParceiros.Find(s => s.Id == selected.parceiroCod);
                                    if (parceiroIt != null) parceiro = parceiroIt.Desc;
                                }

                                string claus = "Não";
                                if (selected.clausuradoStandard != null && selected.clausuradoStandard==true)
                                {
                                    claus = "Sim";
                                }
                                string comAdc = "Não";
                                if (selected.comAdc != null && selected.comAdc == true)
                                {
                                    comAdc = "Sim";
                                }

                                string creditex = "Não";
                                if (selected.temCreditex != null && selected.temCreditex == true)
                                {
                                    creditex = "Sim";
                                }

                                string urgente = "Não";
                                if (selected.Urgente != null && selected.Urgente == true)
                                {
                                    urgente = "Sim";
                                }

                                string isProposalDuplicated = register.Read("CheckIsProposalDuplicated");
                               

                                if (isProposalDuplicated == "1")
                                {
                                    sheet.Range["C4"].Value = string.Empty;
                                    sheet.Range["C5"].Value = string.Empty;
                                    sheet.Range["C21"].Value = string.Empty;
                                }
                                else
                                {
                                    sheet.Range["C4"].Value = selected.NumProposta;
                                    sheet.Range["C5"].Value = selected.EstadoProposta;
                                    sheet.Range["C21"].Value = selected.GuidProposta;
                                }

                                sheet.Range["C9"].Value = selected.nomeCliente;
                                sheet.Range["C10"].Value = selected.numContribuinteCliente;
                                sheet.Range["C11"].Value = selected.emailCliente;
                                sheet.Range["C12"].Value = selected.websiteCliente;
                                sheet.Range["C14"].Value = selected.moradaCliente;
                                sheet.Range["C15"].Value = selected.codigoPostalCliente;
                                sheet.Range["E15"].Value = selected.codigoPostalCliente3;
                                sheet.Range["C16"].Value = selected.localidadeCliente;
                                sheet.Range["C17"].Value = selected.telefoneCliente;
                                sheet.Range["J9"].Value = perfilArea;
                                sheet.Range["P9"].Value = parceiro;
                                sheet.Range["P10"].Value = selected.tipoContratoDesc;

                                sheet.Range["J15"].Value = periocidade;
                                sheet.Range["P15"].Value = copia;
                                sheet.Range["J16"].Value = carencia;
                                sheet.Range["P16"].Value = selected.nBid;
                                sheet.Range["J17"].Value = (selected.duracaoContrato != null) ? selected.duracaoContrato.ToString() : string.Empty;
                                sheet.Range["P16"].Value = selected.nBid;
                                sheet.Range["P11"].Value = claus;
                                sheet.Range["J18"].Value = comAdc;
                                sheet.Range["J19"].Value = creditex;
                                sheet.Range["J20"].Value = urgente;

                                
                                sheet.Range["P18"].Value = (selected.factorCreditex != null) ? selected.factorCreditex.ToString() : string.Empty;
                                sheet.Range["P17"].Value = selected.tabelaFactoresCod;
                            }

                        }

                        if (canContinue)
                        {
                            if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                            {

                                if (selected.equipamentosServicosList != null && selected.equipamentosServicosList.Count > 0)
                                {
                                    string[] columnMapping = new string[] { "tipoEquipamentoDesc", "codigo", "modelo", "quantidade", "precoLista", "", "margem", "", "precoLiqUni", "", "idPart", "topUp", "descricao", "GuidEquipInst", "", "TFMreferenciaServico", "TFMdescontoServico", "", "VolumeRefCorServico", "VolumeCliCorServico", "PrecoExtCorServico", "", "SubTotalCorServico", "", "VolumeRefPBServico", "VolumeCliPBServico", "PrecoExtPBServico", "", "SubTotalPBServico", "", "VolumeRef3CServico", "VolumeCli3CServico", "PrecoExt3CServico", "", "GuidPagePack", "SubTotal3CServico", "", "" };
                                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);


                                    System.Data.DataTable data = new System.Data.DataTable();
                                    data = ConvertToDataTable(selected.equipamentosServicosList);

                                    SheetObject.SetDataBinding(data.DefaultView, null, columnMapping);

                                    long insertLineFix = sheet.Cells.Find("Totais Instalar").Row;
                                    Range LineFix = sheet.Range["A" + insertLineFix + ":AL" + insertLineFix + ""];
                                    LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                                }
                                else
                                {
                                    EliminarAllEquipInstalarServico(sheet);
                                }
                                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                                EliminarAllEquipRemover(sheet);
                                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                                if (selected.equipamentosRemoverList != null && selected.equipamentosRemoverList.Count > 0)
                                {
                                    string[] columnMappingRem = new string[] { "", "NumSerie", "Modelo", "tipoRetirada", "valor", "is3PartyDesc", "dataInicioContrato", "dataFimContrato", "dataTermoContrato", "observacoes", "" };
                                    Microsoft.Office.Tools.Excel.ListObject SheetObjectRem = Globals.Factory.GetVstoObject(sheet.ListObjects[2]);


                                    System.Data.DataTable dataRem = new System.Data.DataTable();
                                    dataRem = ConvertToDataTable(selected.equipamentosRemoverList);

                                    SheetObjectRem.SetDataBinding(dataRem.DefaultView, null, columnMappingRem);

                                    long insertLineFix = sheet.Cells.Find("Totais Remover").Row;
                                    Range LineFix = sheet.Range["AN" + insertLineFix + ":AX" + insertLineFix + ""];
                                    LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                                }
                                
                                

                            }

                            if (sheet.Name.Equals(Settings.Default.SheetRecursosHumanos))
                            {
                                if (selected.recursosHumanosList != null && selected.recursosHumanosList.Count > 0)
                                {
                                    string[] columnMapping = new string[] { "nivelOperadorDesc", "descricao", "", "quantidade", "custoMensal", "RendaMensal", "", "" };
                                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);


                                    System.Data.DataTable data = new System.Data.DataTable();
                                    data = ConvertToDataTable(selected.recursosHumanosList);

                                    SheetObject.SetDataBinding(data.DefaultView, null, columnMapping);

                                    setDuracaoContratoCell(sheet, data.Rows.Count, "K");

                                    long insertLineFix = sheet.Cells.Find("Totais").Row;
                                    Range LineFix = sheet.Range["A" + insertLineFix + ":Z" + insertLineFix + ""];
                                    LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                                }
                                else
                                {
                                    EliminarAllRecursosHumanos(sheet);
                                }
                            }

                            if (sheet.Name.Equals(Settings.Default.SheetSoftware))
                            {
                                if (selected.softwareList != null && selected.softwareList.Count > 0)
                                {
                                    string[] columnMapping = new string[] { "designacao", "preco", "", "total", "quantidade", "", "manutencao", "", "manutencaoTotal", "primeiroAnoIncuidoDesc", "is3PartyDesc", "","" };
                                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);


                                    System.Data.DataTable data = new System.Data.DataTable();
                                    data = ConvertToDataTable(selected.softwareList);

                                    SheetObject.SetDataBinding(data.DefaultView, null, columnMapping);

                                    setDuracaoContratoCell(sheet, data.Rows.Count, "O");
                                    setFactorCell(sheet, data.Rows.Count, "P");

                                    long insertLineFix = sheet.Cells.Find("Totais").Row;
                                    Range LineFix = sheet.Range["A" + insertLineFix + ":Z" + insertLineFix + ""];
                                    LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

                                }
                                else
                                {
                                    EliminarAllSofware(sheet);
                                }
                            }

                            if (sheet.Name.Equals(Settings.Default.SheetOutrosServicos))
                            {
                                if (selected.outrosServicosList != null && selected.outrosServicosList.Count > 0)
                                {
                                    string[] columnMapping = new string[] { "descricao", "precoLista", "", "total", "quantidade", "", "" };
                                    Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);


                                    System.Data.DataTable data = new System.Data.DataTable();
                                    data = ConvertToDataTable(selected.outrosServicosList);

                                    SheetObject.SetDataBinding(data.DefaultView, null, columnMapping);

                                    setDuracaoContratoCell(sheet, data.Rows.Count, "J");

                                    long insertLineFix = sheet.Cells.Find("Totais").Row;
                                    Range LineFix = sheet.Range["A" + insertLineFix + ":Z" + insertLineFix + ""];
                                    LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                                }
                                else
                                {
                                    EliminarAllOutrosServ(sheet);
                                }
                            }
                        }

                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            
        }

        //CODIGO DUPLICADO - PENSAR EM PASSAR PARA A CAMADA DE BUSINESS

        private void EliminarAllOutrosServ(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetOutrosServicos);
                long totaisLine = sheet.Cells.Find("Totais").Row;
                totaisDy = totaisLine;

                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":G" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["E3"].Value = "";

                    }

                    totaisDy = sheet.Cells.Find("Totais").Row;
                }
                this.ProtectSheet(Settings.Default.SheetOutrosServicos);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
            }
        }

        private void EliminarAllSofware(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetSoftware);
                long totaisLine = sheet.Cells.Find("Totais").Row;
                totaisDy = totaisLine;

                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":M" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["E3"].Value = "";
                        sheet.Range["G3"].Value = "";
                        sheet.Range["I3"].Value = "";
                        sheet.Range["J3"].Value = "";
                        sheet.Range["K3"].Value = "";

                    }

                    totaisDy = sheet.Cells.Find("Totais").Row;
                }
                this.ProtectSheet(Settings.Default.SheetSoftware);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
            }
        }

        public void EliminarAllRecursosHumanos(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetRecursosHumanos);
                long totaisLine = sheet.Cells.Find("Totais").Row;
                totaisDy = totaisLine;

                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":H" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {

                        //Range Line = sheet.Range["A3:E3"];
                        //Line.Value = "";
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["F3"].Value = "";
                    }

                    totaisDy = sheet.Cells.Find("Totais").Row;
                }
                this.ProtectSheet(Settings.Default.SheetRecursosHumanos);
            }
            catch (Exception ex)
            {

                HandleException(ex.Message);

            }
        }

        private void EliminarAllEquipInstalarServico(Worksheet sheet)
        {
            try
            {
                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                long totaisLine = sheet.Cells.Find("Totais Instalar").Row;
                totaisDy = totaisLine;


                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["A" + index + ":AL" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    else
                    {
                        sheet.Range["A3"].Value = "";
                        sheet.Range["B3"].Value = "";
                        sheet.Range["C3"].Value = "";
                        sheet.Range["D3"].Value = "";
                        sheet.Range["E3"].Value = "";
                        sheet.Range["G3"].Value = "";
                        sheet.Range["H3"].Value = "";
                        sheet.Range["I3"].Value = "";
                        sheet.Range["K3"].Value = "";
                        sheet.Range["L3"].Value = "";
                        sheet.Range["M3"].Value = "";
                        sheet.Range["N3"].Value = "";
                        sheet.Range["O3"].Value = "";
                        sheet.Range["P3"].Value = "";
                        sheet.Range["Q3"].Value = "";

                        sheet.Range["S3"].Value = "";
                        sheet.Range["T3"].Value = "";
                        sheet.Range["U3"].Value = "";
                        sheet.Range["W3"].Value = "";


                        sheet.Range["Y3"].Value = "";
                        sheet.Range["Z3"].Value = "";
                        sheet.Range["AA3"].Value = "";
                        sheet.Range["AC3"].Value = "";

                        sheet.Range["AE3"].Value = "";
                        sheet.Range["AF3"].Value = "";
                        sheet.Range["AG3"].Value = "";
                        sheet.Range["AJ3"].Value = "";
                        sheet.Range["AI3"].Value = "";

                    }

                    totaisDy = sheet.Cells.Find("Totais Instalar").Row;

                }

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
            }

        }

        private void EliminarAllEquipRemover(Worksheet sheet)
        {
            try
            {

                long totaisDy = 0;
                this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                long totaisLine = sheet.Cells.Find("Totais Remover").Row;
                totaisDy = totaisLine;


                for (int i = 3; i < totaisLine; i++)
                {

                    int index = 4;
                    if (totaisDy != 4)
                    {
                        Range Line = sheet.Range["AN" + index + ":AX" + index + ""];
                        Line.Delete(Microsoft.Office.Interop.Excel.XlDeleteShiftDirection.xlShiftUp);
                        //Line.Delete();
                    }
                    else
                    {
                        //Range Line = (Range)sheet.Rows[selection.Row];
                        Range Line = sheet.Range["AN3:AW3"];
                        Line.Value = "";
                    }

                    totaisDy = sheet.Cells.Find("Totais Remover").Row;
                }

                this.ProtectSheet(Settings.Default.SheetEquipInstalar);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
            }
        }

        private void ProtectSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();
            if (Sheet != null)
            {
                this.ProtectSheet(Sheet);
            }
        }

        private void ProtectSheet(Worksheet Sheet, bool AllowNewLines = false, bool AllowPivot = false)
        {
            if (!Sheet.ProtectContents ||
               !Sheet.ProtectDrawingObjects ||
               !Sheet.ProtectScenarios)
                Sheet.Protect(SheetProtectionPassword, true, true, true, false, false, false, false, false, AllowNewLines, false, false, AllowNewLines, true, true, AllowPivot);
        }

        private void UnProtectSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();
            if (Sheet != null)
            {
                this.UnProtectSheet(Sheet);
            }
        }

        private void UnProtectSheet(Worksheet Sheet)
        {
            if (Sheet.ProtectContents ||
                Sheet.ProtectDrawingObjects ||
                Sheet.ProtectScenarios)
                Sheet.Unprotect(SheetProtectionPassword);
        }

        public void setDuracaoContratoCell(Worksheet sheet, int totLines, string col)
        {
            for (int i = 3; i <= totLines+3; i++)
            {
                sheet.Range[col + i].Value = "=DadosClienteGerais!J17";
            }
        }

        public void setFactorCell(Worksheet sheet, int totLines, string col)
        {
            for (int i = 3; i <= totLines + 3; i++)
            {
                sheet.Range[col + i].Value = "=DadosClienteGerais!P18";
            }
        }

        public System.Data.DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            System.Data.DataTable table = new System.Data.DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        private void PesquisaForm_Load(object sender, EventArgs e)
        {
            comboBoxVendedor.Text = string.Empty;
            comboBoxTipoNegocio.Text = string.Empty;
            comboBoxEstadoProposta.Text = string.Empty;


        }


        private void buttonFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonLimparDados_Click(object sender, EventArgs e)
        {
            textBoxIdProposta.Text = string.Empty;
            textBoxNomeCliente.Text = string.Empty;
            textBoxNifCliente.Text = string.Empty;
            comboBoxEstadoProposta.Text = string.Empty;
            comboBoxTipoNegocio.Text = string.Empty;
            radioButtonCreditexNao.Checked = false;
            radioButtonCreditexSim.Checked = false;
            comboBoxVendedor.Text = string.Empty;
            dateTimePickerDtInicio.Text = DateTime.Now.ToShortDateString();
            dateTimePickerDtFim.Text = DateTime.Now.ToShortDateString();

        }


        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void PesquisaForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonPesquisar_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                buttonFechar_Click(sender, e);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

    }
}
