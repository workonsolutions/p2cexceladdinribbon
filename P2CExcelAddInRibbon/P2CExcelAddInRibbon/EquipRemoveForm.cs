﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;

namespace ExcelAddIn
{
    public partial class EquipRemoveForm : Form
    {

        public EquipRemoveForm()
        {
            InitializeComponent();
        }

        private void InsertDataInTable()
        {
            try
            {
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                long insertLine = sheet.Cells.Find("Totais Remover").Row;
                var fristLine = sheet.Range["AO3"].Value;
                long row = 0;

                if (fristLine != null && fristLine != string.Empty)
                {
                    Range Line = sheet.Range["AN" + insertLine + ":AX" + insertLine + ""];
                    Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    row = insertLine;
                }
                else
                    row = 3;

                string tipoRetirada = string.Empty;
                string is3Party = "Não";

                if (radioButtonRetoma.Checked)
                    tipoRetirada = "Retoma";
                if (radioButtonTopUp.Checked)
                    tipoRetirada = "TopUp";

                if (checkBox3Party.Checked)
                    is3Party = "Sim";

                sheet.Range["AO" + row].Value = textBoxNumSerie.Text;
                sheet.Range["AP" + row].Value = textBoxModelo.Text;
                sheet.Range["AQ" + row].Value = tipoRetirada;
                sheet.Range["AR" + row].Value = Convert.ToDouble((textBoxPreco.Text == string.Empty) ? "0" : textBoxPreco.Text);
                sheet.Range["AS" + row].Value = is3Party;
                sheet.Range["AT" + row].Value = dateTimePickerDtInicio.Text;
                sheet.Range["AU" + row].Value = dateTimePickerDtFim.Text;
                sheet.Range["AV" + row].Value = dateTimePickerDtTermo.Text;
                sheet.Range["AW" + row].Value = textBoxObservacoes.Text;
                //sheet.Range["H" + row].Formula = "=E" + row + "-(E" + row + "*(0.01*F" + row + "))";
                //sheet.Range["I" + row].Formula = "=H"+row+"*D"+row;

                var rangeSel = sheet.get_Range("AO3", "AO3");
                rangeSel.Select();

                long insertLineFix = sheet.Cells.Find("Totais Remover").Row;
                Range LineFix = sheet.Range["AN" + insertLineFix + ":AX" + insertLineFix + ""];
                LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            this.Close();
        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }


        private bool ValidateControls()
        {
            bool isValidated = true;

            double preco = 0.0D;
            bool validpreco = double.TryParse(textBoxPreco.Text, out preco);

            if (!validpreco && textBoxPreco.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxPreco, "Preço Inválido");
            }



            if (textBoxNumSerie.Text==null || textBoxNumSerie.Text==string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxNumSerie, "Número de Série obrigatório");
            }


            return isValidated;
        }

        private void buttonInserir_Click(object sender, EventArgs e)
        {

            if (ValidateControls())
                InsertDataInTable();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
