﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using P2CExcelAddInRibbon.Properties;
using P2CExcelAddIn.Business.ServicesController;
using SpiritucSI.Xerox.Workflow.Business;
using P2CExcelAddIn.Business.ProposalP2CtoExcelAddIn;
using P2CExcelAddIn.Business.Security;

namespace ExcelAddIn
{
    public partial class SavePropostaForm : Form
    {

        public ProposalExcel propostaToSend { get; set; }
        bool isNewProposal { get; set; }
        public string macAddress { get; set; }
        public string encryptKey { get; set; }
        private string SheetProtectionPassword = "!xerox2013";


        public SavePropostaForm()
        {
            InitializeComponent();
        }

        private void SavePropostaForm_Load(object sender, EventArgs e)
        {
            if (propostaToSend.GuidProposta != null && propostaToSend.GuidProposta != string.Empty)
            {
                //proposta ja existente
                labelText.Text = labelText.Text + " " + propostaToSend.NumProposta.ToString();
                buttonAtualizarDraft.Text = "Actualizar Proposta";
                isNewProposal = false;
            }
            else
            {
                //proposta nova
                labelText.Text = "Seleccione a acção a realizar para criação da nova proposta";
                buttonAtualizarDraft.Text = "Criar Em Construção";
                isNewProposal = true;
            }

        }

        private void buttonAtualizarDraft_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                
                SessionKeyRegistry register = new SessionKeyRegistry();
                string dtSessao = register.Read("ChaveSessao");
                string userSessao = register.Read("UserNameSessao");
                string sessKey = userSessao + "<------&#Xerox2013#&----->pass<------&#Xerox2013#&----->" + dtSessao;

                sessKey = register.Encrypt(sessKey, true, encryptKey);
                ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();
                var retData = service.CreateProposal(propostaToSend, sessKey, true, macAddress);

                ExcelAddIn.MensagensForm msgForm = new MensagensForm();
                if (retData.status.sucess)
                {
                    SetDataProposal(retData);

                    msgForm.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.ok_48;
                    if (isNewProposal)
                    {
                        var masterData = service.GetProposal("", "", 0, retData.NumProposta, "", null, null, null, "", sessKey, macAddress);
                        var serializable = new System.Web.Script.Serialization.JavaScriptSerializer();
                        serializable.MaxJsonLength = 2147483644;
                        var dataDes = serializable.Deserialize<ProposalDataReturn>(masterData);
                        UpdateGuidEquipInstal(dataDes.propostasList.FirstOrDefault());
                        msgForm.msgToShow = "Proposta criada com sucesso";
                    }
                    else
                    {
                        msgForm.msgToShow = "Proposta actualizada com sucesso";
                    }
                }
                else
                {
                    msgForm.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                    msgForm.msgToShow = "Erro ao enviar os dados da prosposta:" + retData.status.errorMessage;
                }

                this.Close();


                msgForm.ShowDialog();
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void UpdateGuidEquipInstal(ProposalExcel proposta)
        {
            foreach (Worksheet sheet in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetEquipInstalar))
                {
                    if (proposta.equipamentosServicosList != null && proposta.equipamentosServicosList.Count > 0)
                    {
                        this.UnProtectSheet(Settings.Default.SheetEquipInstalar);
                        string[] columnMapping = new string[] { "tipoEquipamentoDesc", "codigo", "modelo", "quantidade", "precoLista", "desconto", "margem", "", "", "", "idPart", "topUp", "descricao", "GuidEquipInst", "", "TFMreferenciaServico", "TFMdescontoServico", "", "VolumeRefCorServico", "VolumeCliCorServico", "PrecoExtCorServico", "DescontoCorServico", "", "", "VolumeRefPBServico", "VolumeCliPBServico", "PrecoExtPBServico", "DescontoPBServico", "", "", "VolumeRef3CServico", "VolumeCli3CServico", "PrecoExt3CServico", "Desconto3CServico", "GuidPagePack", "", "", "" };
                        Microsoft.Office.Tools.Excel.ListObject SheetObject = Globals.Factory.GetVstoObject(sheet.ListObjects[1]);


                        System.Data.DataTable data = new System.Data.DataTable();
                        data = ConvertToDataTable(proposta.equipamentosServicosList);

                        SheetObject.SetDataBinding(data.DefaultView, null, columnMapping);

                        long insertLineFix = sheet.Cells.Find("Totais Instalar").Row;
                        Range LineFix = sheet.Range["A" + insertLineFix + ":AL" + insertLineFix + ""];
                        LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                    }
                }
            }
        }

        public System.Data.DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            System.Data.DataTable table = new System.Data.DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        private void buttonAvancaAprovacao_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
               // ProposalP2CtoExcelAddInClient service = new ProposalP2CtoExcelAddInClient();
                SessionKeyRegistry register = new SessionKeyRegistry();
                string dtSessao = register.Read("ChaveSessao");
                string userSessao = register.Read("UserNameSessao");
                string sessKey = userSessao + "<------&#Xerox2013#&----->pass<------&#Xerox2013#&----->" + dtSessao;

                sessKey = register.Encrypt(sessKey, true, encryptKey);
                ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();
                var retData = service.CreateProposal(propostaToSend, sessKey, false, macAddress);

                ExcelAddIn.MensagensForm msgForm = new MensagensForm();
                if (retData.status.sucess)
                {

                    string estadoProposa = service.GetProposalState(retData.GuidProposta, sessKey, macAddress);
                    retData.estadoProposta = estadoProposa;
                    SetDataProposal(retData);
                    if (!retData.estadoProposta.Equals("Draft"))
                    {
                        msgForm.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.ok_48;
                        if (isNewProposal)
                            msgForm.msgToShow = "Proposta criada no estado PendenteAprovacao com sucesso";
                        else
                            msgForm.msgToShow = "Proposta actualizada para o estado PendenteAprovacao com sucesso";

                    }
                    else
                    {
                        msgForm.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                        msgForm.msgToShow = "Não foi possivel avançar o estado da proposta, verifique se todas as condições estão reunidas para poder avançar";
                    }

                }
                else
                {
                    msgForm.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
                    msgForm.msgToShow = "Erro ao enviar os dados da prosposta:\n" + retData.status.errorMessage;
                }

                this.Cursor = Cursors.Arrow;

                this.Close();

                msgForm.ShowDialog();
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SetDataProposal(ProposalReturnStatus retData)
        {
            foreach (Worksheet sheet in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
            {
                if (sheet.Name.Equals(Settings.Default.SheetDadosGerais))
                {
                    sheet.Range["C4"].Value = retData.NumProposta.ToString();
                    sheet.Range["C5"].Value = retData.estadoProposta;
                    sheet.Range["C21"].Value = retData.GuidProposta;
                }
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void UnProtectSheet(string SheetName)
        {
            Worksheet Sheet = (from Worksheet ws in ExcelAddIn.Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets
                               where ws.Name == SheetName
                               select ws).FirstOrDefault();
            if (Sheet != null)
            {
                this.UnProtectSheet(Sheet);
            }
        }

        private void UnProtectSheet(Worksheet Sheet)
        {
            if (Sheet.ProtectContents ||
                Sheet.ProtectDrawingObjects ||
                Sheet.ProtectScenarios)
                Sheet.Unprotect(SheetProtectionPassword);
        }



    }
}
