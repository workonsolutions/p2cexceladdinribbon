﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using P2CExcelAddIn.Business.ServicesController;
using P2CExcelAddIn.Business.Security;

namespace ExcelAddIn
{
    public partial class LoginForm : Form
    {


        public bool IsLoggedIn { get; set; }
        public string macAddress { get; set; }
        public string encriptKey { get; set; }

        public LoginForm()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                SessionKeyRegistry register = new SessionKeyRegistry();

                this.Cursor = Cursors.WaitCursor;

                var sessionKey = textBoxLogin.Text + "<------&#Xerox2013#&----->" + textBoxPass.Text + "<------&#Xerox2013#&----->" + DateTime.Now.ToShortDateString();

                sessionKey = register.Encrypt(sessionKey, true, encriptKey);
                ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();
                var returnLogin = service.Login(sessionKey, macAddress);

                if (returnLogin.sucess)
                {

                    register.Write("ChaveSessao", DateTime.Now.ToString());
                    register.Write("UserNameSessao", textBoxLogin.Text);
                    labelErrrorMsg.Visible = false;
                    IsLoggedIn = true;
                    this.Close();
                }
                else
                {
                    labelErrrorMsg.Visible = true;
                    labelErrrorMsg.Text = returnLogin.errorMessage;
                }

                this.Cursor = Cursors.Default;


            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            finally 
            {
                this.Cursor = Cursors.Default;
            }

        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonLogin_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                button1_Click(sender, e);
            }
        }
    }
}
