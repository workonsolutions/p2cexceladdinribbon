﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;
using P2CExcelAddInRibbon.Properties;

namespace ExcelAddIn
{
    public partial class RHForm : Form
    {
        public IEnumerable<object> DataSource_Niveis
        {
            get
            {
                return (IEnumerable<object>)bindingSourceNiveis.DataSource;
            }
            set
            {
                bindingSourceNiveis.DataSource = value;
                comboBoxNoperador.DataSource = bindingSourceNiveis;
                comboBoxNoperador.DisplayMember = "NivelRecurso";
            }
        }


        public RHForm()
        {
            InitializeComponent();
        }

        private void InsertDataInTable()
        {
            try
            {
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                long insertLine = sheet.Cells.Find("Totais").Row;
                string fristLine = sheet.Range["A3"].Value;
                long row = 0;

                if (fristLine != null && fristLine != string.Empty)
                {
                    Range Line = sheet.Range["A" + insertLine + ":H" + insertLine + ""];
                    Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    row = insertLine;
                }
                else
                    row = 3;

                double rendaMensal = Convert.ToDouble((textBoxPrecoBase.Text == string.Empty) ? "0" : textBoxPrecoBase.Text);
                if (textBoxUpLift.Text != string.Empty)
                {
                    rendaMensal = rendaMensal * (1 + Convert.ToDouble(textBoxUpLift.Text) / 100) * Convert.ToDouble(textBoxQtd.Text);
                }

                sheet.Range["K" + row].Value = "=DadosClienteGerais!J17";
                sheet.Range["A" + row].Value = comboBoxNoperador.Text;
                sheet.Range["B" + row].Value = textBoxDescricao.Text;
                //sheet.Range["C" + row].Value = Convert.ToDouble((textBoxUpLift.Text == string.Empty) ? "0" : textBoxUpLift.Text);
                sheet.Range["D" + row].Value = Convert.ToDouble((textBoxQtd.Text == string.Empty) ? "0" : textBoxQtd.Text);
                sheet.Range["E" + row].Value = Convert.ToDouble((textBoxPrecoBase.Text == string.Empty) ? "0" : textBoxPrecoBase.Text);
                sheet.Range["F" + row].Value = rendaMensal;
                sheet.Range["H" + row].Value = "=F" + row + "*" + "K" + row + "";

                long insertLineFix = sheet.Cells.Find("Totais").Row;
                Range LineFix = sheet.Range["A" + insertLineFix + ":Z" + insertLineFix + ""];
                LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            this.Close();
        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private bool ValidateControls()
        {
            bool isValidated = true;

            double upLift = 0.0D;
            bool validUpLift = double.TryParse(textBoxUpLift.Text, out upLift);

            if ((!validUpLift || upLift<0) && textBoxUpLift.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxUpLift, "UpLift Inválido");
            }

            int quantidade = 0;
            bool validQtd = int.TryParse(textBoxQtd.Text, out quantidade);

            if (!validQtd || quantidade<0)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxQtd, "Quantidade Inválida");
            }

            return isValidated;

        }

        private void buttonInserir_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
                InsertDataInTable();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxNoperador_SelectedIndexChanged(object sender, EventArgs e)
        {

            var nivelIt = (ExcelNivelOperador)comboBoxNoperador.SelectedItem;

            textBoxPrecoBase.Text = nivelIt.CustoMensal.ToString();
        }

        private void RHForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonInserir_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                buttonCancelar_Click(sender, e);
            }
        }

        
    }
}
