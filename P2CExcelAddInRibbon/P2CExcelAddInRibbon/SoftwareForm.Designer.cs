﻿namespace ExcelAddIn
{
    partial class SoftwareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox1ano = new System.Windows.Forms.CheckBox();
            this.textBoxDescontoManutencao = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxManutAno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxDesconto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPrecoLista = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox3Party = new System.Windows.Forms.CheckBox();
            this.textBoxQuantidade = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxDesignacao = new System.Windows.Forms.TextBox();
            this.labelCodigo = new System.Windows.Forms.Label();
            this.buttonInserir = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.checkBox1ano);
            this.groupBox1.Controls.Add(this.textBoxDescontoManutencao);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxManutAno);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxDesconto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxPrecoLista);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.checkBox3Party);
            this.groupBox1.Controls.Add(this.textBoxQuantidade);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxDesignacao);
            this.groupBox1.Controls.Add(this.labelCodigo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 189);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Software";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(184, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "€";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(386, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 41;
            this.label8.Text = "€";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(386, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "%";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(561, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "%";
            // 
            // checkBox1ano
            // 
            this.checkBox1ano.AutoSize = true;
            this.checkBox1ano.Location = new System.Drawing.Point(416, 104);
            this.checkBox1ano.Name = "checkBox1ano";
            this.checkBox1ano.Size = new System.Drawing.Size(175, 17);
            this.checkBox1ano.TabIndex = 7;
            this.checkBox1ano.Text = "1º ano de manutenção incluído";
            this.checkBox1ano.UseVisualStyleBackColor = true;
            // 
            // textBoxDescontoManutencao
            // 
            this.textBoxDescontoManutencao.Location = new System.Drawing.Point(313, 101);
            this.textBoxDescontoManutencao.Name = "textBoxDescontoManutencao";
            this.textBoxDescontoManutencao.Size = new System.Drawing.Size(59, 20);
            this.textBoxDescontoManutencao.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(206, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Desc. Manutenção:";
            // 
            // textBoxManutAno
            // 
            this.textBoxManutAno.Location = new System.Drawing.Point(106, 101);
            this.textBoxManutAno.Name = "textBoxManutAno";
            this.textBoxManutAno.Size = new System.Drawing.Size(59, 20);
            this.textBoxManutAno.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Manutenção Ano:";
            // 
            // textBoxDesconto
            // 
            this.textBoxDesconto.Location = new System.Drawing.Point(482, 66);
            this.textBoxDesconto.Name = "textBoxDesconto";
            this.textBoxDesconto.Size = new System.Drawing.Size(59, 20);
            this.textBoxDesconto.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(413, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Desconto:";
            // 
            // textBoxPrecoLista
            // 
            this.textBoxPrecoLista.Location = new System.Drawing.Point(311, 66);
            this.textBoxPrecoLista.Name = "textBoxPrecoLista";
            this.textBoxPrecoLista.Size = new System.Drawing.Size(59, 20);
            this.textBoxPrecoLista.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(242, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Preço Lista:";
            // 
            // checkBox3Party
            // 
            this.checkBox3Party.AutoSize = true;
            this.checkBox3Party.Location = new System.Drawing.Point(106, 144);
            this.checkBox3Party.Name = "checkBox3Party";
            this.checkBox3Party.Size = new System.Drawing.Size(68, 17);
            this.checkBox3Party.TabIndex = 8;
            this.checkBox3Party.Text = "3rd Party";
            this.checkBox3Party.UseVisualStyleBackColor = true;
            // 
            // textBoxQuantidade
            // 
            this.textBoxQuantidade.Location = new System.Drawing.Point(106, 66);
            this.textBoxQuantidade.Name = "textBoxQuantidade";
            this.textBoxQuantidade.Size = new System.Drawing.Size(59, 20);
            this.textBoxQuantidade.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Quantidade:";
            // 
            // textBoxDesignacao
            // 
            this.textBoxDesignacao.Location = new System.Drawing.Point(106, 32);
            this.textBoxDesignacao.Name = "textBoxDesignacao";
            this.textBoxDesignacao.Size = new System.Drawing.Size(228, 20);
            this.textBoxDesignacao.TabIndex = 1;
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Location = new System.Drawing.Point(33, 32);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(67, 13);
            this.labelCodigo.TabIndex = 16;
            this.labelCodigo.Text = "Designação:";
            // 
            // buttonInserir
            // 
            this.buttonInserir.Location = new System.Drawing.Point(464, 220);
            this.buttonInserir.Name = "buttonInserir";
            this.buttonInserir.Size = new System.Drawing.Size(118, 23);
            this.buttonInserir.TabIndex = 9;
            this.buttonInserir.Text = "Inserir Software";
            this.buttonInserir.UseVisualStyleBackColor = true;
            this.buttonInserir.Click += new System.EventHandler(this.buttonInserir_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(383, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // SoftwareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 259);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonInserir);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SoftwareForm";
            this.Text = "Adicionar Software";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SoftwareForm_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.Button buttonInserir;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxDesignacao;
        private System.Windows.Forms.TextBox textBoxQuantidade;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox3Party;
        private System.Windows.Forms.TextBox textBoxPrecoLista;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1ano;
        private System.Windows.Forms.TextBox textBoxDescontoManutencao;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxManutAno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxDesconto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}