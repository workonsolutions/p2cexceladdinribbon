﻿namespace ExcelAddIn
{
    partial class EquipInstalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCodigo = new System.Windows.Forms.Label();
            this.labelDesc = new System.Windows.Forms.Label();
            this.textBoxDesc = new System.Windows.Forms.TextBox();
            this.labelModelo = new System.Windows.Forms.Label();
            this.textBoxModelo = new System.Windows.Forms.TextBox();
            this.labelDesconto = new System.Windows.Forms.Label();
            this.textBoxDesconto = new System.Windows.Forms.TextBox();
            this.labelMg = new System.Windows.Forms.Label();
            this.textBoxMg = new System.Windows.Forms.TextBox();
            this.labelPreco = new System.Windows.Forms.Label();
            this.textBoxPreco = new System.Windows.Forms.TextBox();
            this.labelQtd = new System.Windows.Forms.Label();
            this.textBoxQtd = new System.Windows.Forms.TextBox();
            this.comboBoxCodigo = new System.Windows.Forms.ComboBox();
            this.radioButton3party = new System.Windows.Forms.RadioButton();
            this.radioButtonCliente = new System.Windows.Forms.RadioButton();
            this.radioButtonXerox = new System.Windows.Forms.RadioButton();
            this.bindingSourceEquipamentos = new System.Windows.Forms.BindingSource(this.components);
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquipamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelCodigo);
            this.groupBox1.Controls.Add(this.labelDesc);
            this.groupBox1.Controls.Add(this.textBoxDesc);
            this.groupBox1.Controls.Add(this.labelModelo);
            this.groupBox1.Controls.Add(this.textBoxModelo);
            this.groupBox1.Controls.Add(this.labelDesconto);
            this.groupBox1.Controls.Add(this.textBoxDesconto);
            this.groupBox1.Controls.Add(this.labelMg);
            this.groupBox1.Controls.Add(this.textBoxMg);
            this.groupBox1.Controls.Add(this.labelPreco);
            this.groupBox1.Controls.Add(this.textBoxPreco);
            this.groupBox1.Controls.Add(this.labelQtd);
            this.groupBox1.Controls.Add(this.textBoxQtd);
            this.groupBox1.Controls.Add(this.comboBoxCodigo);
            this.groupBox1.Controls.Add(this.radioButton3party);
            this.groupBox1.Controls.Add(this.radioButtonCliente);
            this.groupBox1.Controls.Add(this.radioButtonXerox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(579, 184);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Equipamento a Instalar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(324, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "€";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "%";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "%";
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Location = new System.Drawing.Point(40, 70);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(43, 13);
            this.labelCodigo.TabIndex = 16;
            this.labelCodigo.Text = "Código:";
            // 
            // labelDesc
            // 
            this.labelDesc.AutoSize = true;
            this.labelDesc.Location = new System.Drawing.Point(365, 144);
            this.labelDesc.Name = "labelDesc";
            this.labelDesc.Size = new System.Drawing.Size(58, 13);
            this.labelDesc.TabIndex = 15;
            this.labelDesc.Text = "Descrição:";
            // 
            // textBoxDesc
            // 
            this.textBoxDesc.Enabled = false;
            this.textBoxDesc.Location = new System.Drawing.Point(432, 142);
            this.textBoxDesc.Name = "textBoxDesc";
            this.textBoxDesc.Size = new System.Drawing.Size(138, 20);
            this.textBoxDesc.TabIndex = 9;
            // 
            // labelModelo
            // 
            this.labelModelo.AutoSize = true;
            this.labelModelo.Location = new System.Drawing.Point(187, 144);
            this.labelModelo.Name = "labelModelo";
            this.labelModelo.Size = new System.Drawing.Size(45, 13);
            this.labelModelo.TabIndex = 13;
            this.labelModelo.Text = "Modelo:";
            // 
            // textBoxModelo
            // 
            this.textBoxModelo.Enabled = false;
            this.textBoxModelo.Location = new System.Drawing.Point(238, 142);
            this.textBoxModelo.Name = "textBoxModelo";
            this.textBoxModelo.Size = new System.Drawing.Size(71, 20);
            this.textBoxModelo.TabIndex = 8;
            // 
            // labelDesconto
            // 
            this.labelDesconto.AutoSize = true;
            this.labelDesconto.Location = new System.Drawing.Point(27, 144);
            this.labelDesconto.Name = "labelDesconto";
            this.labelDesconto.Size = new System.Drawing.Size(56, 13);
            this.labelDesconto.TabIndex = 11;
            this.labelDesconto.Text = "Desconto:";
            // 
            // textBoxDesconto
            // 
            this.textBoxDesconto.Location = new System.Drawing.Point(87, 141);
            this.textBoxDesconto.Name = "textBoxDesconto";
            this.textBoxDesconto.Size = new System.Drawing.Size(46, 20);
            this.textBoxDesconto.TabIndex = 7;
            // 
            // labelMg
            // 
            this.labelMg.AutoSize = true;
            this.labelMg.Location = new System.Drawing.Point(355, 110);
            this.labelMg.Name = "labelMg";
            this.labelMg.Size = new System.Drawing.Size(70, 13);
            this.labelMg.TabIndex = 9;
            this.labelMg.Text = "Mg. Parceiro:";
            // 
            // textBoxMg
            // 
            this.textBoxMg.Enabled = false;
            this.textBoxMg.Location = new System.Drawing.Point(431, 108);
            this.textBoxMg.Name = "textBoxMg";
            this.textBoxMg.Size = new System.Drawing.Size(66, 20);
            this.textBoxMg.TabIndex = 6;
            // 
            // labelPreco
            // 
            this.labelPreco.AutoSize = true;
            this.labelPreco.Location = new System.Drawing.Point(171, 110);
            this.labelPreco.Name = "labelPreco";
            this.labelPreco.Size = new System.Drawing.Size(63, 13);
            this.labelPreco.TabIndex = 7;
            this.labelPreco.Text = "Preço Lista:";
            // 
            // textBoxPreco
            // 
            this.textBoxPreco.Enabled = false;
            this.textBoxPreco.Location = new System.Drawing.Point(238, 108);
            this.textBoxPreco.Name = "textBoxPreco";
            this.textBoxPreco.Size = new System.Drawing.Size(71, 20);
            this.textBoxPreco.TabIndex = 5;
            // 
            // labelQtd
            // 
            this.labelQtd.AutoSize = true;
            this.labelQtd.Location = new System.Drawing.Point(18, 110);
            this.labelQtd.Name = "labelQtd";
            this.labelQtd.Size = new System.Drawing.Size(65, 13);
            this.labelQtd.TabIndex = 5;
            this.labelQtd.Text = "Quantidade:";
            // 
            // textBoxQtd
            // 
            this.textBoxQtd.Location = new System.Drawing.Point(87, 108);
            this.textBoxQtd.MaxLength = 3;
            this.textBoxQtd.Name = "textBoxQtd";
            this.textBoxQtd.Size = new System.Drawing.Size(29, 20);
            this.textBoxQtd.TabIndex = 4;
            // 
            // comboBoxCodigo
            // 
            this.comboBoxCodigo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxCodigo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxCodigo.FormattingEnabled = true;
            this.comboBoxCodigo.Location = new System.Drawing.Point(83, 67);
            this.comboBoxCodigo.Name = "comboBoxCodigo";
            this.comboBoxCodigo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCodigo.TabIndex = 3;
            this.comboBoxCodigo.SelectedIndexChanged += new System.EventHandler(this.comboBoxCodigo_SelectedIndexChanged);
            // 
            // radioButton3party
            // 
            this.radioButton3party.AutoSize = true;
            this.radioButton3party.Location = new System.Drawing.Point(218, 32);
            this.radioButton3party.Name = "radioButton3party";
            this.radioButton3party.Size = new System.Drawing.Size(67, 17);
            this.radioButton3party.TabIndex = 2;
            this.radioButton3party.TabStop = true;
            this.radioButton3party.Text = "3rd Party";
            this.radioButton3party.UseVisualStyleBackColor = true;
            this.radioButton3party.CheckedChanged += new System.EventHandler(this.radioButton3party_CheckedChanged);
            // 
            // radioButtonCliente
            // 
            this.radioButtonCliente.AutoSize = true;
            this.radioButtonCliente.Location = new System.Drawing.Point(116, 32);
            this.radioButtonCliente.Name = "radioButtonCliente";
            this.radioButtonCliente.Size = new System.Drawing.Size(90, 17);
            this.radioButtonCliente.TabIndex = 1;
            this.radioButtonCliente.TabStop = true;
            this.radioButtonCliente.Text = "Equip. Cliente";
            this.radioButtonCliente.UseVisualStyleBackColor = true;
            this.radioButtonCliente.CheckedChanged += new System.EventHandler(this.radioButtonCliente_CheckedChanged);
            // 
            // radioButtonXerox
            // 
            this.radioButtonXerox.AutoSize = true;
            this.radioButtonXerox.Checked = true;
            this.radioButtonXerox.Location = new System.Drawing.Point(21, 32);
            this.radioButtonXerox.Name = "radioButtonXerox";
            this.radioButtonXerox.Size = new System.Drawing.Size(85, 17);
            this.radioButtonXerox.TabIndex = 0;
            this.radioButtonXerox.TabStop = true;
            this.radioButtonXerox.Text = "Equip. Xerox";
            this.radioButtonXerox.UseVisualStyleBackColor = true;
            this.radioButtonXerox.CheckedChanged += new System.EventHandler(this.radioButtonXerox_CheckedChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(471, 202);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(118, 23);
            this.buttonOk.TabIndex = 10;
            this.buttonOk.Text = "Inserir Equipamento";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(382, 202);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelar.TabIndex = 11;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // EquipInstalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 240);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EquipInstalForm";
            this.Text = "Adicionar Equipamento a Instalar";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EquipInstalForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EquipInstalForm_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquipamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxCodigo;
        private System.Windows.Forms.RadioButton radioButton3party;
        private System.Windows.Forms.RadioButton radioButtonCliente;
        private System.Windows.Forms.RadioButton radioButtonXerox;
        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.Label labelDesc;
        private System.Windows.Forms.TextBox textBoxDesc;
        private System.Windows.Forms.Label labelModelo;
        private System.Windows.Forms.TextBox textBoxModelo;
        private System.Windows.Forms.Label labelDesconto;
        private System.Windows.Forms.TextBox textBoxDesconto;
        private System.Windows.Forms.Label labelMg;
        private System.Windows.Forms.TextBox textBoxMg;
        private System.Windows.Forms.Label labelPreco;
        private System.Windows.Forms.TextBox textBoxPreco;
        private System.Windows.Forms.Label labelQtd;
        private System.Windows.Forms.TextBox textBoxQtd;
        private System.Windows.Forms.BindingSource bindingSourceEquipamentos;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}