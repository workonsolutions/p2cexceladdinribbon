﻿namespace ExcelAddIn
{
    partial class PesquisaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonLimparDados = new System.Windows.Forms.Button();
            this.buttonPesquisar = new System.Windows.Forms.Button();
            this.dateTimePickerDtFim = new System.Windows.Forms.DateTimePicker();
            this.radioButtonCreditexNao = new System.Windows.Forms.RadioButton();
            this.radioButtonCreditexSim = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxIdProposta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNomeCliente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxVendedor = new System.Windows.Forms.ComboBox();
            this.bindingSourceVendedor = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerDtInicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNifCliente = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxTipoNegocio = new System.Windows.Forms.ComboBox();
            this.bindingSourceTipoNegocio = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxEstadoProposta = new System.Windows.Forms.ComboBox();
            this.bindingSourceEstadoProp = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewResultados = new System.Windows.Forms.DataGridView();
            this.buttonFechar = new System.Windows.Forms.Button();
            this.bindingSourcePropostas = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTipoNegocio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEstadoProp)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResultados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePropostas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonLimparDados);
            this.groupBox1.Controls.Add(this.buttonPesquisar);
            this.groupBox1.Controls.Add(this.dateTimePickerDtFim);
            this.groupBox1.Controls.Add(this.radioButtonCreditexNao);
            this.groupBox1.Controls.Add(this.radioButtonCreditexSim);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxIdProposta);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxNomeCliente);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBoxVendedor);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateTimePickerDtInicio);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxNifCliente);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxTipoNegocio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxEstadoProposta);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(742, 187);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisa de Propostas";
            // 
            // buttonLimparDados
            // 
            this.buttonLimparDados.Location = new System.Drawing.Point(570, 155);
            this.buttonLimparDados.Name = "buttonLimparDados";
            this.buttonLimparDados.Size = new System.Drawing.Size(86, 23);
            this.buttonLimparDados.TabIndex = 12;
            this.buttonLimparDados.Text = "Limpar Dados";
            this.buttonLimparDados.UseVisualStyleBackColor = true;
            this.buttonLimparDados.Click += new System.EventHandler(this.buttonLimparDados_Click);
            // 
            // buttonPesquisar
            // 
            this.buttonPesquisar.Location = new System.Drawing.Point(662, 155);
            this.buttonPesquisar.Name = "buttonPesquisar";
            this.buttonPesquisar.Size = new System.Drawing.Size(75, 23);
            this.buttonPesquisar.TabIndex = 11;
            this.buttonPesquisar.Text = "Pesquisar";
            this.buttonPesquisar.UseVisualStyleBackColor = true;
            this.buttonPesquisar.Click += new System.EventHandler(this.buttonPesquisar_Click);
            // 
            // dateTimePickerDtFim
            // 
            this.dateTimePickerDtFim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDtFim.Location = new System.Drawing.Point(474, 131);
            this.dateTimePickerDtFim.Name = "dateTimePickerDtFim";
            this.dateTimePickerDtFim.Size = new System.Drawing.Size(93, 20);
            this.dateTimePickerDtFim.TabIndex = 7;
            // 
            // radioButtonCreditexNao
            // 
            this.radioButtonCreditexNao.AutoSize = true;
            this.radioButtonCreditexNao.Location = new System.Drawing.Point(412, 105);
            this.radioButtonCreditexNao.Name = "radioButtonCreditexNao";
            this.radioButtonCreditexNao.Size = new System.Drawing.Size(45, 17);
            this.radioButtonCreditexNao.TabIndex = 9;
            this.radioButtonCreditexNao.TabStop = true;
            this.radioButtonCreditexNao.Text = "Não";
            this.radioButtonCreditexNao.UseVisualStyleBackColor = true;
            // 
            // radioButtonCreditexSim
            // 
            this.radioButtonCreditexSim.AutoSize = true;
            this.radioButtonCreditexSim.Location = new System.Drawing.Point(367, 105);
            this.radioButtonCreditexSim.Name = "radioButtonCreditexSim";
            this.radioButtonCreditexSim.Size = new System.Drawing.Size(42, 17);
            this.radioButtonCreditexSim.TabIndex = 8;
            this.radioButtonCreditexSim.TabStop = true;
            this.radioButtonCreditexSim.Text = "Sim";
            this.radioButtonCreditexSim.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(316, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Creditex";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(86, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "# Proposta";
            // 
            // textBoxIdProposta
            // 
            this.textBoxIdProposta.Location = new System.Drawing.Point(149, 31);
            this.textBoxIdProposta.Name = "textBoxIdProposta";
            this.textBoxIdProposta.Size = new System.Drawing.Size(121, 20);
            this.textBoxIdProposta.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(291, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Nome Cliente";
            // 
            // textBoxNomeCliente
            // 
            this.textBoxNomeCliente.Location = new System.Drawing.Point(365, 67);
            this.textBoxNomeCliente.Name = "textBoxNomeCliente";
            this.textBoxNomeCliente.Size = new System.Drawing.Size(121, 20);
            this.textBoxNomeCliente.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(90, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Vendedor";
            // 
            // comboBoxVendedor
            // 
            this.comboBoxVendedor.DataSource = this.bindingSourceVendedor;
            this.comboBoxVendedor.FormattingEnabled = true;
            this.comboBoxVendedor.Location = new System.Drawing.Point(149, 131);
            this.comboBoxVendedor.Name = "comboBoxVendedor";
            this.comboBoxVendedor.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVendedor.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(455, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "a";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "De";
            // 
            // dateTimePickerDtInicio
            // 
            this.dateTimePickerDtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDtInicio.Location = new System.Drawing.Point(355, 131);
            this.dateTimePickerDtInicio.Name = "dateTimePickerDtInicio";
            this.dateTimePickerDtInicio.Size = new System.Drawing.Size(93, 20);
            this.dateTimePickerDtInicio.TabIndex = 6;
            this.dateTimePickerDtInicio.Value = new System.DateTime(2014, 1, 1, 15, 20, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(503, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "NIF Cliente";
            // 
            // textBoxNifCliente
            // 
            this.textBoxNifCliente.Location = new System.Drawing.Point(567, 70);
            this.textBoxNifCliente.Name = "textBoxNifCliente";
            this.textBoxNifCliente.Size = new System.Drawing.Size(121, 20);
            this.textBoxNifCliente.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tipo de Negócio";
            // 
            // comboBoxTipoNegocio
            // 
            this.comboBoxTipoNegocio.DataSource = this.bindingSourceTipoNegocio;
            this.comboBoxTipoNegocio.FormattingEnabled = true;
            this.comboBoxTipoNegocio.Location = new System.Drawing.Point(149, 97);
            this.comboBoxTipoNegocio.Name = "comboBoxTipoNegocio";
            this.comboBoxTipoNegocio.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTipoNegocio.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Estado da Proposta";
            // 
            // comboBoxEstadoProposta
            // 
            this.comboBoxEstadoProposta.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxEstadoProposta.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEstadoProposta.DataSource = this.bindingSourceEstadoProp;
            this.comboBoxEstadoProposta.FormattingEnabled = true;
            this.comboBoxEstadoProposta.Location = new System.Drawing.Point(149, 68);
            this.comboBoxEstadoProposta.Name = "comboBoxEstadoProposta";
            this.comboBoxEstadoProposta.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEstadoProposta.TabIndex = 2;
            this.comboBoxEstadoProposta.SelectedIndexChanged += new System.EventHandler(this.comboBoxEstadoProposta_SelectedIndexChanged);
            // 
            // bindingSourceEstadoProp
            // 
            this.bindingSourceEstadoProp.DataMember = global::P2CExcelAddInRibbon.Properties.Settings.Default.SessionKeyRegister;
            this.bindingSourceEstadoProp.CurrentChanged += new System.EventHandler(this.bindingSourceEstadoProp_CurrentChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Controls.Add(this.dataGridViewResultados);
            this.groupBox2.Location = new System.Drawing.Point(12, 205);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(742, 215);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resultados";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(731, 170);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // dataGridViewResultados
            // 
            this.dataGridViewResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResultados.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewResultados.Name = "dataGridViewResultados";
            this.dataGridViewResultados.Size = new System.Drawing.Size(528, 150);
            this.dataGridViewResultados.TabIndex = 0;
            // 
            // buttonFechar
            // 
            this.buttonFechar.Location = new System.Drawing.Point(674, 430);
            this.buttonFechar.Name = "buttonFechar";
            this.buttonFechar.Size = new System.Drawing.Size(75, 23);
            this.buttonFechar.TabIndex = 13;
            this.buttonFechar.Text = "Fechar";
            this.buttonFechar.UseVisualStyleBackColor = true;
            this.buttonFechar.Click += new System.EventHandler(this.buttonFechar_Click);
            // 
            // PesquisaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 465);
            this.Controls.Add(this.buttonFechar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PesquisaForm";
            this.ShowIcon = false;
            this.Text = "Pesquisa";
            this.Load += new System.EventHandler(this.PesquisaForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PesquisaForm_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTipoNegocio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEstadoProp)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResultados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePropostas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonLimparDados;
        private System.Windows.Forms.Button buttonPesquisar;
        private System.Windows.Forms.DateTimePicker dateTimePickerDtFim;
        private System.Windows.Forms.RadioButton radioButtonCreditexNao;
        private System.Windows.Forms.RadioButton radioButtonCreditexSim;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxIdProposta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNomeCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxVendedor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerDtInicio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxTipoNegocio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxEstadoProposta;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewResultados;
        private System.Windows.Forms.Button buttonFechar;
        private System.Windows.Forms.BindingSource bindingSourceEstadoProp;
        private System.Windows.Forms.BindingSource bindingSourceTipoNegocio;
        private System.Windows.Forms.BindingSource bindingSourceVendedor;
        private System.Windows.Forms.BindingSource bindingSourcePropostas;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNifCliente;
    }
}