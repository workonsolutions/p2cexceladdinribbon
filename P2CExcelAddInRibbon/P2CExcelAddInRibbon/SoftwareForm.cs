﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;

namespace ExcelAddIn
{
    public partial class SoftwareForm : Form
    {

        public SoftwareForm()
        {
            InitializeComponent();
        }


        private void InsertDataInTable()
        {
            try
            {
                var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                long insertLine = sheet.Cells.Find("Totais").Row;
                string fristLine = sheet.Range["A3"].Value;
                long row = 0;

                if (fristLine != null && fristLine != string.Empty)
                {
                    Range Line = sheet.Range["A" + insertLine + ":M" + insertLine + ""];
                    Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    row = insertLine;
                }
                else
                    row = 3;

                string is3Party = "Não";
                string fristAno = "Não";

                if (checkBox3Party.Checked)
                    is3Party = "Sim";

                if (checkBox1ano.Checked)
                    fristAno = "Sim";

                double precoUnit = Convert.ToDouble((textBoxPrecoLista.Text != string.Empty) ? textBoxPrecoLista.Text : "0");
                if (textBoxDesconto.Text != string.Empty && Convert.ToDouble(textBoxDesconto.Text) > 0)
                {
                    precoUnit = precoUnit - (precoUnit * (0.01 * Convert.ToDouble(textBoxDesconto.Text)));
                }

                double manuUnit = Convert.ToDouble((textBoxManutAno.Text != string.Empty) ? textBoxManutAno.Text : "0");
                if (textBoxDescontoManutencao.Text != string.Empty && Convert.ToDouble(textBoxDescontoManutencao.Text) > 0)
                {
                    manuUnit = manuUnit - (manuUnit * (0.01 * Convert.ToDouble(textBoxDescontoManutencao.Text)));
                }

                sheet.Range["O" + row].Value = "=DadosClienteGerais!J17";
                sheet.Range["P" + row].Value = "=DadosClienteGerais!P18";
                sheet.Range["A" + row].Value = textBoxDesignacao.Text;
                sheet.Range["B" + row].Value = Convert.ToDouble((textBoxPrecoLista.Text == string.Empty) ? "0" : textBoxPrecoLista.Text);
                //sheet.Range["C" + row].Value = Convert.ToDouble((textBoxDesconto.Text == string.Empty) ? "0" : textBoxDesconto.Text);
                sheet.Range["D" + row].Value = precoUnit;
                sheet.Range["E" + row].Value = Convert.ToDouble((textBoxQuantidade.Text == string.Empty) ? "0" : textBoxQuantidade.Text);
                sheet.Range["G" + row].Value = Convert.ToDouble((textBoxManutAno.Text == string.Empty) ? "0" : textBoxManutAno.Text);
                sheet.Range["I" + row].Value = manuUnit;


                sheet.Range["J" + row].Value = fristAno;
                sheet.Range["K" + row].Value = is3Party;
                //sheet.Range["L" + row].Value = Convert.ToDouble((textBoxDescontoManutencao.Text == string.Empty) ? "0" : textBoxDescontoManutencao.Text);
                

                long insertLineFix = sheet.Cells.Find("Totais").Row;
                Range LineFix = sheet.Range["A" + insertLineFix + ":Z" + insertLineFix + ""];
                LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }
            //s.Manutencao / 12 * (periodoContrato - (s.ManutPrimAnoIncluido.GetValueOrDefault(false) ? 12 : 0)) * s.Quantidade * (1 - s.DescontoManutencao.GetValueOrDefault(0) / 100);
            this.Close();
        }


        private bool ValidateControls()
        {
            bool isValidated = true;

            int quantidade = 0;
            bool validQtd = int.TryParse(textBoxQuantidade.Text, out quantidade);

            if (!validQtd || quantidade<0)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxQuantidade, "Quantidade Inválida");
            }

            double preco = 0.0D;
            bool validPreco = double.TryParse(textBoxPrecoLista.Text, out preco);

            if (!validPreco || preco<0)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxPrecoLista, "Preço Lista Inválido");
            }

            double desconto = 0.0D;
            bool validDesc = double.TryParse(textBoxDesconto.Text, out desconto);

            if ((!validDesc || desconto<0) && textBoxDesconto.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDesconto, "Desconto Inválido");
            }

            double manutencaoAno = 0.0D;
            bool validManuAno = double.TryParse(textBoxManutAno.Text, out manutencaoAno);

            if ((!validManuAno || manutencaoAno<0) && textBoxDesconto.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxManutAno, "Manutenção ano Inválida");
            }

            double descManutencao = 0.0D;
            bool validDescMan = double.TryParse(textBoxDescontoManutencao.Text, out descManutencao);

            if ((!validDescMan || descManutencao<0) && textBoxDescontoManutencao.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDescontoManutencao, "Manutenção ano Inválida");
            }

            

            return isValidated;
        }
        
        private void buttonInserir_Click(object sender, EventArgs e)
        {

            if (ValidateControls())
                InsertDataInTable();

        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SoftwareForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonInserir_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                button1_Click(sender, e);
            }
        }

        
    }
}
