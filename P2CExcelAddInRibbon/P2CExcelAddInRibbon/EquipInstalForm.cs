﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using SpiritucSI.Xerox.Workflow.Business;

namespace ExcelAddIn
{
    public partial class EquipInstalForm : Form
    {

        public IEnumerable<object> DataSource_Equipamentos
        {
            get
            {
                return (IEnumerable<object>)bindingSourceEquipamentos.DataSource;
            }
            set
            {
                bindingSourceEquipamentos.DataSource = value;
                comboBoxCodigo.DataSource = bindingSourceEquipamentos;
                comboBoxCodigo.DisplayMember = "product";
            }
        }

        public List<ExcelServico> listServicos { get; set; }

        public EquipInstalForm()
        {
            InitializeComponent();
        }


        private bool ValidateControls()
        {
            bool isValidated = true;

            double desconto = 0.0D;
            bool validDesc = double.TryParse(textBoxDesconto.Text, out desconto);

            if ((!validDesc || desconto<0) && textBoxDesconto.Text != string.Empty)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxDesconto, "Desconto Inválido");
            }

            int quantidade = 0;
            bool validQtd = int.TryParse(textBoxQtd.Text, out quantidade);

            if (!validQtd || quantidade<0)
            {
                isValidated = false;
                errorProvider1.SetError(textBoxQtd, "Quantidade Inválida");
            }

            if (radioButton3party.Checked)
            {
                double precoLista = 0.0D;
                bool validLista = double.TryParse(textBoxPreco.Text, out precoLista);

                if ((!validLista || precoLista<0) && textBoxPreco.Text != string.Empty)
                {
                    isValidated = false;
                    errorProvider1.SetError(textBoxPreco, "Preço Lista Inválido");
                }

                double margem = 0.0D;
                bool validMargem = double.TryParse(textBoxMg.Text, out margem);

                if ((!validMargem || margem<0) && textBoxMg.Text != string.Empty)
                {
                    isValidated = false;
                    errorProvider1.SetError(textBoxMg, "Margem Inválida");
                }

                if (textBoxModelo.Text == string.Empty)
                {
                    isValidated = false;
                    errorProvider1.SetError(textBoxModelo, "Modelo Inválido");
                }

               

            }
           

            return isValidated;

        }


        private void InsertDataInTable()
        {
            try
            {

                string codigoEquip = comboBoxCodigo.SelectedText;

                ExcelEquipamento equipamentoSel = (ExcelEquipamento)comboBoxCodigo.SelectedItem;

                if (equipamentoSel != null)
                {
                    var sheet = (Worksheet)ExcelAddIn.Globals.ThisAddIn.Application.ActiveSheet;
                    long insertLine = sheet.Cells.Find("Totais Instalar").Row;
                    string fristLine = sheet.Range["A3"].Value;
                    long row = 0;

                    if (fristLine != null && fristLine != string.Empty)
                    {
                        //Range Line = (Range)sheet.Rows[insertLine];
                        Range Line = sheet.Range["A" + insertLine + ":AL" + insertLine + ""];
                        Line.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        row = insertLine;
                    }
                    else
                        row = 3;


                    //listEquipamentos.Where(o => o.product.Equals(codigoEquip)).FirstOrDefault();

                    string tipoEquip = string.Empty;

                    if (radioButton3party.Checked)
                        tipoEquip = radioButton3party.Text;
                    if (radioButtonCliente.Checked)
                        tipoEquip = radioButtonCliente.Text;
                    if (radioButtonXerox.Checked)
                        tipoEquip = radioButtonXerox.Text;

                    double precoUnit = equipamentoSel.listPrice;
                    if (textBoxDesconto.Text != string.Empty && Convert.ToDouble(textBoxDesconto.Text) > 0)
                    {
                        precoUnit = equipamentoSel.listPrice - (equipamentoSel.listPrice * (0.01 * Convert.ToDouble(textBoxDesconto.Text)));
                    }

                    if (!radioButton3party.Checked)
                    {
                        
                        sheet.Range["A" + row].Value = tipoEquip;
                        sheet.Range["B" + row].Value = equipamentoSel.product;
                        sheet.Range["C" + row].Value = equipamentoSel.modelCode;
                        sheet.Range["D" + row].Value = Convert.ToDouble((textBoxQtd.Text == string.Empty) ? "0" : textBoxQtd.Text);
                        sheet.Range["E" + row].Value = equipamentoSel.listPrice; //string.Format("{0:#,##0.00}", equipamentoSel.listPrice);  //equipamentoSel.listPrice.ToString();
                        //sheet.Range["F" + row].Value = //Convert.ToDouble((textBoxDesconto.Text == string.Empty) ? "0" : textBoxDesconto.Text); ;
                        sheet.Range["G" + row].Value = Convert.ToDouble("0");
                        sheet.Range["H" + row].Value = "Não";
                        sheet.Range["I" + row].Value = precoUnit;
                        sheet.Range["K" + row].Value = equipamentoSel.idPart;
                        sheet.Range["L" + row].Value = "0,00";
                        sheet.Range["M" + row].Value = equipamentoSel.description;
                        sheet.Range["N" + row].Value = string.Empty;
                        //sheet.Range["H" + row].Formula = "=E" + row + "-(E" + row + "*(0.01*F" + row + "))";
                        //sheet.Range["I" + row].Formula = "=H"+row+"*D"+row;
                    }
                    else
                    {
                        sheet.Range["A" + row].Value = tipoEquip;
                        sheet.Range["B" + row].Value = string.Empty;
                        sheet.Range["C" + row].Value = textBoxModelo.Text;
                        sheet.Range["D" + row].Value = textBoxQtd.Text;
                        sheet.Range["E" + row].Value = textBoxPreco.Text;
                        //sheet.Range["F" + row].Value = textBoxDesconto.Text;
                        sheet.Range["G" + row].Value = textBoxMg.Text;
                        sheet.Range["H" + row].Value = "Sim";
                        sheet.Range["I" + row].Value = precoUnit;
                        sheet.Range["K" + row].Value = string.Empty;
                        sheet.Range["L" + row].Value = "0,00";
                        sheet.Range["M" + row].Value = textBoxDesc.Text;
                        sheet.Range["N" + row].Value = string.Empty;
                    }

                    var rangeSel = sheet.get_Range("A3", "A3");
                    rangeSel.Select();

                    long insertLineFix = sheet.Cells.Find("Totais Instalar").Row;
                    Range LineFix = sheet.Range["A" + insertLineFix + ":AL" + insertLineFix + ""];
                    LineFix.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

                    this.Close();
                }
                else
                {
                    HandleException("Equipamento não existe");

                }

            }
            catch (Exception ex)
            {
                HandleException(ex.Message);
                //throw; 
            }

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {

            if (ValidateControls())
                InsertDataInTable();

        }

        private void radioButtonXerox_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxCodigo.Enabled = true;
            textBoxQtd.Enabled = true;
            textBoxDesc.Enabled = false;
            textBoxDesconto.Enabled = true;
            textBoxMg.Enabled = false;
            textBoxModelo.Enabled = false;
            textBoxPreco.Enabled = false;
            

        }

        private void radioButtonCliente_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxCodigo.Enabled = true;
            textBoxQtd.Enabled = true;
            textBoxDesc.Enabled = false;
            textBoxDesconto.Enabled = true;
            textBoxMg.Enabled = false;
            textBoxModelo.Enabled = false;
            textBoxPreco.Enabled = false;
        }

        private void radioButton3party_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxCodigo.Enabled = false;
            textBoxQtd.Enabled = true;
            textBoxDesc.Enabled = true;
            textBoxDesconto.Enabled = true;
            textBoxMg.Enabled = true;
            textBoxModelo.Enabled = true;
            textBoxPreco.Enabled = true;
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void HandleException(string message)
        {
            MensagensForm formMsg = new MensagensForm();
            formMsg.imageDisplay = P2CExcelAddInRibbon.Properties.Resources.warning_48;
            formMsg.msgToShow = message;
            formMsg.ShowDialog();
        }

        private void EquipInstalForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonOk_Click(sender, e);
            }

            if (e.KeyCode == Keys.Escape)
            {
                buttonCancelar_Click(sender, e);
            }
        }

        private void EquipInstalForm_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void comboBoxCodigo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExcelEquipamento equipamentoSel = (ExcelEquipamento)comboBoxCodigo.SelectedItem;

            if (equipamentoSel != null)
            {
                textBoxPreco.Text = equipamentoSel.listPrice.ToString();
            }
        }

        
    }
}
