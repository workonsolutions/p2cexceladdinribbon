﻿namespace ExcelAddIn
{
    partial class EquipRemoveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxObservacoes = new System.Windows.Forms.TextBox();
            this.checkBox3Party = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerDtTermo = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerDtFim = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerDtInicio = new System.Windows.Forms.DateTimePicker();
            this.textBoxPreco = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxModelo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNumSerie = new System.Windows.Forms.TextBox();
            this.labelCodigo = new System.Windows.Forms.Label();
            this.radioButtonTopUp = new System.Windows.Forms.RadioButton();
            this.radioButtonRetoma = new System.Windows.Forms.RadioButton();
            this.buttonInserir = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxObservacoes);
            this.groupBox1.Controls.Add(this.checkBox3Party);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dateTimePickerDtTermo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dateTimePickerDtFim);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateTimePickerDtInicio);
            this.groupBox1.Controls.Add(this.textBoxPreco);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxModelo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxNumSerie);
            this.groupBox1.Controls.Add(this.labelCodigo);
            this.groupBox1.Controls.Add(this.radioButtonTopUp);
            this.groupBox1.Controls.Add(this.radioButtonRetoma);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(569, 263);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Equipamento a Remover";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(466, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "€";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(41, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Observações:";
            // 
            // textBoxObservacoes
            // 
            this.textBoxObservacoes.Location = new System.Drawing.Point(122, 206);
            this.textBoxObservacoes.Multiline = true;
            this.textBoxObservacoes.Name = "textBoxObservacoes";
            this.textBoxObservacoes.Size = new System.Drawing.Size(202, 41);
            this.textBoxObservacoes.TabIndex = 30;
            // 
            // checkBox3Party
            // 
            this.checkBox3Party.AutoSize = true;
            this.checkBox3Party.Location = new System.Drawing.Point(411, 103);
            this.checkBox3Party.Name = "checkBox3Party";
            this.checkBox3Party.Size = new System.Drawing.Size(68, 17);
            this.checkBox3Party.TabIndex = 29;
            this.checkBox3Party.Text = "3rd Party";
            this.checkBox3Party.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Data Termo contrato:";
            // 
            // dateTimePickerDtTermo
            // 
            this.dateTimePickerDtTermo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDtTermo.Location = new System.Drawing.Point(122, 161);
            this.dateTimePickerDtTermo.Name = "dateTimePickerDtTermo";
            this.dateTimePickerDtTermo.Size = new System.Drawing.Size(93, 20);
            this.dateTimePickerDtTermo.TabIndex = 27;
            this.dateTimePickerDtTermo.Value = new System.DateTime(2013, 1, 1, 15, 20, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Data Fim contrato:";
            // 
            // dateTimePickerDtFim
            // 
            this.dateTimePickerDtFim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDtFim.Location = new System.Drawing.Point(121, 134);
            this.dateTimePickerDtFim.Name = "dateTimePickerDtFim";
            this.dateTimePickerDtFim.Size = new System.Drawing.Size(93, 20);
            this.dateTimePickerDtFim.TabIndex = 25;
            this.dateTimePickerDtFim.Value = new System.DateTime(2013, 1, 1, 15, 20, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Data Ínicio contrato:";
            // 
            // dateTimePickerDtInicio
            // 
            this.dateTimePickerDtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDtInicio.Location = new System.Drawing.Point(121, 101);
            this.dateTimePickerDtInicio.Name = "dateTimePickerDtInicio";
            this.dateTimePickerDtInicio.Size = new System.Drawing.Size(93, 20);
            this.dateTimePickerDtInicio.TabIndex = 23;
            this.dateTimePickerDtInicio.Value = new System.DateTime(2013, 1, 1, 15, 20, 0, 0);
            // 
            // textBoxPreco
            // 
            this.textBoxPreco.Location = new System.Drawing.Point(328, 65);
            this.textBoxPreco.Name = "textBoxPreco";
            this.textBoxPreco.Size = new System.Drawing.Size(123, 20);
            this.textBoxPreco.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(285, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Preço:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Tipo de Retirada:";
            // 
            // textBoxModelo
            // 
            this.textBoxModelo.Location = new System.Drawing.Point(328, 32);
            this.textBoxModelo.Name = "textBoxModelo";
            this.textBoxModelo.Size = new System.Drawing.Size(123, 20);
            this.textBoxModelo.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(279, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Modelo:";
            // 
            // textBoxNumSerie
            // 
            this.textBoxNumSerie.Location = new System.Drawing.Point(120, 32);
            this.textBoxNumSerie.Name = "textBoxNumSerie";
            this.textBoxNumSerie.Size = new System.Drawing.Size(123, 20);
            this.textBoxNumSerie.TabIndex = 17;
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Location = new System.Drawing.Point(63, 36);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(44, 13);
            this.labelCodigo.TabIndex = 16;
            this.labelCodigo.Text = "# Série:";
            // 
            // radioButtonTopUp
            // 
            this.radioButtonTopUp.AutoSize = true;
            this.radioButtonTopUp.Location = new System.Drawing.Point(187, 65);
            this.radioButtonTopUp.Name = "radioButtonTopUp";
            this.radioButtonTopUp.Size = new System.Drawing.Size(58, 17);
            this.radioButtonTopUp.TabIndex = 1;
            this.radioButtonTopUp.Text = "TopUp";
            this.radioButtonTopUp.UseVisualStyleBackColor = true;
            // 
            // radioButtonRetoma
            // 
            this.radioButtonRetoma.AutoSize = true;
            this.radioButtonRetoma.Checked = true;
            this.radioButtonRetoma.Location = new System.Drawing.Point(121, 65);
            this.radioButtonRetoma.Name = "radioButtonRetoma";
            this.radioButtonRetoma.Size = new System.Drawing.Size(62, 17);
            this.radioButtonRetoma.TabIndex = 0;
            this.radioButtonRetoma.TabStop = true;
            this.radioButtonRetoma.Text = "Retoma";
            this.radioButtonRetoma.UseVisualStyleBackColor = true;
            // 
            // buttonInserir
            // 
            this.buttonInserir.Location = new System.Drawing.Point(464, 285);
            this.buttonInserir.Name = "buttonInserir";
            this.buttonInserir.Size = new System.Drawing.Size(118, 23);
            this.buttonInserir.TabIndex = 2;
            this.buttonInserir.Text = "Inserir Equipamento";
            this.buttonInserir.UseVisualStyleBackColor = true;
            this.buttonInserir.Click += new System.EventHandler(this.buttonInserir_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(383, 285);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(220, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(170, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "* De acordo com mapa de capitais";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(220, 136);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "* De acordo com mapa de capitais";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(220, 165);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(190, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "* Último dia do contrato a descontinuar";
            // 
            // EquipRemoveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 315);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonInserir);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EquipRemoveForm";
            this.Text = "Adicionar Equipamento a Remover";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonTopUp;
        private System.Windows.Forms.RadioButton radioButtonRetoma;
        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.Button buttonInserir;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxModelo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNumSerie;
        private System.Windows.Forms.TextBox textBoxPreco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxObservacoes;
        private System.Windows.Forms.CheckBox checkBox3Party;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerDtTermo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePickerDtFim;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerDtInicio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}