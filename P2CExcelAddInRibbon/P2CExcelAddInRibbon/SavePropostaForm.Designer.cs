﻿namespace ExcelAddIn
{
    partial class SavePropostaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAtualizarDraft = new System.Windows.Forms.Button();
            this.labelText = new System.Windows.Forms.Label();
            this.buttonAvancaAprovacao = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAtualizarDraft
            // 
            this.buttonAtualizarDraft.Location = new System.Drawing.Point(19, 55);
            this.buttonAtualizarDraft.Name = "buttonAtualizarDraft";
            this.buttonAtualizarDraft.Size = new System.Drawing.Size(111, 38);
            this.buttonAtualizarDraft.TabIndex = 0;
            this.buttonAtualizarDraft.Text = "Actualizar Proposta";
            this.buttonAtualizarDraft.UseVisualStyleBackColor = true;
            this.buttonAtualizarDraft.Click += new System.EventHandler(this.buttonAtualizarDraft_Click);
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(69, 22);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(233, 13);
            this.labelText.TabIndex = 1;
            this.labelText.Text = "Seleccione a acção a realizar para a proprosta: ";
            // 
            // buttonAvancaAprovacao
            // 
            this.buttonAvancaAprovacao.Location = new System.Drawing.Point(158, 55);
            this.buttonAvancaAprovacao.Name = "buttonAvancaAprovacao";
            this.buttonAvancaAprovacao.Size = new System.Drawing.Size(111, 38);
            this.buttonAvancaAprovacao.TabIndex = 2;
            this.buttonAvancaAprovacao.Text = "Avançar para Aprovação";
            this.buttonAvancaAprovacao.UseVisualStyleBackColor = true;
            this.buttonAvancaAprovacao.Click += new System.EventHandler(this.buttonAvancaAprovacao_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(282, 55);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(111, 38);
            this.buttonCancelar.TabIndex = 3;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // SavePropostaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 113);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonAvancaAprovacao);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.buttonAtualizarDraft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SavePropostaForm";
            this.Text = "Enviar Proposta";
            this.Load += new System.EventHandler(this.SavePropostaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAtualizarDraft;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Button buttonAvancaAprovacao;
        private System.Windows.Forms.Button buttonCancelar;
    }
}