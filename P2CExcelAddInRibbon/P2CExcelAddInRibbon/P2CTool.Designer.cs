﻿using P2CExcelAddInRibbon.Properties;

namespace ExcelAddIn
{
    partial class P2CTool : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public P2CTool()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(P2CTool));
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.C_grp_Connection = this.Factory.CreateRibbonGroup();
            this.pnlProposta = this.Factory.CreateRibbonGroup();
            this.pnlActualizacao = this.Factory.CreateRibbonGroup();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.labelDMData = this.Factory.CreateRibbonLabel();
            this.groupEquipServInstalar = this.Factory.CreateRibbonGroup();
            this.groupRecursosHumanos = this.Factory.CreateRibbonGroup();
            this.groupSoftware = this.Factory.CreateRibbonGroup();
            this.groupOutrosServ = this.Factory.CreateRibbonGroup();
            this.groupDadosResumo = this.Factory.CreateRibbonGroup();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.box5 = this.Factory.CreateRibbonBox();
            this.label2 = this.Factory.CreateRibbonLabel();
            this.Btn_Login = this.Factory.CreateRibbonButton();
            this.buttonLoggedIn = this.Factory.CreateRibbonButton();
            this.buttonCriarProposta = this.Factory.CreateRibbonButton();
            this.SearchPropost = this.Factory.CreateRibbonButton();
            this.SaveProposta = this.Factory.CreateRibbonButton();
            this.buttonValidarDadosProposta = this.Factory.CreateRibbonButton();
            this.buttonActualizar = this.Factory.CreateRibbonButton();
            this.buttonAdicionarEquip = this.Factory.CreateRibbonButton();
            this.buttonAdicionarServico = this.Factory.CreateRibbonButton();
            this.buttonAdicionarEquipRem = this.Factory.CreateRibbonButton();
            this.buttonEliminarEquip = this.Factory.CreateRibbonButton();
            this.buttonEliminarEquipRemover = this.Factory.CreateRibbonButton();
            this.buttonAdicionarRH = this.Factory.CreateRibbonButton();
            this.buttonEliminarRH = this.Factory.CreateRibbonButton();
            this.buttonAddSoft = this.Factory.CreateRibbonButton();
            this.buttonDelSoft = this.Factory.CreateRibbonButton();
            this.buttonAddOutrosServ = this.Factory.CreateRibbonButton();
            this.buttonDelOutroServ = this.Factory.CreateRibbonButton();
            this.box3 = this.Factory.CreateRibbonBox();
            this.editBoxTipoContrato = this.Factory.CreateRibbonEditBox();
            this.editBoxPeriocidade = this.Factory.CreateRibbonEditBox();
            this.editBoxFactor = this.Factory.CreateRibbonEditBox();
            this.box2 = this.Factory.CreateRibbonBox();
            this.editBoxPrazo = this.Factory.CreateRibbonEditBox();
            this.editBoxTotalEquipInst = this.Factory.CreateRibbonEditBox();
            this.editBoxTotServico = this.Factory.CreateRibbonEditBox();
            this.box1 = this.Factory.CreateRibbonBox();
            this.editBoxTotalRH = this.Factory.CreateRibbonEditBox();
            this.editBoxTotalSoft = this.Factory.CreateRibbonEditBox();
            this.editBoxOutrosServ = this.Factory.CreateRibbonEditBox();
            this.box4 = this.Factory.CreateRibbonBox();
            this.editBoxTotalOrs = this.Factory.CreateRibbonEditBox();
            this.editBoxTotalCont = this.Factory.CreateRibbonEditBox();
            this.editBoxTotalRenda = this.Factory.CreateRibbonEditBox();
            this.SearchProposta = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.C_grp_Connection.SuspendLayout();
            this.pnlProposta.SuspendLayout();
            this.pnlActualizacao.SuspendLayout();
            this.groupEquipServInstalar.SuspendLayout();
            this.groupRecursosHumanos.SuspendLayout();
            this.groupSoftware.SuspendLayout();
            this.groupOutrosServ.SuspendLayout();
            this.groupDadosResumo.SuspendLayout();
            this.box5.SuspendLayout();
            this.box3.SuspendLayout();
            this.box2.SuspendLayout();
            this.box1.SuspendLayout();
            this.box4.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.group1);
            this.tab1.Groups.Add(this.C_grp_Connection);
            this.tab1.Groups.Add(this.pnlProposta);
            this.tab1.Groups.Add(this.pnlActualizacao);
            this.tab1.Groups.Add(this.groupEquipServInstalar);
            this.tab1.Groups.Add(this.groupRecursosHumanos);
            this.tab1.Groups.Add(this.groupSoftware);
            this.tab1.Groups.Add(this.groupOutrosServ);
            this.tab1.Groups.Add(this.groupDadosResumo);
            resources.ApplyResources(this.tab1, "tab1");
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.box5);
            this.group1.Name = "group1";
            // 
            // C_grp_Connection
            // 
            this.C_grp_Connection.Items.Add(this.Btn_Login);
            this.C_grp_Connection.Items.Add(this.buttonLoggedIn);
            this.C_grp_Connection.Items.Add(this.buttonCriarProposta);
            resources.ApplyResources(this.C_grp_Connection, "C_grp_Connection");
            this.C_grp_Connection.Name = "C_grp_Connection";
            // 
            // pnlProposta
            // 
            this.pnlProposta.Items.Add(this.SearchPropost);
            this.pnlProposta.Items.Add(this.SaveProposta);
            this.pnlProposta.Items.Add(this.buttonValidarDadosProposta);
            resources.ApplyResources(this.pnlProposta, "pnlProposta");
            this.pnlProposta.Name = "pnlProposta";
            // 
            // pnlActualizacao
            // 
            this.pnlActualizacao.Items.Add(this.buttonActualizar);
            this.pnlActualizacao.Items.Add(this.label1);
            this.pnlActualizacao.Items.Add(this.labelDMData);
            resources.ApplyResources(this.pnlActualizacao, "pnlActualizacao");
            this.pnlActualizacao.Name = "pnlActualizacao";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // labelDMData
            // 
            resources.ApplyResources(this.labelDMData, "labelDMData");
            this.labelDMData.Name = "labelDMData";
            // 
            // groupEquipServInstalar
            // 
            this.groupEquipServInstalar.Items.Add(this.buttonAdicionarEquip);
            this.groupEquipServInstalar.Items.Add(this.buttonAdicionarServico);
            this.groupEquipServInstalar.Items.Add(this.buttonAdicionarEquipRem);
            this.groupEquipServInstalar.Items.Add(this.buttonEliminarEquip);
            this.groupEquipServInstalar.Items.Add(this.buttonEliminarEquipRemover);
            resources.ApplyResources(this.groupEquipServInstalar, "groupEquipServInstalar");
            this.groupEquipServInstalar.Name = "groupEquipServInstalar";
            // 
            // groupRecursosHumanos
            // 
            this.groupRecursosHumanos.Items.Add(this.buttonAdicionarRH);
            this.groupRecursosHumanos.Items.Add(this.buttonEliminarRH);
            resources.ApplyResources(this.groupRecursosHumanos, "groupRecursosHumanos");
            this.groupRecursosHumanos.Name = "groupRecursosHumanos";
            // 
            // groupSoftware
            // 
            this.groupSoftware.Items.Add(this.buttonAddSoft);
            this.groupSoftware.Items.Add(this.buttonDelSoft);
            resources.ApplyResources(this.groupSoftware, "groupSoftware");
            this.groupSoftware.Name = "groupSoftware";
            // 
            // groupOutrosServ
            // 
            this.groupOutrosServ.Items.Add(this.buttonAddOutrosServ);
            this.groupOutrosServ.Items.Add(this.buttonDelOutroServ);
            resources.ApplyResources(this.groupOutrosServ, "groupOutrosServ");
            this.groupOutrosServ.Name = "groupOutrosServ";
            // 
            // groupDadosResumo
            // 
            this.groupDadosResumo.Items.Add(this.box3);
            this.groupDadosResumo.Items.Add(this.box2);
            this.groupDadosResumo.Items.Add(this.box1);
            this.groupDadosResumo.Items.Add(this.box4);
            resources.ApplyResources(this.groupDadosResumo, "groupDadosResumo");
            this.groupDadosResumo.Name = "groupDadosResumo";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "LCG.png");
            // 
            // box5
            // 
            this.box5.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box5.Items.Add(this.label2);
            this.box5.Name = "box5";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // Btn_Login
            // 
            this.Btn_Login.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.Btn_Login.Image = global::P2CExcelAddInRibbon.Properties.Resources.secure_locked_32;
            resources.ApplyResources(this.Btn_Login, "Btn_Login");
            this.Btn_Login.Name = "Btn_Login";
            this.Btn_Login.ShowImage = true;
            this.Btn_Login.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Btn_Login_Click);
            // 
            // buttonLoggedIn
            // 
            this.buttonLoggedIn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            resources.ApplyResources(this.buttonLoggedIn, "buttonLoggedIn");
            this.buttonLoggedIn.Name = "buttonLoggedIn";
            this.buttonLoggedIn.ShowImage = true;
            this.buttonLoggedIn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click);
            // 
            // buttonCriarProposta
            // 
            this.buttonCriarProposta.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonCriarProposta.Image = global::P2CExcelAddInRibbon.Properties.Resources.save_validate_32;
            resources.ApplyResources(this.buttonCriarProposta, "buttonCriarProposta");
            this.buttonCriarProposta.Name = "buttonCriarProposta";
            this.buttonCriarProposta.ShowImage = true;
            this.buttonCriarProposta.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCriarProposta_Click);
            // 
            // SearchPropost
            // 
            this.SearchPropost.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.SearchPropost.Image = global::P2CExcelAddInRibbon.Properties.Resources.browse_32;
            resources.ApplyResources(this.SearchPropost, "SearchPropost");
            this.SearchPropost.Name = "SearchPropost";
            this.SearchPropost.ShowImage = true;
            this.SearchPropost.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SearchPropost_Click);
            // 
            // SaveProposta
            // 
            this.SaveProposta.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.SaveProposta.Image = global::P2CExcelAddInRibbon.Properties.Resources.upload_32;
            resources.ApplyResources(this.SaveProposta, "SaveProposta");
            this.SaveProposta.Name = "SaveProposta";
            this.SaveProposta.ShowImage = true;
            this.SaveProposta.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SaveProposta_Click);
            // 
            // buttonValidarDadosProposta
            // 
            this.buttonValidarDadosProposta.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonValidarDadosProposta.Image = global::P2CExcelAddInRibbon.Properties.Resources.list_task_32;
            resources.ApplyResources(this.buttonValidarDadosProposta, "buttonValidarDadosProposta");
            this.buttonValidarDadosProposta.Name = "buttonValidarDadosProposta";
            this.buttonValidarDadosProposta.ShowImage = true;
            this.buttonValidarDadosProposta.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonValidarDadosProposta_Click);
            // 
            // buttonActualizar
            // 
            this.buttonActualizar.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonActualizar.Image = global::P2CExcelAddInRibbon.Properties.Resources.refresh_32;
            resources.ApplyResources(this.buttonActualizar, "buttonActualizar");
            this.buttonActualizar.Name = "buttonActualizar";
            this.buttonActualizar.ShowImage = true;
            this.buttonActualizar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonActualizar_Click);
            // 
            // buttonAdicionarEquip
            // 
            this.buttonAdicionarEquip.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonAdicionarEquip.Image = global::P2CExcelAddInRibbon.Properties.Resources.expand_32;
            resources.ApplyResources(this.buttonAdicionarEquip, "buttonAdicionarEquip");
            this.buttonAdicionarEquip.Name = "buttonAdicionarEquip";
            this.buttonAdicionarEquip.ShowImage = true;
            this.buttonAdicionarEquip.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAdicionarEquip_Click);
            // 
            // buttonAdicionarServico
            // 
            this.buttonAdicionarServico.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonAdicionarServico.Image = global::P2CExcelAddInRibbon.Properties.Resources.expand_32;
            resources.ApplyResources(this.buttonAdicionarServico, "buttonAdicionarServico");
            this.buttonAdicionarServico.Name = "buttonAdicionarServico";
            this.buttonAdicionarServico.ShowImage = true;
            this.buttonAdicionarServico.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAdicionarServico_Click);
            // 
            // buttonAdicionarEquipRem
            // 
            this.buttonAdicionarEquipRem.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonAdicionarEquipRem.Image = global::P2CExcelAddInRibbon.Properties.Resources.expand_32;
            resources.ApplyResources(this.buttonAdicionarEquipRem, "buttonAdicionarEquipRem");
            this.buttonAdicionarEquipRem.Name = "buttonAdicionarEquipRem";
            this.buttonAdicionarEquipRem.ShowImage = true;
            this.buttonAdicionarEquipRem.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAdicionarEquipRem_Click);
            // 
            // buttonEliminarEquip
            // 
            this.buttonEliminarEquip.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonEliminarEquip.Image = global::P2CExcelAddInRibbon.Properties.Resources.cancel_delete_321;
            resources.ApplyResources(this.buttonEliminarEquip, "buttonEliminarEquip");
            this.buttonEliminarEquip.Name = "buttonEliminarEquip";
            this.buttonEliminarEquip.ShowImage = true;
            this.buttonEliminarEquip.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonEliminarEquip_Click);
            // 
            // buttonEliminarEquipRemover
            // 
            this.buttonEliminarEquipRemover.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonEliminarEquipRemover.Image = global::P2CExcelAddInRibbon.Properties.Resources.cancel_delete_321;
            resources.ApplyResources(this.buttonEliminarEquipRemover, "buttonEliminarEquipRemover");
            this.buttonEliminarEquipRemover.Name = "buttonEliminarEquipRemover";
            this.buttonEliminarEquipRemover.ShowImage = true;
            this.buttonEliminarEquipRemover.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonEliminarEquipRemover_Click);
            // 
            // buttonAdicionarRH
            // 
            this.buttonAdicionarRH.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonAdicionarRH.Image = global::P2CExcelAddInRibbon.Properties.Resources.expand_32;
            resources.ApplyResources(this.buttonAdicionarRH, "buttonAdicionarRH");
            this.buttonAdicionarRH.Name = "buttonAdicionarRH";
            this.buttonAdicionarRH.ShowImage = true;
            this.buttonAdicionarRH.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAdicionarRH_Click);
            // 
            // buttonEliminarRH
            // 
            this.buttonEliminarRH.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonEliminarRH.Image = global::P2CExcelAddInRibbon.Properties.Resources.cancel_delete_321;
            resources.ApplyResources(this.buttonEliminarRH, "buttonEliminarRH");
            this.buttonEliminarRH.Name = "buttonEliminarRH";
            this.buttonEliminarRH.ShowImage = true;
            this.buttonEliminarRH.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonEliminarRH_Click);
            // 
            // buttonAddSoft
            // 
            this.buttonAddSoft.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonAddSoft.Image = global::P2CExcelAddInRibbon.Properties.Resources.expand_32;
            resources.ApplyResources(this.buttonAddSoft, "buttonAddSoft");
            this.buttonAddSoft.Name = "buttonAddSoft";
            this.buttonAddSoft.ShowImage = true;
            this.buttonAddSoft.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAddSoft_Click);
            // 
            // buttonDelSoft
            // 
            this.buttonDelSoft.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonDelSoft.Image = global::P2CExcelAddInRibbon.Properties.Resources.cancel_delete_321;
            resources.ApplyResources(this.buttonDelSoft, "buttonDelSoft");
            this.buttonDelSoft.Name = "buttonDelSoft";
            this.buttonDelSoft.ShowImage = true;
            this.buttonDelSoft.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDelSoft_Click);
            // 
            // buttonAddOutrosServ
            // 
            this.buttonAddOutrosServ.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonAddOutrosServ.Image = global::P2CExcelAddInRibbon.Properties.Resources.expand_32;
            resources.ApplyResources(this.buttonAddOutrosServ, "buttonAddOutrosServ");
            this.buttonAddOutrosServ.Name = "buttonAddOutrosServ";
            this.buttonAddOutrosServ.ShowImage = true;
            this.buttonAddOutrosServ.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAddOutrosServ_Click);
            // 
            // buttonDelOutroServ
            // 
            this.buttonDelOutroServ.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonDelOutroServ.Image = global::P2CExcelAddInRibbon.Properties.Resources.cancel_delete_321;
            resources.ApplyResources(this.buttonDelOutroServ, "buttonDelOutroServ");
            this.buttonDelOutroServ.Name = "buttonDelOutroServ";
            this.buttonDelOutroServ.ShowImage = true;
            this.buttonDelOutroServ.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDelOutroServ_Click);
            // 
            // box3
            // 
            this.box3.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box3.Items.Add(this.editBoxTipoContrato);
            this.box3.Items.Add(this.editBoxPeriocidade);
            this.box3.Items.Add(this.editBoxFactor);
            this.box3.Name = "box3";
            // 
            // editBoxTipoContrato
            // 
            resources.ApplyResources(this.editBoxTipoContrato, "editBoxTipoContrato");
            this.editBoxTipoContrato.Name = "editBoxTipoContrato";
            // 
            // editBoxPeriocidade
            // 
            resources.ApplyResources(this.editBoxPeriocidade, "editBoxPeriocidade");
            this.editBoxPeriocidade.Name = "editBoxPeriocidade";
            // 
            // editBoxFactor
            // 
            resources.ApplyResources(this.editBoxFactor, "editBoxFactor");
            this.editBoxFactor.Name = "editBoxFactor";
            // 
            // box2
            // 
            this.box2.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box2.Items.Add(this.editBoxPrazo);
            this.box2.Items.Add(this.editBoxTotalEquipInst);
            this.box2.Items.Add(this.editBoxTotServico);
            this.box2.Name = "box2";
            // 
            // editBoxPrazo
            // 
            resources.ApplyResources(this.editBoxPrazo, "editBoxPrazo");
            this.editBoxPrazo.Name = "editBoxPrazo";
            // 
            // editBoxTotalEquipInst
            // 
            resources.ApplyResources(this.editBoxTotalEquipInst, "editBoxTotalEquipInst");
            this.editBoxTotalEquipInst.Name = "editBoxTotalEquipInst";
            // 
            // editBoxTotServico
            // 
            resources.ApplyResources(this.editBoxTotServico, "editBoxTotServico");
            this.editBoxTotServico.Name = "editBoxTotServico";
            // 
            // box1
            // 
            this.box1.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box1.Items.Add(this.editBoxTotalRH);
            this.box1.Items.Add(this.editBoxTotalSoft);
            this.box1.Items.Add(this.editBoxOutrosServ);
            this.box1.Name = "box1";
            resources.ApplyResources(this.box1, "box1");
            // 
            // editBoxTotalRH
            // 
            resources.ApplyResources(this.editBoxTotalRH, "editBoxTotalRH");
            this.editBoxTotalRH.Name = "editBoxTotalRH";
            // 
            // editBoxTotalSoft
            // 
            resources.ApplyResources(this.editBoxTotalSoft, "editBoxTotalSoft");
            this.editBoxTotalSoft.Name = "editBoxTotalSoft";
            // 
            // editBoxOutrosServ
            // 
            resources.ApplyResources(this.editBoxOutrosServ, "editBoxOutrosServ");
            this.editBoxOutrosServ.Name = "editBoxOutrosServ";
            // 
            // box4
            // 
            this.box4.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box4.Items.Add(this.editBoxTotalOrs);
            this.box4.Items.Add(this.editBoxTotalCont);
            this.box4.Items.Add(this.editBoxTotalRenda);
            this.box4.Name = "box4";
            // 
            // editBoxTotalOrs
            // 
            resources.ApplyResources(this.editBoxTotalOrs, "editBoxTotalOrs");
            this.editBoxTotalOrs.Name = "editBoxTotalOrs";
            // 
            // editBoxTotalCont
            // 
            resources.ApplyResources(this.editBoxTotalCont, "editBoxTotalCont");
            this.editBoxTotalCont.Name = "editBoxTotalCont";
            // 
            // editBoxTotalRenda
            // 
            resources.ApplyResources(this.editBoxTotalRenda, "editBoxTotalRenda");
            this.editBoxTotalRenda.Name = "editBoxTotalRenda";
            // 
            // SearchProposta
            // 
            this.SearchProposta.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.SearchProposta.Image = global::P2CExcelAddInRibbon.Properties.Resources.browse_32;
            resources.ApplyResources(this.SearchProposta, "SearchProposta");
            this.SearchProposta.Name = "SearchProposta";
            this.SearchProposta.ShowImage = true;
            // 
            // P2CTool
            // 
            this.Name = "P2CTool";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.P2CTool_Load);
            this.LoadImage += new Microsoft.Office.Tools.Ribbon.RibbonLoadImageEventHandler(this.P2CTool_LoadImage);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.C_grp_Connection.ResumeLayout(false);
            this.C_grp_Connection.PerformLayout();
            this.pnlProposta.ResumeLayout(false);
            this.pnlProposta.PerformLayout();
            this.pnlActualizacao.ResumeLayout(false);
            this.pnlActualizacao.PerformLayout();
            this.groupEquipServInstalar.ResumeLayout(false);
            this.groupEquipServInstalar.PerformLayout();
            this.groupRecursosHumanos.ResumeLayout(false);
            this.groupRecursosHumanos.PerformLayout();
            this.groupSoftware.ResumeLayout(false);
            this.groupSoftware.PerformLayout();
            this.groupOutrosServ.ResumeLayout(false);
            this.groupOutrosServ.PerformLayout();
            this.groupDadosResumo.ResumeLayout(false);
            this.groupDadosResumo.PerformLayout();
            this.box5.ResumeLayout(false);
            this.box5.PerformLayout();
            this.box3.ResumeLayout(false);
            this.box3.PerformLayout();
            this.box2.ResumeLayout(false);
            this.box2.PerformLayout();
            this.box1.ResumeLayout(false);
            this.box1.PerformLayout();
            this.box4.ResumeLayout(false);
            this.box4.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup C_grp_Connection;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton Btn_Login;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup pnlProposta;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SaveProposta;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SearchPropost;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SearchProposta;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonActualizar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCriarProposta;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup pnlActualizacao;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel labelDMData;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupEquipServInstalar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAdicionarEquip;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAdicionarServico;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonEliminarEquip;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupDadosResumo;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTipoContrato;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxPeriocidade;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxFactor;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxPrazo;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box1;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotalEquipInst;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotServico;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotalCont;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box3;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAdicionarEquipRem;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonEliminarEquipRemover;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupRecursosHumanos;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAdicionarRH;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonEliminarRH;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupSoftware;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAddSoft;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDelSoft;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupOutrosServ;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAddOutrosServ;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDelOutroServ;
        private System.Windows.Forms.Timer timer1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonValidarDadosProposta;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box4;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotalRH;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotalSoft;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotalOrs;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxOutrosServ;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox editBoxTotalRenda;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonLoggedIn;
        private System.Windows.Forms.ImageList imageList1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box5;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label2;


    }
  
}
