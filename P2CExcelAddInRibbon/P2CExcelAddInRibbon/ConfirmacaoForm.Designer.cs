﻿namespace ExcelAddIn
{
    partial class ConfirmacaoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxMsg = new System.Windows.Forms.PictureBox();
            this.labelMsg = new System.Windows.Forms.Label();
            this.buttonNao = new System.Windows.Forms.Button();
            this.buttonSim = new System.Windows.Forms.Button();
            this.buttonDuplicar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMsg)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxMsg
            // 
            this.pictureBoxMsg.Image = global::P2CExcelAddInRibbon.Properties.Resources.delete_48;
            this.pictureBoxMsg.Location = new System.Drawing.Point(13, 9);
            this.pictureBoxMsg.Name = "pictureBoxMsg";
            this.pictureBoxMsg.Size = new System.Drawing.Size(59, 57);
            this.pictureBoxMsg.TabIndex = 1;
            this.pictureBoxMsg.TabStop = false;
            // 
            // labelMsg
            // 
            this.labelMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMsg.Location = new System.Drawing.Point(77, 18);
            this.labelMsg.Name = "labelMsg";
            this.labelMsg.Size = new System.Drawing.Size(294, 92);
            this.labelMsg.TabIndex = 2;
            this.labelMsg.Text = "Mensagem de Delete";
            // 
            // buttonNao
            // 
            this.buttonNao.Location = new System.Drawing.Point(187, 113);
            this.buttonNao.Name = "buttonNao";
            this.buttonNao.Size = new System.Drawing.Size(65, 23);
            this.buttonNao.TabIndex = 3;
            this.buttonNao.Text = "Não";
            this.buttonNao.UseVisualStyleBackColor = true;
            this.buttonNao.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonSim
            // 
            this.buttonSim.Location = new System.Drawing.Point(98, 113);
            this.buttonSim.Name = "buttonSim";
            this.buttonSim.Size = new System.Drawing.Size(65, 23);
            this.buttonSim.TabIndex = 4;
            this.buttonSim.Text = "Sim";
            this.buttonSim.UseVisualStyleBackColor = true;
            this.buttonSim.Click += new System.EventHandler(this.buttonSim_Click);
            // 
            // buttonDuplicar
            // 
            this.buttonDuplicar.Location = new System.Drawing.Point(267, 113);
            this.buttonDuplicar.Name = "buttonDuplicar";
            this.buttonDuplicar.Size = new System.Drawing.Size(105, 23);
            this.buttonDuplicar.TabIndex = 5;
            this.buttonDuplicar.Text = "Duplicar Proposta";
            this.buttonDuplicar.UseVisualStyleBackColor = true;
            this.buttonDuplicar.Click += new System.EventHandler(this.buttonDuplicar_Click);
            // 
            // ConfirmacaoForm
            // 
            this.AcceptButton = this.buttonNao;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 149);
            this.Controls.Add(this.buttonDuplicar);
            this.Controls.Add(this.buttonSim);
            this.Controls.Add(this.buttonNao);
            this.Controls.Add(this.labelMsg);
            this.Controls.Add(this.pictureBoxMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmacaoForm";
            this.Load += new System.EventHandler(this.MensagensForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMsg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxMsg;
        private System.Windows.Forms.Label labelMsg;
        private System.Windows.Forms.Button buttonNao;
        private System.Windows.Forms.Button buttonSim;
        private System.Windows.Forms.Button buttonDuplicar;
    }
}