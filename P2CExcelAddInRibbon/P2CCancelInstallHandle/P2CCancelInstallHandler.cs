﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Collections;
using System.Configuration.Install;
using P2CExcelAddIn.Business.Security;

namespace P2CCancelInstallHandle
{
    [RunInstaller(true)]
    public partial class P2CCancelInstallHandler : System.Configuration.Install.Installer
    {
        public P2CCancelInstallHandler()
        {
            InitializeComponent();
        }

        public override void Commit(IDictionary savedState)
        {
            SessionKeyRegistry register = new SessionKeyRegistry();

            string isUpdated = register.Read("P2CUpdatedSucess");

            if (isUpdated != null && isUpdated.Equals("Sucess"))
            {
                base.Commit(savedState);
            }
            else
                throw new InstallException("É obrigatório actualizar os dados mestre.");

        }
    }
}
