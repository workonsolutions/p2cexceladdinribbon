﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Win32;
using P2CExcelAddIn.Business.Security;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine ("A ler....");
           
            RegistryKey baseRegistryKey = Registry.Users;
            string subKey = Settings.Default.SessionKeyRegister;

            //RegistryKey rk = baseRegistryKey;
            //RegistryKey sk1 = rk.OpenSubKey(subKey);

            //Console.WriteLine(sk1);
            //Console.WriteLine( (string)sk1.GetValue("P2CInstallLocation"));
            //var fileLocation = SessionKeyRegistry.Read("P2CInstallLocation");
            //Console.WriteLine(fileLocation);

            RegistryKey rk = baseRegistryKey;
            // I have to use CreateSubKey 
            // (create or open it if already exits), 
            // 'cause OpenSubKey open a subKey as read-only
            RegistryKey sk1 = rk.CreateSubKey(subKey);
            // Save the value
            sk1.SetValue("Test123", "this is test");

            Console.WriteLine( Settings.Default.Template_ApplicationMenu);
            Console.ReadLine();
        }

       
    }
}
