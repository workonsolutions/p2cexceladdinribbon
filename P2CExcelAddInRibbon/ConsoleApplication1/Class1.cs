﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using ConsoleApplication1;

namespace P2CExcelAddIn.Business.Security
{
    public static class SessionKeyRegistry
    {
        static RegistryKey baseRegistryKey = Registry.CurrentUser;
        static string subKey = Settings.Default.SessionKeyRegister;

        public static string Read(string KeyName)
        {
            // Opening the registry key
            RegistryKey rk = baseRegistryKey;
            // Open a subKey as read-only
            RegistryKey sk1 = rk.OpenSubKey(subKey);

            return (string)sk1.GetValue(KeyName.ToUpper());
            // If the RegistrySubKey doesn't exist -> (null)
           
        }

        public static bool Write(string KeyName, object Value)
        {
            try
            {
                // Setting
                RegistryKey rk = baseRegistryKey;
                // I have to use CreateSubKey 
                // (create or open it if already exits), 
                // 'cause OpenSubKey open a subKey as read-only
                RegistryKey sk1 = rk.CreateSubKey(subKey);
                // Save the value
                sk1.SetValue(KeyName.ToUpper(), Value);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

      


    }
}
