﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using P2CExcelAddIn.Business.Security;


namespace UpdateMasterDataInstalation
{
    [RunInstaller(true)]
    public partial class InstallerUpdateMasterData : System.Configuration.Install.Installer
    {
        public InstallerUpdateMasterData()
        {
            InitializeComponent();
        }


        protected override void OnBeforeInstall(IDictionary savedState)
        {
            try
            {
                base.OnBeforeInstall(savedState);
                FileInfo fileInfo = new FileInfo
                (System.Reflection.Assembly.GetExecutingAssembly().Location);
                //Take custom action data values
                string sProgram = Context.Parameters["Run"];
                sProgram = Path.Combine(fileInfo.DirectoryName, sProgram);
                Trace.WriteLine("Install sProgram= " + sProgram);
                OpenWithStartInfo(sProgram);
            }
            catch (Exception exc)
            {
                Context.LogMessage(exc.ToString());
                throw;
            }
        }

        void OpenWithStartInfo(string sProgram)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(sProgram);
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            string[] ExcludeKeys = new string[] { "run", "WaitForExit" };
            startInfo.Arguments = ContextParametersToCommandArguments(Context, ExcludeKeys);
            Trace.WriteLine("run the program " + sProgram + startInfo.Arguments);
            Process p = Process.Start(startInfo);
            ShowWindow(p.MainWindowHandle, WindowShowStyle.Show); 	//otherwise it is 
            //not activated 
            SetForegroundWindow(p.MainWindowHandle);
            BringWindowToTop(p.MainWindowHandle); 	// Make sure the user will see 
            // the new window above of the setup.
            Trace.WriteLine("the program Responding= " + p.Responding);
            if ((Context.IsParameterTrue("WaitForExit")))
            {
                p.WaitForExit();// Have to hold the setup until the application is closed.
            }
        }

        public static String ContextParametersToCommandArguments
        (InstallContext context, string[] ExcludeKeys)
        {
            ExcludeKeys = ToLower(ExcludeKeys);
            StringBuilder sb = new StringBuilder();
            foreach (DictionaryEntry de in context.Parameters)
            {
                string sKey = (string)de.Key;
                bool bAdd = true;
                if (ExcludeKeys != null)
                {
                    bAdd = (Array.IndexOf(ExcludeKeys, sKey.ToLower()) < 0);
                }
                if (bAdd)
                {
                    AppendArgument(sb, sKey, (string)de.Value);
                }
            }
            return sb.ToString();
        }

        public static StringBuilder AppendArgument(StringBuilder sb, String Key, string value)
        {
            sb.Append(" /");
            sb.Append(Key);
            //Note that if value is empty string, = sign is expected, e.g."/PORT="
            if (value != null)
            {
                sb.Append("=");
                sb.Append(value);
            }
            return sb;
        }

        #region "FS library methods"
        public static string[] ToLower(string[] Strings)
        {
            if (Strings != null)
            {
                for (int i = 0; i < Strings.Length; i++)
                {
                    Strings[i] = Strings[i].ToLower();
                }
            }
            return Strings;
        }
        #endregion //"FS library methods"
        #region "showWindow"

        // http://pinvoke.net/default.aspx/user32.BringWindowToTop
        [DllImport("user32.dll")]
        static extern bool BringWindowToTop(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        //from http://pinvoke.net/default.aspx/user32.SwitchToThisWindow 
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, WindowShowStyle nCmdShow);

        /// <summary>Enumeration of the different ways of showing a window using 
        /// ShowWindow</summary>
        private enum WindowShowStyle : uint
        {
            Hide = 0,
            ShowNormal = 1,
            ShowMinimized = 2,
            ShowMaximized = 3,
            Maximize = 3,
            ShowNormalNoActivate = 4,
            Show = 5,
            Minimize = 6,
            ShowMinNoActivate = 7,
            ShowNoActivate = 8,
            Restore = 9,
            ShowDefault = 10,
            ForceMinimized = 11
        }
        #endregion
    }
}
