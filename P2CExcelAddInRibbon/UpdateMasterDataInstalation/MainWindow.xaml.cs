﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using P2CExcelAddIn.Business.Security;
using P2CExcelAddIn.Business.ServicesController;
using P2CExcelAddIn.Business.ExcelControllers;
using System.Reflection;
using P2CExcelAddIn.Business.Properties;
using System.IO;


namespace UpdateMasterDataInstalation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            SessionKeyRegistry register = new SessionKeyRegistry();
            try
            {
                this.Cursor = Cursors.Wait;



                var sessionKey = textBoxUsername.Text + "<------&#Xerox2013#&----->" + passwordBox1.Password + "<------&#Xerox2013#&----->" + DateTime.Now.ToString();

                sessionKey = register.Encrypt(sessionKey, true, "!xerox2013");
                string macAddress = P2CAddinUtilsController.GetMACAddress();
                ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();

                var returnLogin = service.Login(sessionKey, macAddress);

                if (returnLogin.sucess)
                {

                    register.Write("ChaveSessao", DateTime.Now.ToString());
                    register.Write("UserNameSessao", textBoxUsername.Text);
                    labelMsgError.Visibility = Visibility.Hidden;
                    UpdateMasterData(sessionKey, macAddress);
                    register.Write("DadosMestreData", DateTime.Now.ToString());
                    register.Write("P2CUpdatedSucess", "Sucess");
                    FileInfo fileInfo = new FileInfo
                    (System.Reflection.Assembly.GetExecutingAssembly().Location);
                    register.Write("P2CInstallLocation", fileInfo.Directory + "\\");
                    this.Cursor = Cursors.Arrow;
                    this.Close();
                }
                else
                {
                    register.Write("P2CUpdatedSucess", "failed");
                    labelMsgError.Visibility = Visibility.Visible;
                    labelMsgError.Content = returnLogin.errorMessage;
                    this.Cursor = Cursors.Arrow;
                }



            }
            catch (Exception ex)
            {
                register.Write("P2CUpdatedSucess", "failed");
                labelMsgError.Visibility = Visibility.Visible;
                labelMsgError.Content = ex.Message;
                
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        }


        public void UpdateMasterData(string sessionKey,string macAddress)
        {

            FileInfo fileInfo2 = new FileInfo
                    (System.Reflection.Assembly.GetExecutingAssembly().Location);

            //string path = "c:\\" + Settings.Default.AppRoot + Settings.Default.Template_ApplicationMenu;
            string path = fileInfo2.Directory +"\\"+ Settings.Default.Template_ApplicationMenu;
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook wb = excel.Workbooks.Open(path);
            excel.DisplayAlerts = false;
            SessionKeyRegistry register = new SessionKeyRegistry();

           // string sessKey = register.Encrypt(sessionKey, true, "!xerox2013");
            ProposalP2CtoExcelAddInController service = new ProposalP2CtoExcelAddInController();
            var masterData = service.GetMasterData(sessionKey, macAddress);

            P2CAddInMasterDataController.UpdateMasterData(wb, masterData);


            wb.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLTemplate);
            wb.Close();
            
            
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            SessionKeyRegistry register = new SessionKeyRegistry();
            register.Write("P2CUpdatedSucess", "failed");
            this.Close();
        }

    }
}
